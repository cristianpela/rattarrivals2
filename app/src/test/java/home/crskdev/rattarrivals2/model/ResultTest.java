package home.crskdev.rattarrivals2.model;

import org.junit.Test;

import home.crskdev.rattarrivals2.lines.model.Line;

import static org.junit.Assert.*;

/**
 * Created by criskey on 15/9/2017.
 */
public class ResultTest {
    @Test
    public void caseOf() throws Exception {
        assertTrue(Result.caseOf(new Result.LineResult(Line.NO_LINE), Result.LineResult.class));
    }

}