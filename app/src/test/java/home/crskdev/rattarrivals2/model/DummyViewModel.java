package home.crskdev.rattarrivals2.model;

/**
 * Created by criskey on 4/10/2017.
 */
public class DummyViewModel extends BaseViewModel {

    private final int id;

    public String foo = "";

    public DummyViewModel(int id) {
        this.id = id;
    }

    public DummyViewModel(int id, String foo) {
        this.id = id;
        this.foo = foo;
    }
}
