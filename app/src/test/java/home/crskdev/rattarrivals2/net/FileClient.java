package home.crskdev.rattarrivals2.net;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.File;
import java.io.IOException;

/**
 * Created by criskey on 20/8/2017.
 */
public class FileClient implements Client {

    private File file;

    public FileClient(File file) {
        this.file = file;
    }

    @Override
    public Document request(String url) throws IOException {
        return Jsoup.parse(file, "UTF-8", "http://example.com/");
    }
}
