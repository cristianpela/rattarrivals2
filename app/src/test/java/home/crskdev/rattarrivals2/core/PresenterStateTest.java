package home.crskdev.rattarrivals2.core;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import home.crskdev.rattarrivals2.BaseTest;
import home.crskdev.rattarrivals2.model.Result;
import home.crskdev.rattarrivals2.model.Result.Wait;
import home.crskdev.rattarrivals2.util.Pair;
import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;

/**
 * Created by criskey on 7/9/2017.
 */
public class PresenterStateTest extends BaseTest {
    @Test
    public void nextState() throws Exception {
        PresenterState state = new PresenterState();
        TestObserver<List<Result>> test = state.observable().map(this::asList).test();
        state.next(Wait.WAIT);
        state.nextError(new Error("FOO"));
        state.next(Wait.WAIT);
        state.next(new Result.Data<Integer>(1){});
        state.next(new Result.Data<Integer>(2){});
        state.next(new Result.Data<Integer>(2){});
        state.next(new Result.Data<Integer>(3){});

        printList(test.values());
    }


    @Test
    public void name() throws Exception {
        TestObserver<Pair<Integer, Integer>> test = Observable.fromIterable(Arrays.asList(1, 2, 3, 4))
                .scan(Pair.of(0, 0), (acc, curr) -> Pair.of(acc.second, curr)).test();

        System.out.println(test.values());

    }

    private List<Result> asList(Collection<Result> col) {
        List<Result> l = new ArrayList<>(col);
        Collections.sort(l, (c1, c2)
                -> c1.getClass().getSimpleName().compareTo(c2.getClass().getSimpleName()));
        return l;
    }


}