package home.crskdev.rattarrivals2.lines.services;

import org.junit.Test;

import home.crskdev.rattarrivals2.lines.services.LineFormatter;

import static org.junit.Assert.*;

/**
 * Created by criskey on 1/9/2017.
 */
public class LineFormatterTest {
    @Test
    public void format() throws Exception {
        LineFormatter formater = new LineFormatter();
        assertEquals("M1", formater.format("Metropolitana 1"));
        assertEquals("E1", formater.format("Express 1"));
        assertEquals("1b", formater.format("1 barat"));
        assertEquals("E1b", formater.format("expres 1 barat"));
        assertEquals("E4b", formater.format("Expres 4 barat"));
    }

}