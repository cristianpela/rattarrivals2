package home.crskdev.rattarrivals2.lines.services;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import home.crskdev.rattarrivals2.lines.model.Line;
import home.crskdev.rattarrivals2.lines.model.LineType;
import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;

import static home.crskdev.rattarrivals2.lines.model.LineType.AUTO;
import static home.crskdev.rattarrivals2.lines.model.LineType.TRAM;
import static home.crskdev.rattarrivals2.lines.model.LineType.TROLLEY;
import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by criskey on 11/8/2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class CacheLineParseServiceImplTest {

    @Mock
    LineParseService src;

    @Test
    public void parseLine() throws Exception {
        List<Line> l1 = Collections.singletonList(new Line(1, "", AUTO, false));
        List<Line> l2 = Collections.singletonList(new Line(2, "", TRAM, false));
        List<Line> l3 = Collections.singletonList(new Line(3, "", TROLLEY, false));
        when(src.parseLine(AUTO)).thenReturn(Observable.just(l1));
        when(src.parseLine(TRAM)).thenReturn(Observable.just(l2));
        when(src.parseLine(LineType.TROLLEY)).thenReturn(Observable.just(l3));

        LineParseService cache = new CacheLineParseService(src);
        cache.parseLine(AUTO).subscribe();
        cache.parseLine(TRAM).subscribe();
        cache.parseLine(LineType.TROLLEY).subscribe();


        TestObserver<List<Line>> test = cache.parseLine(AUTO).mergeWith(
                cache.parseLine(TRAM)).mergeWith(
                cache.parseLine(LineType.TROLLEY)).test();

        assertEquals(Arrays.asList(l1, l2, l3), test.values());

        verify(src, times(3)).parseLine(any(LineType.class));


    }

}