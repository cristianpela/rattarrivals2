package home.crskdev.rattarrivals2.lines.presentation;

import org.junit.Test;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

import home.crskdev.rattarrivals2.BaseTest;
import home.crskdev.rattarrivals2.lines.presentation.StaleDataWatcher.StaleData;
import home.crskdev.rattarrivals2.util.ImmLists;
import home.crskdev.rattarrivals2.util.time.TimeValue;
import io.reactivex.Flowable;
import io.reactivex.Single;
import io.reactivex.subscribers.TestSubscriber;

import static java.util.concurrent.TimeUnit.MINUTES;
import static java.util.concurrent.TimeUnit.SECONDS;
import static junit.framework.Assert.assertEquals;

/**
 * .
 * Created by criskey on 31/10/2017.
 */
public class StaleDataWatcherTest extends BaseTest {
    @Test
    public void watch() throws Exception {

        testScheduler.advanceTimeTo(System.currentTimeMillis(), TimeUnit.MILLISECONDS);

        long timeStampMock = System.currentTimeMillis() - 10L * 60L * 1000L; // 10 minutes back in time

        Single<List<MockData>> repo = Single.just(Collections.singletonList(new MockData(1, "foo", timeStampMock)));

        StaleDataWatcher<Integer> sdw = new StaleDataWatcher<>(new TimeValue(30, SECONDS), testScheduler);

        Flowable<List<StaleData<Integer>>> intervalCallee = repo
                .toFlowable()
                .compose(sdw.mapTransformer(mk -> StaleData.asMillisData(mk.id, mk.time)));

        TestSubscriber<List<StaleData<Integer>>> test = sdw
                .watch(intervalCallee)
                .compose(sdw.timeTransformer(TimeUnit.MINUTES))
                .test();

        testScheduler.advanceTimeBy(10, MINUTES);

        assertEquals(20, test.valueCount());

        ImmLists.print(test.values());


    }

    private static class MockData {
        final int id;
        final String name;
        final long time;

        private MockData(int id, String name, long time) {
            this.id = id;
            this.name = name;
            this.time = time;
        }
    }

}