package home.crskdev.rattarrivals2.lines.presentation;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import home.crskdev.rattarrivals2.BaseTest;
import home.crskdev.rattarrivals2.arrivals.model.Arrival;
import home.crskdev.rattarrivals2.arrivals.model.ArrivalExtra;
import home.crskdev.rattarrivals2.arrivals.model.ArrivalExtraViewModel;
import home.crskdev.rattarrivals2.arrivals.model.ArrivalViewModel;
import home.crskdev.rattarrivals2.arrivals.repository.ArrivalRepository;
import home.crskdev.rattarrivals2.arrivals.services.ArrivalParseService;
import home.crskdev.rattarrivals2.lines.model.LineType;
import home.crskdev.rattarrivals2.lines.repository.LineRepository;
import home.crskdev.rattarrivals2.model.Action;
import home.crskdev.rattarrivals2.model.Result;
import home.crskdev.rattarrivals2.util.ResourceFinder;
import home.crskdev.rattarrivals2.util.time.CountValueCacheContainer;
import home.crskdev.rattarrivals2.util.time.FeedingIntervalCounter;
import home.crskdev.rattarrivals2.util.time.IntervalCounterImpl;
import home.crskdev.rattarrivals2.util.time.TimeEstimated;
import home.crskdev.rattarrivals2.util.time.TimeUtils;
import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.schedulers.TestScheduler;
import io.reactivex.subjects.PublishSubject;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by criskey on 14/9/2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class LinePresenterTest extends BaseTest {


    @Mock
    LineRepository repository;

    @Mock
    ArrivalRepository arrivalRepository;

    @Mock
    ArrivalParseService arrivalParseService;

    @Mock
    LineView view;

    @Mock
    ResourceFinder resourceFinder;

    private LinePresenter presenter;
    private TestScheduler externalTestScheduler = new TestScheduler();
    /**
     * Scheduler used by presenter internally (the computation scheduler)
     * Do not use it in other streams. Use externalTestScheduler or other test scheduler
     * like countdown, intervals etc..
     */
    private TestScheduler internalTestScheduler = new TestScheduler();


    StaleDataWatcher<String> staleWatcher = new StaleDataWatcher<>(TimeUtils.STALE_TIME_VALUE, new TestScheduler());

    //todo fix meh

    @Override
    public void setUp() throws Exception {
        super.setUp();
        presenter = new LinePresenter(
                Schedulers.trampoline(),
                arrivalRepository,
                arrivalParseService,
                new CountValueCacheContainer(),
                new IntervalCounterImpl(internalTestScheduler),
                staleWatcher, resourceFinder);
        presenter.computationScheduler = internalTestScheduler;
    }

    @Test
    public void should_show_pinned() throws Exception {
        when(arrivalRepository.observeShowing()).thenReturn(Flowable.fromArray(false, true));
        when(arrivalRepository.getAllExtra()).thenReturn(Single.never());
        when(arrivalRepository.observePin()).thenReturn(Observable.never());
        when(arrivalRepository.observeUnpin()).thenReturn(Observable.never());

        presenter.attachView(view, Collections.emptyMap(), Collections.emptyMap());

        ArgumentCaptor<Boolean> showCaptor = ArgumentCaptor.forClass(Boolean.class);
        verify(view, times(2)).onShowPinned(showCaptor.capture());
        assertEquals(Arrays.asList(false, true), showCaptor.getAllValues());
    }

    @Test
    public void should_refresh_pinned_station() throws Exception {
        final PublishSubject<List<ArrivalExtra>> pinned = PublishSubject.create();

        when(arrivalRepository.observeShowing()).thenReturn(Flowable.never());
        when(arrivalRepository.getAllExtra()).thenReturn(Single.never());
        when(arrivalRepository.observePin()).thenReturn(pinned);
        when(arrivalRepository.observeUnpin()).thenReturn(Observable.never());

        final long newLastUpdate = 20L;
        final long newTime = 2L;
        doAnswer(invoke -> Observable
                .just(new Result.StationResult(((Arrival) invoke.getArgument(0))
                        .lastUpdated(newLastUpdate).time(TimeEstimated.nonEstimated(newTime)))))
                .when(arrivalParseService).fetchStation(any(Arrival.class));
        doAnswer(invoke -> {
            ArrivalExtra a = invoke.getArgument(0);
            pinned.onNext(Collections.singletonList(a));
            return Single.just(a);
        }).when(arrivalRepository).refresh(any(ArrivalExtra.class));

        presenter.attachView(view, Collections.emptyMap(), Collections.emptyMap());


        final ArrivalExtraViewModel initial = new ArrivalExtraViewModel(
                1, "station", "to", "line", LineType.AUTO, 0, true, 1L, "", 0, mockEstimated(), 1, 10L, "", true);
        presenter.bindActions(Observable.just(new Action.StationAction(new ArrivalViewModel(
                1, "station", "to", 1L, "", 0, mockEstimated(), 10L, "", true))));

        verify(view).onWait();
//        verify(view).onDone();
//
        @SuppressWarnings("unchecked")
        ArgumentCaptor<List<ArrivalExtraViewModel>> modelCaptor = ArgumentCaptor.forClass(List.class);
        verify(view).addOrUpdatePinnedArrivals(modelCaptor.capture());
        ArrivalExtraViewModel updatedVM = modelCaptor.getValue().get(0);

        assertEquals(initial, updatedVM);
        assertEquals(updatedVM.lastUpdated, newLastUpdate);
        assertEquals(updatedVM.time, newTime);
    }


    @Test
    public void should_unpin() throws Exception {
        final PublishSubject<ArrivalExtra> unPinned = PublishSubject.create();

        when(arrivalRepository.observeShowing()).thenReturn(Flowable.never());
        when(arrivalRepository.getAllExtra()).thenReturn(Single.never());
        when(arrivalRepository.observePin()).thenReturn(Observable.never());
        when(arrivalRepository.observeUnpin()).thenReturn(unPinned);

        doAnswer(invoke -> {
            ArrivalExtra arr = ((ArrivalExtra) invoke.getArgument(0)).togglePinned();
            unPinned.onNext(arr);
            return Single.just(arr);
        }).when(arrivalRepository).unpin(any(ArrivalExtra.class));

        presenter.attachView(view, Collections.emptyMap(), Collections.emptyMap());

        final ArrivalExtraViewModel initial = new ArrivalExtraViewModel(
                1, "station", "to", "line", LineType.AUTO, 0,  true, 1L, "", 0, mockEstimated(), 1, 10L, "", true);
        presenter.bindActions(Observable.just(new Action.Pinned(initial)));

        ArgumentCaptor<ArrivalExtraViewModel> captor = ArgumentCaptor.forClass(ArrivalExtraViewModel.class);
        verify(view).removePinnedArrival(captor.capture());
        final ArrivalExtraViewModel actual = captor.getValue();
        assertEquals(initial, actual);
        assertFalse(actual.pinned);
    }

    private TimeEstimated mockEstimated(){
        return TimeEstimated.nonEstimated(0L);
    }

    @Test
    public void test_stale_data() throws Exception {

        //todo update to service 2
//
//        TestScheduler staleTestScheduler = new TestScheduler();
//
//        LinePresenter linePresenter = new LinePresenter(uiScheduler, pinnedArrivalService,
//                repository, arrivalParseService, new CountdownCooldown(new TestScheduler(), 1),
//                new StaleDataWatcher(staleTestScheduler));
//        linePresenter.computationScheduler = testScheduler;
//        linePresenter.postInject();
//
//        Arrival viewModel = new Arrival(1L, "FOO", System.currentTimeMillis(), "BOO", 0L);
//        //        doReturn(Observable.just(new Result
////                .StationResult(viewModel)))
////                .when(arrivalParseService).fetch(any(Action.class));
////        doReturn(Completable.complete())
////                .when(pinnedArrivalService).refreshCompletable(any(Arrival.class));
//        doReturn(Observable.just(viewModel))
//                .when(pinnedArrivalService).observable();
//        doReturn(Single.just(Line.NO_LINE))
//                .when(repository).getLinesById(anyLong());
//
//        linePresenter.attachView(view, Collections.emptyMap());
//
//        staleTestScheduler.advanceTimeTo(5, TimeUnit.MINUTES);
//
//        ArgumentCaptor<Integer> colorCaptor = ArgumentCaptor.forClass(Integer.class);
//        verify(view, times(5)).onStaleColor(colorCaptor.capture());
//
//        assertEquals(Arrays.asList(
//                FRESH_DATA_COLOR,
//                FRESH_DATA_COLOR,
//                ABOUT_TO_STALE_DATA_COLOR,
//                ABOUT_TO_STALE_DATA_COLOR,
//                STALE_DATA_COLOR
//        ), colorCaptor.getAllValues());
    }
}