package home.crskdev.rattarrivals2.lines.services;

import org.jsoup.nodes.Document;
import org.junit.Test;

import home.crskdev.rattarrivals2.BaseTest;
import home.crskdev.rattarrivals2.lines.model.LineType;
import home.crskdev.rattarrivals2.lines.services.LinesParser;
import home.crskdev.rattarrivals2.net.Client;
import home.crskdev.rattarrivals2.net.FileClient;

import static org.junit.Assert.*;

/**
 * Created by criskey on 11/8/2017.
 */
public class LinesParserTest extends BaseTest {
    @Test
    public void getLines() throws Exception {

        Client client = new FileClient(loadFileFromResources("lines_tram.html"));
        Document doc = client.request("");

        LinesParser linesParser = new LinesParser(doc, LineType.TRAM);

        String expected = "[Line{id='1106', title='1', type=TRAM, favorite=false}, " +
                "Line{id='1126', title='2', type=TRAM, favorite=false}, " +
                "Line{id='1266', title='4', type=TRAM, favorite=false}, " +
                "Line{id='1553', title='5', type=TRAM, favorite=false}, " +
                "Line{id='2686', title='6A', type=TRAM, favorite=false}, " +
                "Line{id='2706', title='6B', type=TRAM, favorite=false}, " +
                "Line{id='2846', title='7', type=TRAM, favorite=false}, " +
                "Line{id='1558', title='8', type=TRAM, favorite=false}, " +
                "Line{id='2406', title='9', type=TRAM, favorite=false}, " +
                "Line{id='2726', title='10', type=TRAM, favorite=false}]";

        //check string data was expected
        assertEquals(expected, linesParser.getLines().toString());

    }

}