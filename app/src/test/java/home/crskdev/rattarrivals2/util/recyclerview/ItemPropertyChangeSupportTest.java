package home.crskdev.rattarrivals2.util.recyclerview;

import org.junit.Test;

import static junit.framework.Assert.assertEquals;

/**
 * Created by criskey on 6/10/2017.
 */
public class ItemPropertyChangeSupportTest {
    @Test
    public void changeProperty() throws Exception {
        B b = new B();
        ItemPropertyChangeSupport<? super A> support = new ItemPropertyChangeSupport<>();
        support.changeProperty(b, "fieldA", 1);
        support.changeProperty(b, "fieldB", 2);
        assertEquals(1, b.fieldA);
        assertEquals(2, b.fieldB);
    }

    public static class A{
        int fieldA = 0;
    }

    public static class B extends A{
        int fieldB = 0;
    }

}