package home.crskdev.rattarrivals2.util.time;

import org.junit.Test;

import java.util.concurrent.TimeUnit;

import home.crskdev.rattarrivals2.BaseTest;
import timber.log.Timber;

import static org.junit.Assert.*;

/**
 * .
 * Created by criskey on 19/10/2017.
 */
public class ResumableCountValueTest extends BaseTest {




    @Test
    public void should_resume() {

        CountValue cv = new CountValue(0, 0, 5, TimeUnit.MINUTES, CountValue.FORWARD);
        ResumableCountValue rcv = ResumableCountValue.from(cv, testScheduler.now(TimeUnit.MILLISECONDS) );

        testScheduler.advanceTimeBy(2, TimeUnit.MINUTES);

        assertEquals(2, rcv.resumeAt(testScheduler.now(TimeUnit.MILLISECONDS)).getValue());

        Timber.i("Hello %s", "world");

//        //lets pass 2 seconds
//        long passed = now + (2 * 1000);
//        CountValue updated = rcv.resumeAt(passed);
//        //should return current - passed seconds
//        assertEquals(3, updated.getValue());
//
//        //lets pass a lot of seconds beyond min
//        passed = now + (100 * 1000);
//        updated = rcv.resumeAt(passed);
//        //should return value eq to min
//        assertEquals(0, updated.getValue());
    }

}