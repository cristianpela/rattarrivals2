package home.crskdev.rattarrivals2.util.time;

import org.junit.Test;

import java.util.concurrent.TimeUnit;

import home.crskdev.rattarrivals2.BaseTest;

import static org.junit.Assert.*;

/**
 * .
 * Created by criskey on 19/10/2017.
 */
public class CountValueTest extends BaseTest {

    CountValue cv = CountValue.startAtMin(0, 50, TimeUnit.SECONDS, CountValue.FORWARD);

    @Test
    public void should_pass_operations() throws Exception {
        cv = cv.advance(20, CountValue.FORWARD);
        assertEquals(20, cv.getValue());
        cv = cv.advance(10, CountValue.BACKWARD);
        assertEquals(10, cv.getValue());
        assertEquals(10, cv.remaining());
        assertEquals(40, cv.passed());
        cv = cv.changeDirection(CountValue.FORWARD);
        assertEquals(40, cv.remaining());
        cv = cv.changeTimeUnit(TimeUnit.MILLISECONDS);
        assertEquals(40 * 1000, cv.remaining());
        cv = cv.advance(40 * 1000); // should reach max limit 50
        assertTrue(cv.hasReachedEnd());
    }

    @Test
    public void should_clamp_if_out_of_bounds() {
        cv = cv.advance(500);
        assertEquals(50, cv.getValue());
    }

}