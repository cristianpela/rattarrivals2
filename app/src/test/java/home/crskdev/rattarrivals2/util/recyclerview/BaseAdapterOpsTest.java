package home.crskdev.rattarrivals2.util.recyclerview;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.stream.Collectors;

import home.crskdev.rattarrivals2.model.DummyViewModel;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;

/**
 * NOTE: The other methods are tested in ArrivalsAdapterOpsImplTest
 * Created by criskey on 2/10/2017.
 */
public class BaseAdapterOpsTest {

    private DummyViewModel dummyViewModel1;
    private DummyViewModel dummyViewModel2;
    private DummyViewModel dummyViewModel3;
    private AdapterOps<DummyViewModel> adapterOps;

    @Before
    public void setUp() throws Exception {
        adapterOps = new BaseAdapterOps<>();
        dummyViewModel1 = new DummyViewModel(1, "foo1");
        dummyViewModel2 = new DummyViewModel(2, "foo2");
        dummyViewModel3 = new DummyViewModel(3, "foo3");
        adapterOps.setItems(Arrays.asList(dummyViewModel1, dummyViewModel2, dummyViewModel3));
    }

    @Test
    public void addOrUpdateFeature() throws Exception {
        adapterOps.changePropertyAll("foo", "newFooValueAll");
        assertEquals(Arrays.asList(
                "newFooValueAll",
                "newFooValueAll",
                "newFooValueAll"
        ), adapterOps.itemStream().map(i -> i.foo).collect(Collectors.toList()));
    }

    @Test
    public void shouldRemoveItem() {
        final int index = adapterOps.remove(dummyViewModel2);
        assertEquals(1, index);
        assertEquals(2, adapterOps.getItemCount());
    }

    @Test
    public void shouldUpdateItems() {
        dummyViewModel1.foo = "newFoo1";
        dummyViewModel2.foo = "newFoo2";
        adapterOps.upsert(dummyViewModel2, dummyViewModel1, new DummyViewModel(4, "foo4"));
        assertEquals(
                Arrays.asList(
                        "newFoo1",
                        "newFoo2",
                        "foo3",
                        "foo4"
                ),
                adapterOps.itemStream().map(vm -> vm.foo).collect(Collectors.toList()));
    }
}