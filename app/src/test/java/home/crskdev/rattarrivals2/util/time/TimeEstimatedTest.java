package home.crskdev.rattarrivals2.util.time;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by criskey on 17/10/2017.
 */
public class TimeEstimatedTest {
    @Test
    public void estimate() throws Exception {
        long now = System.currentTimeMillis();
        long next3minutes = now + 3 * 60 * 1000;
        final TimeEstimated estimate = TimeEstimated.estimate(now, next3minutes);
        assertEquals(TimeEstimated.Estimation.NOW, estimate.estimation);
        assertEquals(next3minutes, estimate.time);
    }

}