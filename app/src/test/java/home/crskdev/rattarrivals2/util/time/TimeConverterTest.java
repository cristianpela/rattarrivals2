package home.crskdev.rattarrivals2.util.time;

import org.junit.Test;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import static home.crskdev.rattarrivals2.util.time.TimeUtils.*;
import static home.crskdev.rattarrivals2.util.time.TimeEstimated.Estimation.ABOUT_TO;
import static home.crskdev.rattarrivals2.util.time.TimeEstimated.Estimation.NOW;
import static home.crskdev.rattarrivals2.util.time.TimeEstimated.Estimation.NO_ESTIMATION;
import static org.junit.Assert.*;

/**
 * Created by criskey on 1/9/2017.
 */
public class TimeConverterTest {

    @Test
    public void should_show_right_now() throws Exception {
        //16:00 reference
        Calendar calendar = GregorianCalendar.getInstance(ROMANIAN_TIME_ZONE);
        calendar.set(2017, Calendar.DECEMBER, 31, 16, 0);
        long referenceTime = calendar.getTimeInMillis();
        TimeConverter tc = new TimeConverter(referenceTime, ROMANIAN_TIME_ZONE);

        assertEquals("16:00", getFormat(HOUR_MINUTES_FORMAT, tc.format(">>")));
        assertEquals(NOW, tc.format(">>").estimation);
        assertEquals("16:03", getFormat(HOUR_MINUTES_FORMAT, tc.format("3min.")));
        assertEquals(NOW, tc.format("3min.").estimation);
        assertEquals("16:03", getFormat(HOUR_MINUTES_FORMAT, tc.format(" 3 Min ")));
        assertEquals(ABOUT_TO, tc.format(" 7 Min ").estimation);
        assertEquals("16:11", getFormat(HOUR_MINUTES_FORMAT, tc.format("11 min.")));
        assertEquals(NO_ESTIMATION, tc.format("11 min.").estimation);
        assertEquals("02:00", getFormat(HOUR_MINUTES_FORMAT, tc.format("**:**")));
        assertEquals(NO_ESTIMATION, tc.format("**:**").estimation);
        assertEquals("17:15", getFormat(HOUR_MINUTES_FORMAT, tc.format("17:15")));
        assertEquals(NO_ESTIMATION, tc.format("17:15").estimation);

        //we advance one day if hour is lesser than the reference
        TimeEstimated time = tc.format("14:15");
        calendar.setTimeInMillis(time.time);
        assertEquals("14:15", getFormat(HOUR_MINUTES_FORMAT, time));
        assertEquals(1, calendar.get(Calendar.DAY_OF_MONTH));
        assertEquals(Calendar.JANUARY, calendar.get(Calendar.MONTH));
        assertEquals(2018, calendar.get(Calendar.YEAR));


    }

    private String getFormat(DateFormat format, TimeEstimated estimated) {
        return format.format(new Date(estimated.time));
    }

}