package home.crskdev.rattarrivals2.util.time;

import org.junit.Test;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.*;

/**
 * Created by criskey on 24/10/2017.
 */
public class TimeUtilsTest {
    @Test
    public void safeConvert() throws Exception {
        assertEquals(3, TimeUtils.safeConvert(3000, TimeUnit.MILLISECONDS, TimeUnit.SECONDS));
    }

}