package home.crskdev.rattarrivals2.util.time;

import org.junit.Test;

import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.LongStream;
import java.util.stream.Stream;

import home.crskdev.rattarrivals2.BaseTest;
import home.crskdev.rattarrivals2.util.ImmLists;
import io.reactivex.subscribers.TestSubscriber;

import static org.junit.Assert.*;

/**
 * .
 * Created by criskey on 19/10/2017.
 */
public class IntervalCounterTest extends BaseTest {


    @Test
    public void startCountBackwards() throws Exception {
        CountValue initial = CountValue.startAtMax(0, 10, TimeUnit.SECONDS, CountValue.BACKWARD);
        IntervalCounter counter = new IntervalCounterImpl(testScheduler, 1);
        TestSubscriber<CountValue> test = counter.startCount(initial).test();
        testScheduler.advanceTimeTo(30, TimeUnit.SECONDS);

        List<Long> values = ImmLists.map(test.values(), CountValue::getValue);
        assertTrue(test.values().get(0).isFlag());
        assertEquals(revRange(0, 10 + 1).collect(Collectors.toList()), values.subList(1, values.size()));
    }

    @Test
    public void startCountForwards() throws Exception {
        CountValue initial = CountValue.startAtMin(0, 10, TimeUnit.SECONDS, CountValue.FORWARD);
        IntervalCounter counter = new IntervalCounterImpl(testScheduler, 1);
        TestSubscriber<CountValue> test = counter.startCount(initial).test();
        testScheduler.advanceTimeTo(30, TimeUnit.SECONDS);

        List<Long> values = ImmLists.map(test.values(), CountValue::getValue);
        assertTrue(test.values().get(0).isFlag());
        assertEquals(LongStream.range(0, 10 + 1)
                .boxed().collect(Collectors.toList()), values.subList(1, values.size()));
        System.out.println(values);
    }

    @Test
    public void resumeCountForwards() throws Exception {
        CountValue initial = new CountValue(5, 0, 10, TimeUnit.SECONDS, CountValue.FORWARD);
        IntervalCounter counter = new IntervalCounterImpl(testScheduler, 1);
        TestSubscriber<CountValue> test = counter.startCount(initial).test();
        testScheduler.advanceTimeTo(30, TimeUnit.SECONDS);

        List<Long> values = ImmLists.map(test.values(), CountValue::getValue);
        assertTrue(test.values().get(0).isFlag());
        assertEquals(LongStream.range(5, 10 + 1)
                .boxed().collect(Collectors.toList()), values.subList(1, values.size()));
    }

    Stream<Long> revRange(long from, long to) {
        return LongStream.range(from, to).map(i -> to - i + from - 1).boxed();
    }
}