package home.crskdev.rattarrivals2.util;

import org.junit.Test;

import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import home.crskdev.rattarrivals2.BaseTest;
import home.crskdev.rattarrivals2.model.Result;
import home.crskdev.rattarrivals2.model.Result.Cooldown;
import io.reactivex.observers.TestObserver;

import static org.junit.Assert.*;

/**
 * Created by criskey on 18/10/2017.
 */
public class CountdownCooldownTest extends BaseTest {

    @SuppressWarnings("UnnecessaryBoxing")
    private static final Integer FLAG_START_VALUE = Integer.valueOf(Cooldown.FLAG_START_VALUE);

    final int max = 30;
    CountdownCooldown countDown = new CountdownCooldown(testScheduler, max);

    static Stream<Integer> revRange(int from, int to) {
        return IntStream.range(from, to).map(i -> to - i + from - 1).boxed();
    }

    @Test
    public void should_cool_down_whole_interval() throws Exception {
        TestObserver<Cooldown> test = createObservableTestAdvance();
        List<Integer> collect = test.values().stream().map(c -> c.value).collect(Collectors.toList());
        assertEquals(FLAG_START_VALUE, collect.get(0));
        assertEquals(revRange(0, max + 1).collect(Collectors.toList()), collect.subList(1, collect.size()));
        assertEquals(new Cooldown(0, max), countDown.getRemaining());
    }

    @Test
    public void should_end_when_remaining_is_0() throws Exception {
        countDown.resume(new Cooldown(-1, max));
        final TestObserver<Cooldown> test = createObservableTestAdvance();
        test.assertComplete();
        assertEquals(0, test.valueCount());
        assertEquals(new Cooldown(0, max), countDown.getRemaining());
    }

    @Test(expected = Exception.class)
    public void should_throw_if_remain_is_bigger_than_max() {
        countDown.resume(new Cooldown(1337, max));
        countDown.startCooldown();
    }

    @Test
    public void should_after_pause_should_resume_with_right_values() throws Exception {
        TestObserver<Cooldown> test = createObservableTestAdvance(10);

        List<Integer> collect = test.values().stream().map(c -> c.value).collect(Collectors.toList());
        assertEquals(FLAG_START_VALUE, collect.get(0));
        assertEquals(revRange(20, max + 1).collect(Collectors.toList()), collect.subList(1, collect.size()));
        assertEquals(new Cooldown(20, max), countDown.getRemaining());

        test.dispose();
        test.assertNotComplete();

        countDown.resume(new Cooldown(20, max));
        test = createObservableTestAdvance();

        collect = test.values().stream().map(c -> c.value).collect(Collectors.toList());
        assertEquals(FLAG_START_VALUE, collect.get(0));
        assertEquals(revRange(0, 20 + 1).collect(Collectors.toList()), collect.subList(1, collect.size()));
        assertEquals(new Cooldown(0, max), countDown.getRemaining());


    }

    private TestObserver<Cooldown> createObservableTestAdvance(int to) {
        final TestObserver<Cooldown> test = countDown.startCooldown().test();
        testScheduler.advanceTimeTo(to, TimeUnit.SECONDS);
        return test;
    }

    private TestObserver<Cooldown> createObservableTestAdvance() {
        return createObservableTestAdvance(max);
    }
}