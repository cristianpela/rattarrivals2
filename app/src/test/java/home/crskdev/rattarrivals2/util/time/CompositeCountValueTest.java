package home.crskdev.rattarrivals2.util.time;

import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.concurrent.TimeUnit;

import home.crskdev.rattarrivals2.BaseTest;
import home.crskdev.rattarrivals2.util.ImmLists;

import static home.crskdev.rattarrivals2.util.time.CountValue.BACKWARD;
import static home.crskdev.rattarrivals2.util.time.CountValue.FORWARD;
import static org.junit.Assert.*;

/**
 * .
 * Created by criskey on 21/10/2017.
 */
public class CompositeCountValueTest extends BaseTest {

    public static final long MAX = 10;

    public static final long MIN = 0;

    public static final TimeUnit UNIT = TimeUnit.SECONDS;

    public static final int DIR = FORWARD;

    private CompositeCountValue compo;

    @Override
    public void setUp() throws Exception {
        super.setUp();
        compo = CompositeCountValue.createHomogeneus(MIN, MAX, UNIT, DIR, Arrays.asList(
                new CountValue(4 * 1000, MIN * 1000, MAX * 1000, TimeUnit.MILLISECONDS, BACKWARD),
                OwnedCountValue.from(new CountValue(8 * 1000, MIN * 1000, MAX * 1000, TimeUnit.MILLISECONDS,
                        BACKWARD), "foo"),
                CountValue.startAtMin(MIN, MAX, UNIT, DIR)
        ));

    }

    @Test
    public void should_be_homogeneus() throws Exception {
        assertEquals(3, compo.getCountValues().stream()
                .filter(cv -> compo.direction == FORWARD).count());
        assertEquals(3, compo.getCountValues().stream()
                .filter(cv -> compo.unit == TimeUnit.SECONDS).count());
        assertEquals(Arrays.asList(MIN, 4L, 8L), ImmLists.map(compo.getCountValues(), CountValue::getValue));
        assertEquals(Arrays.asList(MIN, MIN, MIN), ImmLists.map(compo.getCountValues(), c -> c.min));
        assertEquals(Arrays.asList(MAX, MAX, MAX), ImmLists.map(compo.getCountValues(), c -> c.max));
    }

    @Test
    public void should_flatten() throws Exception {
        CountValue cv1 = new CountValue(1, MIN, MAX, UNIT, DIR);
        CountValue cv2 = new CountValue(2, MIN, MAX, UNIT, DIR);
        CountValue cv3 = new CountValue(3, MIN, MAX, UNIT, DIR);

        compo = compo.addCountValues(Arrays.asList(
                CompositeCountValue.createHomogeneus(MIN, MAX, UNIT, DIR,  Arrays.asList(cv1, cv2)),
                cv3));

        assertEquals(6, compo.getCountValues().size());
        assertTrue(compo.getCountValues().contains(cv1));
        assertTrue(compo.getCountValues().contains(cv2));
        assertTrue(compo.getCountValues().contains(cv3));

    }

    @Test
    public void advance() throws Exception {
        compo = compo.advance(4);
        //todo right now filtering is disabled
//        assertEquals("Should have 2 count values the 3rd is filtered cause reached MAX",
//                2, compo.getCountValues().size());
        compo = compo.advance(10000);
        assertTrue(compo.hasReachedEnd());
        //todo right now filtering is disabled
        //assertTrue("All count values are clamped to max, get count values must be empty", compo.getCountValues().isEmpty());
    }

    @Test
    public void should_have_diff_value_when_change_direction() throws Exception {
        assertEquals(10, compo.remaining());
        assertEquals("On forward direction must be minimum value of count values", 0, compo.getValue());
        compo = compo.changeDirection(BACKWARD);
        assertEquals("On backward direction must be maximum value of count values", 8, compo.getValue());
        compo = compo.advance(2);
        assertEquals(6, compo.remaining());
    }

    @Test
    public void should_add_new_values() {
        compo = compo.addCountValues(Arrays.asList(
                new CountValue(5 * 1000, MIN * 1000, MAX * 1000, TimeUnit.MILLISECONDS, BACKWARD),
                OwnedCountValue.from(new CountValue(3 * 1000, MIN * 1000, MAX * 1000, TimeUnit.MILLISECONDS,
                        BACKWARD), "boo")
        ));
        assertEquals(5, compo.getCountValues().stream()
                .filter(cv -> compo.direction == FORWARD).count());
        assertEquals(5, compo.getCountValues().stream()
                .filter(cv -> compo.unit == TimeUnit.SECONDS).count());
        assertEquals(Arrays.asList(MIN, 3L, 4L, 5L, 8L), ImmLists.map(compo.getCountValues(), CountValue::getValue));
        assertEquals(Arrays.asList(MIN, MIN, MIN, MIN, MIN), ImmLists.map(compo.getCountValues(), c -> c.min));
        assertEquals(Arrays.asList(MAX, MAX, MAX, MAX, MAX), ImmLists.map(compo.getCountValues(), c -> c.max));
    }

    @Test
    public void should_override_only_value_is_fresh() throws Exception {

        OwnedCountValue<String> noResult = OwnedCountValue.from(CountValue.startFlag(0, 0), "foo");

        compo = compo.addCountValues(Collections.singletonList(
                OwnedCountValue.from(new CountValue(1 * 1000, MIN * 1000, MAX * 1000, TimeUnit.MILLISECONDS,
                        FORWARD), "foo")));

        @SuppressWarnings("unchecked")
        OwnedCountValue<String> fooCountValue = (OwnedCountValue<String>) ImmLists.Chain.from(compo.getCountValues())
                .filter(cv -> cv instanceof OwnedCountValue && ((OwnedCountValue) cv).key.equals("foo"))
                .ifEmptyThen(Collections.singletonList(noResult))
                .get()
                .get(0);

        assertEquals(1, fooCountValue.getValue());
    }

    @Test
    public void should_not_override_if_value_not_fresh() throws Exception {

        OwnedCountValue<String> noResult = OwnedCountValue.from(CountValue.startFlag(0, 0), "foo");

        compo = compo.addCountValues(Collections.singletonList(
                OwnedCountValue.from(new CountValue(9 * 1000, MIN * 1000, MAX * 1000, TimeUnit.MILLISECONDS,
                        FORWARD), "foo")));

        @SuppressWarnings("unchecked")
        OwnedCountValue<String> fooCountValue = (OwnedCountValue<String>) ImmLists.Chain.from(compo.getCountValues())
                .filter(cv -> cv instanceof OwnedCountValue && ((OwnedCountValue) cv).key.equals("foo"))
                .ifEmptyThen(Collections.singletonList(noResult))
                .get()
                .get(0);

        assertEquals(8, fooCountValue.getValue());

    }

    @Test
    public void should_remove() throws Exception {
        compo = compo.removeCountValue(OwnedCountValue.from(new CountValue(8 * 1000, MIN * 1000, MAX * 1000, TimeUnit.MILLISECONDS,
                BACKWARD), "foo"));


        assertTrue(ImmLists.Chain.from(compo.getCountValues())
                .filter(cv -> cv instanceof OwnedCountValue && ((OwnedCountValue) cv).key.equals("foo"))
                .get().isEmpty());

        System.out.println(compo);

        compo = compo.removeCountValue(new CountValue(4 * 1000, MIN * 1000, MAX * 1000, TimeUnit.MILLISECONDS, BACKWARD));

        assertEquals(1, compo.getCountValues().size());

        System.out.println(compo);

        compo = compo.removeCountValue(CountValue.startAtMin(MIN, MAX, UNIT, DIR));

        assertTrue(compo.getCountValues().isEmpty());

        System.out.println(compo.getValue());
    }
}