package home.crskdev.rattarrivals2.util.time;

import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

import home.crskdev.rattarrivals2.BaseTest;
import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subscribers.TestSubscriber;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;


/**
 * .
 * Created by criskey on 21/10/2017.
 */
public class FeedingIntervalCounterTest extends BaseTest {


    @Test
    public void real_time_test() throws Exception {

        TimeUnit period = TimeUnit.MINUTES;

        List<CountValue> next = Collections.singletonList(CountValue.startAtMin(0, 3, TimeUnit.MINUTES, CountValue.FORWARD));

        FeedingIntervalCounterImpl intervalCounter = new FeedingIntervalCounterImpl(Schedulers.newThread(), 0,
                3, period, CountValue.FORWARD);

        intervalCounter.feedValues(next, false);

        intervalCounter.startCount(CountValue.NULL_FLAG).filter(cv -> !cv.isFlag()).subscribe();

        Observable.interval(90, 1, TimeUnit.SECONDS, Schedulers.newThread())
                .doOnNext((__) -> intervalCounter.feedValues(next, false)).take(1).subscribe();

        Thread.sleep(7 * 60 * 1000);
    }


    @Test
    public void startCount() throws Exception {

        TimeUnit period = TimeUnit.SECONDS;
        testScheduler.advanceTimeBy(1, period);

        FeedingIntervalCounterImpl intervalCounter = new FeedingIntervalCounterImpl(testScheduler, 0,
                10, period, CountValue.FORWARD);

        TestSubscriber<CountValue> test = intervalCounter
                .startCount(CountValue.startFlag(0, 0))
                .test();
        intervalCounter.feedValues(Arrays.asList(
                OwnedCountValue
                        .from(CountValue.startAtMin(0, 10, period, CountValue.FORWARD), "foo1")
                , OwnedCountValue
                        .from(new CountValue(3, 0, 10, period, CountValue.FORWARD), "foo2")
        ), false);
        testScheduler.advanceTimeBy(3 * 1200, TimeUnit.MILLISECONDS);
        intervalCounter.feedValues(Collections.singletonList(
                OwnedCountValue
                        .from(new CountValue(0, 0, 10, period, CountValue.FORWARD), "foo1")), false);
        testScheduler.advanceTimeBy(4 * 1000 + 700, TimeUnit.MILLISECONDS);
        intervalCounter.feedValues(Arrays.asList(
                OwnedCountValue
                        .from(CountValue.startAtMin(0, 10, period, CountValue.FORWARD), "foo1")
                , OwnedCountValue
                        .from(new CountValue(5, 0, 10, period, CountValue.FORWARD), "foo3")
        ), false);

        testScheduler.advanceTimeTo(100, period);
        for (CountValue c : test.values()) {
            System.out.println(!c.isFlag() ? ((CompositeCountValue) c).getCountValues() : c);
        }
        test.assertNoErrors();
    }


    @Test
    public void startAndRemoveCount() throws Exception {

        TimeUnit period = TimeUnit.SECONDS;
        testScheduler.advanceTimeBy(1, period);

        FeedingIntervalCounterImpl intervalCounter = new FeedingIntervalCounterImpl(testScheduler, 0,
                10, period, CountValue.FORWARD);

        TestSubscriber<CountValue> test = intervalCounter
                .startCount(CountValue.startFlag(0, 0))
                .test();
        intervalCounter.feedValues(Arrays.asList(
                OwnedCountValue
                        .from(CountValue.startAtMin(0, 10, period, CountValue.FORWARD), "foo1")
                , OwnedCountValue
                        .from(new CountValue(3, 0, 10, period, CountValue.FORWARD), "foo2")
        ), false);
        testScheduler.advanceTimeBy(3 * 1200, TimeUnit.MILLISECONDS);
        intervalCounter.removeFromFeed(OwnedCountValue
                .from(CountValue.startAtMin(0, 10, period, CountValue.FORWARD), "foo1"));
        intervalCounter.feedValues(Arrays.asList(
                OwnedCountValue
                        .from(new CountValue(5, 0, 10, period, CountValue.FORWARD), "foo3")
        ), false);

        testScheduler.advanceTimeTo(100, period);
        for (CountValue c : test.values()) {
            System.out.println(!c.isFlag() ? ((CompositeCountValue) c).getCountValues() : c);
        }
        test.assertNoErrors();
    }

    @Test
    public void startCount2() throws Exception {
        TimeUnit period = TimeUnit.SECONDS;
        testScheduler.advanceTimeBy(1, period);

        FeedingIntervalCounterImpl intervalCounter = new FeedingIntervalCounterImpl(testScheduler, 0,
                10, period, CountValue.FORWARD);

        TestSubscriber<CountValue> test = intervalCounter
                .startCount(CountValue.startFlag(0, 0))
                .test();

        intervalCounter.feedValues(Arrays.asList(
                OwnedCountValue
                        .from(new CountValue(10, 0, 10, period, CountValue.FORWARD), "foo1")), false
        );
        testScheduler.advanceTimeTo(100, period);
        for (CountValue c : test.values()) {
            System.out.println(!c.isFlag() ? ((CompositeCountValue) c).getCountValues() : c);
        }

    }

    @Test
    public void test_offline_behavior() throws Exception {
        TimeUnit period = TimeUnit.SECONDS;
        testScheduler.advanceTimeBy(1, period);
        FeedingIntervalCounterImpl intervalCounter = new FeedingIntervalCounterImpl(testScheduler, 0,
                10, period, CountValue.FORWARD);
        assertTrue(intervalCounter.isOffline());

        //offline feed
        intervalCounter.feedValues(Arrays.asList(
                OwnedCountValue
                        .from(new CountValue(10, 0, 10, period, CountValue.FORWARD), "foo1")), false
        );

        TestSubscriber<CountValue> test = intervalCounter
                .startCount(CountValue.startFlag(0, 0))
                .test();

        assertFalse(intervalCounter.isOffline());

        //online feed - after subscription
        intervalCounter.feedValues(Arrays.asList(
                OwnedCountValue
                        .from(new CountValue(5, 0, 10, period, CountValue.FORWARD), "foo2")), false
        );
        testScheduler.advanceTimeTo(100, period);
        for (CountValue c : test.values()) {
            System.out.println(!c.isFlag() ? ((CompositeCountValue) c).getCountValues() : c);
        }

    }
}