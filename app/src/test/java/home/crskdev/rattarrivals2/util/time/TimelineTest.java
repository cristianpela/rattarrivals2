package home.crskdev.rattarrivals2.util.time;

import org.junit.Test;

import java.util.Iterator;
import java.util.concurrent.TimeUnit;

import home.crskdev.rattarrivals2.BaseTest;

import static org.junit.Assert.assertEquals;

/**
 * .
 * Created by criskey on 23/10/2017.
 */
public class TimelineTest extends BaseTest {
    @Test
    public void nextTick() throws Exception {

        long now = testScheduler.now(TimeUnit.MILLISECONDS);
        Timeline timeline = new Timeline(now, TimeUnit.MINUTES);

        testScheduler.advanceTimeBy(4, TimeUnit.MINUTES);
        testScheduler.advanceTimeBy(40, TimeUnit.SECONDS);

        Timeline.Tick nextTick = timeline.nextTickFrom(testScheduler.now(TimeUnit.MILLISECONDS));

        assertEquals(5, TimeUnit.MILLISECONDS.toMinutes(nextTick.time));
        assertEquals(20, TimeUnit.MILLISECONDS.toSeconds(nextTick.until));

    }

    @Test
    public void nextTick2() throws Exception {

        Timeline timeline = new Timeline(0, TimeUnit.SECONDS);


        Timeline.Tick nextTick = timeline.nextTickFrom(3 * 1000 + 500 + 200);

        assertEquals(4000, nextTick.time);
        assertEquals(300, nextTick.until);

        Timeline.Tick prevTick = timeline.previousTickFrom(3 * 1000 + 500 + 200);

        assertEquals(3000, prevTick.time);
        assertEquals(700, prevTick.until);

        nextTick = timeline.nextTickFrom(3 * 1000);
        assertEquals(4000, nextTick.time);
        assertEquals(1000, nextTick.until);

        prevTick = timeline.previousTickFrom(3 * 1000);

        assertEquals(2000, prevTick.time);
        assertEquals(1000, prevTick.until);


    }

    @Test
    public void with_resume_count_value() throws Exception {

        Timeline timeline = new Timeline(1000, TimeUnit.SECONDS);
        ResumableCountValue rcv = ResumableCountValue.from(CountValue
                .startAtMin(0, 10, TimeUnit.SECONDS, CountValue.FORWARD), timeline.getOrigin());

        Timeline.Tick tick = timeline.nextTickFrom(timeline.getOrigin() +44000 + 700);
        CountValue resume = rcv.resumeAt(tick.time);

        System.out.println(resume);
    }

    @Test
    public void iterateTick() throws Exception {
        Timeline timeline = new Timeline(1000, TimeUnit.SECONDS);

        for (Iterator<Timeline.Tick> it = timeline.iterateToOrigin(10 * 1000 + 200); it.hasNext(); ) {
            System.out.println(it.next());
        }
    }


}