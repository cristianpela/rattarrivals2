package home.crskdev.rattarrivals2.arrivals.repository;

import android.widget.ScrollView;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import home.crskdev.rattarrivals2.BaseTest;
import home.crskdev.rattarrivals2.arrivals.ArrivalsConverter;
import home.crskdev.rattarrivals2.arrivals.model.Arrival;
import home.crskdev.rattarrivals2.arrivals.model.ArrivalExtra;
import home.crskdev.rattarrivals2.lines.model.LineType;
import home.crskdev.rattarrivals2.util.ImmLists;
import home.crskdev.rattarrivals2.util.time.TimeEstimated;
import io.reactivex.Single;
import io.reactivex.observers.TestObserver;

import static home.crskdev.rattarrivals2.arrivals.ArrivalsConverter.composeWithLineIgnored;
import static home.crskdev.rattarrivals2.arrivals.ArrivalsConverter.toDTOFromDTOExtra;
import static home.crskdev.rattarrivals2.arrivals.repository.ArrivalRepositoryConverter.toEntityExtraFromDTOExtra;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doReturn;

/**
 * Created by criskey on 21/9/2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class ArrivalRepositoryImpl2Test extends BaseTest {

    @Mock
    ArrivalDAO dao;
    ArrivalRepository svc;

    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
        svc = new ArrivalRepositoryImpl(dao);
    }

    @Test
    public void observePin() throws Exception {
        ArrivalExtraEntity extra = new ArrivalExtraEntity(
                1886, "Stuparilor", "Grozavescu", "Foo", LineType.AUTO, false, 1234L, 1L);
        doReturn(extra).when(dao).selectOne(anyLong(), anyString(), anyString());

        TestObserver<List<ArrivalExtra>> test = svc.observePin().test();

        Arrival arrival = new Arrival(1886, "Stuparilor", TimeEstimated.nonEstimated(1234L), "Grozavescu", 1L);
        svc.pin(composeWithLineIgnored(arrival)).subscribe();

        assertEquals(arrival, toDTOFromDTOExtra(test.values().get(0).get(0)));
    }

    @Test
    public void observeUnpin() throws Exception {
        TestObserver<ArrivalExtra> test = svc.observeUnpin().test();
        Arrival arrival = new Arrival(1886, "Stuparilor",  TimeEstimated.nonEstimated(1234L), "Grozavescu", 1L);
        svc.unpin(composeWithLineIgnored(arrival)).subscribe();
        assertEquals(arrival.getId(), test.values().get(0).getId());
    }

    @Test
    public void getAll() throws Exception {
        ArrivalExtra extra = new ArrivalExtra(
                1886, "Stuparilor", "Grozavescu", "Foo", LineType.AUTO, false,  TimeEstimated.nonEstimated(1234L), 1L);
        doReturn(Single.just(Collections.singletonList(toEntityExtraFromDTOExtra(extra))))
                .when(dao).getAllExtraSingle();
        List<ArrivalExtra> arrivalExtras = svc.getAllExtra().test().values().get(0);
        assertEquals(Collections.singletonList(extra), arrivalExtras);
    }

    @Test
    public void tryRefresh() throws Exception {
        doReturn(Arrays.asList(
                new ArrivalExtraEntity(
                        1886, "Stuparilor", "Grozavescu", "Foo", LineType.AUTO, false, 1234L, 1L),
                new ArrivalExtraEntity(
                        1887, "Stuparilor2", "Grozavescu2", "Foo", LineType.AUTO, false, 1234L, 1L)
        )).when(dao).selectSome(anyList(), anyList(), anyList());

        TestObserver<List<ArrivalExtra>> testObserverPin = svc.observePin().test();

        Arrival arrUp1 = new Arrival(1886, "Stuparilor",TimeEstimated.nonEstimated(3333L), "Grozavescu", 2L);
        Arrival arrUp2 = new Arrival(1887, "Stuparilor2", TimeEstimated.nonEstimated(4444L), "Grozavescu2", 5L);
        final List<Arrival> inArrivals = Arrays.asList(
                new Arrival(3000, "Foo", TimeEstimated.nonEstimated(54652L), "Bar", 12122L), //not saved in db viewModel
                new Arrival(4000, "Zoo", TimeEstimated.nonEstimated(23225L), "Nar", 24555L), //not saved in db viewModel
                arrUp1,
                arrUp2);
        final TestObserver<List<ArrivalExtra>> testObserverReturn = svc
                .tryRefresh(ImmLists.map(inArrivals, ArrivalsConverter::composeWithLineIgnored)).test();

        assertEquals(Arrays.asList(arrUp1, arrUp2), testObserverPin.values().get(0)
                .stream().map(ArrivalsConverter::toDTOFromDTOExtra).collect(Collectors.toList()));


        final List<ArrivalExtra> actual = testObserverReturn.values().get(0);
        assertEquals(ImmLists.mutableMap(inArrivals, ArrivalsConverter::composeWithLineIgnored), actual);
        assertEquals(true, actual.get(2).pinned);
        assertEquals(true, actual.get(3).pinned);
    }

}