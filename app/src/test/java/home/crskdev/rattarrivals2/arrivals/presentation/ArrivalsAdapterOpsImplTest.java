package home.crskdev.rattarrivals2.arrivals.presentation;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import home.crskdev.rattarrivals2.BaseTest;
import home.crskdev.rattarrivals2.arrivals.model.ArrivalViewModel;
import home.crskdev.rattarrivals2.model.BaseViewModel;
import home.crskdev.rattarrivals2.util.time.TimeEstimated;

import static org.junit.Assert.*;

/**
 * Created by criskey on 2/10/2017.
 */
public class ArrivalsAdapterOpsImplTest extends BaseTest {

    private ArrivalsAdapterOps adapterOps;

    private List<ArrivalViewModel> arrivals;

    @Override
    public void setUp() throws Exception {
        super.setUp();
        final long id = 1886L;
        adapterOps = new ArrivalsAdapterOpsImpl();
        arrivals = Arrays.asList(
                new ArrivalViewModel(id, "Stuparilor", "Grozavescu", 1L, "1L", 0,  mockEstimated(), 2L, "2L", false),
                new ArrivalViewModel(id, "Holdelor", "Grozavescu", 2L, "", 0, mockEstimated(),2L, "", false),
                new ArrivalViewModel(id, "Lipovei", "Grozavescu", 3L, "", 0, mockEstimated(),2L, "", false),
                new ArrivalViewModel(id, "Linistii", "Grozavescu", 4L, "", 0, mockEstimated(),2L, "", false),
                new ArrivalViewModel(id, "Eroilor", "Grozavescu", 5L, "", 0, mockEstimated(),2L, "", false)
        );
        adapterOps.setItems(arrivals);
    }

    private TimeEstimated mockEstimated(){
        return TimeEstimated.nonEstimated(0L);
    }

    @Test
    public void setArrivals() throws Exception {
        assertEquals(arrivals, adapterOps
                .itemStream()
                .collect(Collectors.toList()));
    }

    @Test
    public void setArrivals_when_allreadyHave_data() throws Exception {
        adapterOps.setItems(arrivals);
        assertEquals(arrivals, adapterOps
                .itemStream()
                .collect(Collectors.toList()));
    }

    @Test
    public void setEnabled() throws Exception {
        adapterOps.changePropertyAll(BaseViewModel.PROPERTY_ENABLED, false);
        assertEquals(
                Stream.generate(() -> false)
                        .limit(adapterOps.getItemCount())
                        .collect(Collectors.toList()),
                adapterOps.itemStream()
                        .map(i -> i.enabled)
                        .collect(Collectors.toList()));
    }

    @Test
    public void changePinnedArrival() throws Exception {
        final int index = 1;
        final ArrivalViewModel get = arrivals.get(index);
        final ArrivalViewModel updatedArrival = new ArrivalViewModel(get.lineId, get.stationName,
                get.to, 10L, "", 0, mockEstimated(), 600L,  "", true);
        adapterOps.upsert(updatedArrival);

        final ArrivalViewModel itemAt = adapterOps.getItemAt(index);
        assertEquals(updatedArrival.pinned, itemAt.pinned);
        assertEquals(updatedArrival.lastUpdatedStr, itemAt.lastUpdatedStr);
        assertEquals(updatedArrival.timeStr, itemAt.timeStr);
    }

}