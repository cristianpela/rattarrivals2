package home.crskdev.rattarrivals2.arrivals.repository;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import home.crskdev.rattarrivals2.arrivals.ArrivalsConverter;
import home.crskdev.rattarrivals2.arrivals.model.Arrival;
import home.crskdev.rattarrivals2.util.time.TimeEstimated;

import static org.junit.Assert.*;

/**
 *
 * Created by criskey on 13/10/2017.
 */
public class ArrivalRepositoryConverterTest {
    @Test
    public void extractCompositeKeysFromList() throws Exception {
        final List<Arrival> list = Arrays.asList(
                new Arrival(1L, "name1", "to1", TimeEstimated.nonEstimated(1), 1, true),
                new Arrival(2L, "name2", "to2", TimeEstimated.nonEstimated(1), 1, true),
                new Arrival(3L, "name3", "to3", TimeEstimated.nonEstimated(1), 1, true),
                new Arrival(4L, "name4", "to4", TimeEstimated.nonEstimated(1), 1, true)
        );
        final List<List<?>> keys = ArrivalRepositoryConverter.extractCompositeKeysFromList(list);
        assertEquals(Arrays.asList(1L, 2L, 3L, 4L), new ArrayList<>(keys.get(0)));
        assertEquals(Arrays.asList("name1", "name2", "name3", "name4"),  new ArrayList<>(keys.get(1)));
        assertEquals(Arrays.asList("to1", "to2", "to3", "to4"),  new ArrayList<>(keys.get(2)));
    }

}