package home.crskdev.rattarrivals2.arrivals.services;

import org.junit.Test;

import java.util.List;
import java.util.TimeZone;

import home.crskdev.rattarrivals2.BaseTest;
import home.crskdev.rattarrivals2.arrivals.model.Arrival;
import home.crskdev.rattarrivals2.arrivals.services.ArrivalParser;
import home.crskdev.rattarrivals2.net.Client;
import home.crskdev.rattarrivals2.net.FileClient;
import home.crskdev.rattarrivals2.util.time.TimeConverter;

import static junit.framework.Assert.assertEquals;

/**
 * Created by criskey on 20/8/2017.
 */
public class ArrivalParserTest extends BaseTest {


    @Test
    public void parseArrival() throws Exception {

        final long lastUpdated = 0L;
        final long lineId = 886L;

        Client client = new FileClient(loadFileFromResources("arrival_bus_40.html"));
        ArrivalParser parser = new ArrivalParser(client.request(""), lineId, lastUpdated,
                new TimeConverter(lastUpdated, TimeZone.getTimeZone("Europe/Bucharest")));

        List<Arrival> arrivals = parser.parseArrivals();

        //todo fix test
        
//        String expected ="[Arrival{lineId=886, stationName='T. Grozăvescu', timeStr='77100000', to='Stuparilor', lastUpdatedStr=0, pinned=false}, " +
//                "Arrival{lineId=886, stationName='Oituz', timeStr='76260000', to='Stuparilor', lastUpdatedStr=0, pinned=false}, " +
//                "Arrival{lineId=886, stationName='P-ța C. Europei', timeStr='120000', to='Stuparilor', lastUpdatedStr=0, pinned=false}, " +
//                "Arrival{lineId=886, stationName='Dacia Service', timeStr='0', to='Stuparilor', lastUpdatedStr=0, pinned=false}, " +
//                "Arrival{lineId=886, stationName='Div 9 Cavalerie', timeStr='180000', to='Stuparilor', lastUpdatedStr=0, pinned=false}, " +
//                "Arrival{lineId=886, stationName='Complex Terra', timeStr='240000', to='Stuparilor', lastUpdatedStr=0, pinned=false}, " +
//                "Arrival{lineId=886, stationName='Gara de Est', timeStr='360000', to='Stuparilor', lastUpdatedStr=0, pinned=false}, " +
//                "Arrival{lineId=886, stationName='Stuparilor', timeStr='76200000', to='Stuparilor', lastUpdatedStr=0, pinned=false}, " +
//                "Arrival{lineId=886, stationName='Stuparilor', timeStr='76200000', to='T. Grozăvescu', lastUpdatedStr=0, pinned=false}, " +
//                "Arrival{lineId=886, stationName='Holdelor', timeStr='76320000', to='T. Grozăvescu', lastUpdatedStr=0, pinned=false}, " +
//                "Arrival{lineId=886, stationName='Ap. Petru și Pavel', timeStr='76380000', to='T. Grozăvescu', lastUpdatedStr=0, pinned=false}, " +
//                "Arrival{lineId=886, stationName='Pomiculturii', timeStr='76500000', to='T. Grozăvescu', lastUpdatedStr=0, pinned=false}, " +
//                "Arrival{lineId=886, stationName='Div 9 Cavalerie', timeStr='76560000', to='T. Grozăvescu', lastUpdatedStr=0, pinned=false}, " +
//                "Arrival{lineId=886, stationName='P-ța C. Europei', timeStr='76800000', to='T. Grozăvescu', lastUpdatedStr=0, pinned=false}, " +
//                "Arrival{lineId=886, stationName='T. Grozăvescu', timeStr='77100000', to='T. Grozăvescu', lastUpdatedStr=0, pinned=false}]";
//
//        assertEquals(expected, arrivals.toString());

    }


}