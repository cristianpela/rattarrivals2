package home.crskdev.rattarrivals2.arrivals.presentation;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import home.crskdev.rattarrivals2.BaseTest;
import home.crskdev.rattarrivals2.arrivals.ArrivalsConverter;
import home.crskdev.rattarrivals2.arrivals.model.ArrivalExtra;
import home.crskdev.rattarrivals2.arrivals.model.ArrivalExtraViewModel;
import home.crskdev.rattarrivals2.arrivals.model.ArrivalViewModel;
import home.crskdev.rattarrivals2.arrivals.services.ArrivalParseService;
import home.crskdev.rattarrivals2.model.Action;
import home.crskdev.rattarrivals2.arrivals.model.Arrival;
import home.crskdev.rattarrivals2.lines.model.Line;
import home.crskdev.rattarrivals2.lines.model.LineType;
import home.crskdev.rattarrivals2.model.Result;
import home.crskdev.rattarrivals2.arrivals.model.Route;
import home.crskdev.rattarrivals2.arrivals.repository.ArrivalRepository;
import home.crskdev.rattarrivals2.lines.repository.LineRepository;
import home.crskdev.rattarrivals2.util.CountdownCooldown;
import home.crskdev.rattarrivals2.util.FluentMap;
import home.crskdev.rattarrivals2.util.ImmLists;
import home.crskdev.rattarrivals2.util.ResourceFinder;
import home.crskdev.rattarrivals2.util.time.CountValue;
import home.crskdev.rattarrivals2.util.time.CountValueCacheContainer;
import home.crskdev.rattarrivals2.util.time.IntervalCounter;
import home.crskdev.rattarrivals2.util.time.TimeEstimated;
import io.reactivex.BackpressureStrategy;
import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.schedulers.TestScheduler;
import io.reactivex.subjects.BehaviorSubject;

import static home.crskdev.rattarrivals2.arrivals.ArrivalsConverter.composeWithLineIgnored;
import static home.crskdev.rattarrivals2.arrivals.ArrivalsConverter.toVMExtraFromDTOExtra;
import static home.crskdev.rattarrivals2.model.Action.ChangeWayAction;
import static home.crskdev.rattarrivals2.model.Action.FavoriteAction;
import static home.crskdev.rattarrivals2.model.Action.LoadAction;
import static home.crskdev.rattarrivals2.model.Action.Pinned;
import static home.crskdev.rattarrivals2.util.time.TimeUtils.DEBOUNCE_TIME_VALUE;
import static home.crskdev.rattarrivals2.util.time.TimeUtils.THROTTLE_TIME_VALUE;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 * Created by criskey on 20/8/2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class ArrivalsPresenterTest extends BaseTest {

    private final long id = 1886L;

    private final int startValueCooldown = (int) THROTTLE_TIME_VALUE.value;
    @Mock
    ArrivalRepository arrivalRepository;
    @Mock
    ArrivalParseService arrivalParseService;
    @Mock
    LineRepository lineRepository;
    @Mock
    ArrivalsView view;
    @Mock
    ResourceFinder resourceFinder;
    @Mock
    IntervalCounter countDown;

    private ArrivalsPresenter presenter;
    private Line line = new Line(id, "40", LineType.AUTO, true);

    private TestScheduler externalTestScheduler = new TestScheduler();
    /**
     * Scheduler used by presenter internally (the computation scheduler)
     * Do not use it in other streams. Use externalTestScheduler or other test scheduler
     * like countdown, intervals etc..
     */
    private TestScheduler internalTestScheduler = new TestScheduler();

    @Override
    public void setUp() throws Exception {
        super.setUp();
        presenter = new ArrivalsPresenter(Schedulers.trampoline(),
                arrivalParseService,
                arrivalRepository,
                lineRepository,
                new CountValueCacheContainer(),
                resourceFinder,
                countDown);
        presenter.computationScheduler = internalTestScheduler;
    }

    @Test
    public void should_load_line_by_given_id_from_extras() throws Exception {
        doReturn(Single.just(line)).when(lineRepository).getLinesById(id);
        doReturn(Flowable.never()).when(lineRepository).observeChanges();

        //noinspection unchecked
        presenter.attachView(view, Collections.emptyMap(), new FluentMap<>().put("KEY_LINE_ID", id).get());
        verify(view).onLoadLine(line);
    }

    @Test
    public void should_toggle_favorite() throws Exception {
        doReturn(Single.just(line)).when(lineRepository).getLinesById(anyLong());

        final BehaviorSubject<Line> changes = BehaviorSubject.create();
        doReturn(changes.toFlowable(BackpressureStrategy.LATEST))
                .when(lineRepository).observeChanges();
        doAnswer(new Answer<Completable>() {
            @Override
            public Completable answer(InvocationOnMock invocation) throws Throwable {
                // we only need this for our first invocation further values will be handled by subject
                Line lineInvoke = invocation.getArgument(0);
                Line linePrev = changes.getValue();
                Line nextLine = (linePrev != null) ? linePrev.toggle() : lineInvoke;
                changes.onNext(nextLine);
                return Completable.complete();
            }
        }).when(lineRepository).update(any(Line.class));

        //noinspection unchecked
        presenter.attachView(view, Collections.emptyMap(), new FluentMap<>().put("KEY_LINE_ID", id).get());
        presenter.bindActions(Observable.fromArray(//me mimic favorite toggle button
                new FavoriteAction(id),
                new FavoriteAction(id)
        ));


        ArgumentCaptor<Line> captor = ArgumentCaptor.forClass(Line.class);
        verify(view, times(2)).onChangeFavoriteLine(captor.capture());

        assertEquals(Arrays.asList(false, true),
                captor.getAllValues().stream().map(l -> l.favorite).collect(Collectors.toList()));

    }

    @Test
    public void should_fetch_route_and_pin_its_arrivals_if_they_exists() throws Exception {
        doReturn(Single.just(line)).when(lineRepository).getLinesById(id);
        doReturn(Flowable.never()).when(lineRepository).observeChanges();
        doReturn(Flowable.never()).when(countDown).startCount(any(CountValue.class));


        final String from = "Stuparilor";
        final String to = "Grozavescu";
        final long lastUpdated = 2L;
        List<Arrival> fetchedArrivals = Arrays.asList(
                new Arrival(id, from, to, TimeEstimated.nonEstimated(1L), lastUpdated, false),
                new Arrival(id, "Holdelor", to, TimeEstimated.nonEstimated(2L), lastUpdated, false),
                new Arrival(id, "Lipovei", to, TimeEstimated.nonEstimated(3L), lastUpdated, false),
                new Arrival(id, "Linistii", to, TimeEstimated.nonEstimated(4L), lastUpdated, false),
                new Arrival(id, "Eroilor", to, TimeEstimated.nonEstimated(5L), lastUpdated, false)
        );

        //first 2 stations should be pinned will be pinned
        List<Arrival> updatedArrivals = ImmLists.map(fetchedArrivals, a -> {
            if (a.stationName.equals(from) || a.stationName.equals("Holdelor")) {
                return a.pinned(true);
            }
            return a;
        });

        Route fetchedRoute = new Route(id, from, to, fetchedArrivals, lastUpdated);
        doReturn(Observable.just(new Result.RouteResult(fetchedRoute))).when(arrivalParseService)
                .fetchRoute(any(LoadAction.class));
        doReturn(Single.just(ImmLists.map(updatedArrivals, ArrivalsConverter::composeWithLineIgnored)))
                .when(arrivalRepository)
                .tryRefresh(anyList());

        //noinspection unchecked
        presenter.attachView(view, Collections.emptyMap(), new FluentMap<>().put("KEY_LINE_ID", id).get());

        presenter.bindActions(Observable.just(new LoadAction(id)));
        externalTestScheduler.advanceTimeTo(startValueCooldown - 1, TimeUnit.SECONDS);

        @SuppressWarnings("unchecked")
        ArgumentCaptor<List<ArrivalViewModel>> updateArrivalsCaptor = ArgumentCaptor.forClass(List.class);
        verify(view).onWait();
        verify(view).onDisplayArrivals(updateArrivalsCaptor.capture(), eq(from), eq(to));
        verify(view).onDone();

        //countdown matching
        ArgumentCaptor<Integer> cooldownCaptor = ArgumentCaptor.forClass(Integer.class);
//        //todo 30 or 31 times
//        verify(view, times(startValueCooldown + 1)).onCoolDown(cooldownCaptor.capture(), eq(startValueCooldown), anyBoolean());
//        assertEquals(IntStream.range(0, startValueCooldown)
//                .map(i -> startValueCooldown - i) // reverse order
//                .boxed()
//                .collect(Collectors.toList()), cooldownCaptor.getAllValues().subList(1, cooldownCaptor.getAllValues().size()));

        //pinned check list - first 2 stations should be pinned
        assertEquals(Arrays.asList(true, true, false, false, false),
                updateArrivalsCaptor.getValue().stream().map(a -> a.pinned).collect(Collectors.toList()));

    }

    @Test
    public void should_change_ways() throws Exception {
        doReturn(Single.just(line)).when(lineRepository).getLinesById(id);
        doReturn(Flowable.never()).when(lineRepository).observeChanges();

        final String from = "Stuparilor";
        final String to = "Grozavescu";
        final long lastUpdated = 2L;

        List<Arrival> toArrivals = Arrays.asList(
                new Arrival(id, from, to, TimeEstimated.nonEstimated(1L), lastUpdated, false),
                new Arrival(id, "Holdelor", to, TimeEstimated.nonEstimated(2L), lastUpdated, false),
                new Arrival(id, "Lipovei", to, TimeEstimated.nonEstimated(3L), lastUpdated, false),
                new Arrival(id, "Linistii", to, TimeEstimated.nonEstimated(4L), lastUpdated, false),
                new Arrival(id, "Eroilor", to, TimeEstimated.nonEstimated(5L), lastUpdated, false)
        );

        List<Arrival> fromArrivals = Arrays.asList(
                new Arrival(id, from, to, TimeEstimated.nonEstimated(1L), lastUpdated, false),
                new Arrival(id, "Popa Sapca", from, TimeEstimated.nonEstimated(2L), lastUpdated, false),
                new Arrival(id, "Galeria", from, TimeEstimated.nonEstimated(3L), lastUpdated, false),
                new Arrival(id, "Kappa", from, TimeEstimated.nonEstimated(4L), lastUpdated, false),
                new Arrival(id, "Dedeman", from, TimeEstimated.nonEstimated(5L), lastUpdated, false)
        );

        //first 2 stations should be pinned will be pinned
        List<Arrival> updatedToArrivals = ImmLists.map(toArrivals, a -> {
            if (a.stationName.equals("Stuparilor") ||
                    a.stationName.equals("Holdelor")) return a.pinned(true);
            return a;
        });
        List<Arrival> updatedFromArrivals = ImmLists.map(fromArrivals, a -> {
            if (a.stationName.equals("Kappa")) return a.pinned(true);
            return a;
        });

        //todo this is too hacky
        boolean toWay[] = {true};
        doAnswer(invocation -> {
            List<Arrival> answer = toWay[0] ? toArrivals : fromArrivals;
            String f = toWay[0] ? from : to;
            String t = toWay[0] ? to : from;
            toWay[0] = !toWay[0];
            return Observable.just(new Result.RouteResult(new Route(id, f, t, answer, 1L)));
        }).when(arrivalParseService)
                .changeWay();

        boolean toUpdatedWay[] = {true};
        doAnswer(invocation -> {
            List<Arrival> answer = toWay[0] ? updatedToArrivals : updatedFromArrivals;
            toUpdatedWay[0] = !toUpdatedWay[0];
            return Single.just(ImmLists.map(answer, ArrivalsConverter::composeWithLineIgnored));
        }).when(arrivalRepository)
                .tryRefresh(anyList());

        //noinspection unchecked
        presenter.attachView(view, Collections.emptyMap(), new FluentMap<>().put("KEY_LINE_ID", id).get());

        presenter.bindActions(Observable.fromArray(
                ChangeWayAction.CHANGE_WAY_ACTION,
                ChangeWayAction.CHANGE_WAY_ACTION,
                ChangeWayAction.CHANGE_WAY_ACTION
        ));

        @SuppressWarnings("unchecked")
        ArgumentCaptor<List<ArrivalViewModel>> updateArrivalsCaptor = ArgumentCaptor.forClass(List.class);
        ArgumentCaptor<String> fromCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<String> toCaptor = ArgumentCaptor.forClass(String.class);

        verify(view, times(3)).onDisplayArrivals(updateArrivalsCaptor.capture(), fromCaptor.capture(),
                toCaptor.capture());

        final Boolean mustHaveAtLeastOnePinned = updateArrivalsCaptor
                .getAllValues()
                .stream()
                .flatMap(l -> l.stream().map(i -> i.pinned))
                .reduce((acc, curr) -> acc | curr)
                .orElse(false);
        assertTrue(mustHaveAtLeastOnePinned);
        assertEquals(Arrays.asList(updatedFromArrivals, updatedToArrivals, updatedFromArrivals),
                updateArrivalsCaptor.getAllValues()
                        .stream()
                        .map(i -> ImmLists.map(i, ArrivalsConverter::toDTOFromVM))
                        .collect(Collectors.toList()));
        assertEquals(Arrays.asList(from, to, from), fromCaptor.getAllValues());
        assertEquals(Arrays.asList(to, from, to), toCaptor.getAllValues());
    }

    @Test
    public void should_change_pinned_station() throws Exception {

        doReturn(Single.never()).when(lineRepository).getLinesById(id);
        doReturn(Flowable.never()).when(lineRepository).observeChanges();

        doAnswer((invocation) -> Single.just(invocation.getArgument(0)))
                .when(arrivalRepository).pin(any(ArrivalExtra.class));
        doAnswer((invocation) -> Single.just(invocation.getArgument(0)))
                .when(arrivalRepository).unpin(any(ArrivalExtra.class));

        final Arrival arrival = new Arrival(id, "Stuparilor", "Grozavescu", TimeEstimated.nonEstimated(1L), 2L, false);
        //noinspection unchecked
        presenter.attachView(view, Collections.emptyMap(), new FluentMap<>().put("KEY_LINE_ID", id).get());

        //simulate clicks on a station viewModel to pin/unpin
        final int CLICKS = 4;

        //alternation stream pinned state -> true, false, true, ...
        //because internally is called togglePinned too
        TestScheduler intervalTestScheduler = new TestScheduler();
        final ArrivalExtraViewModel initial = toVMExtraFromDTOExtra(composeWithLineIgnored(arrival),resourceFinder, false);
        presenter.bindActions(Observable
                .interval(DEBOUNCE_TIME_VALUE.value, DEBOUNCE_TIME_VALUE.unit, intervalTestScheduler)
                .scan(new Pinned(initial),
                        (acc, __) -> new Pinned(acc.viewModel.togglePinned()))
                .take(CLICKS)
                .cast(Action.class));

        //advance in intervals given by DEBOUNCE_TIME_VALUE
        IntStream.rangeClosed(1, CLICKS).forEach(__ -> {
            internalTestScheduler.advanceTimeBy(DEBOUNCE_TIME_VALUE.value, DEBOUNCE_TIME_VALUE.unit);
            intervalTestScheduler.advanceTimeBy(DEBOUNCE_TIME_VALUE.value, DEBOUNCE_TIME_VALUE.unit);
        });

        ArgumentCaptor<ArrivalViewModel> pinnedCaptor = ArgumentCaptor.forClass(ArrivalViewModel.class);
        verify(view, times(CLICKS)).onChangePinnedArrival(pinnedCaptor.capture());

        //expected alternation stream pinned state -> true, false, true, ...
        assertEquals(IntStream.rangeClosed(1, CLICKS).boxed()
                        .map(i -> i % 2 != 0).collect(Collectors.toList()),
                pinnedCaptor.getAllValues().stream()
                        .map(a -> a.pinned).collect(Collectors.toList()));

    }

    @Test
    public void should_go_back() throws Exception {
        doReturn(Single.never()).when(lineRepository).getLinesById(id);
        doReturn(Flowable.never()).when(lineRepository).observeChanges();
        //noinspection unchecked
        presenter.attachView(view, Collections.emptyMap(), new FluentMap<>().put("KEY_LINE_ID", id).get());
        presenter.bindActions(Observable.just(Action.Back.BACK));

        verify(view).onGoBack();

    }
}