package home.crskdev.rattarrivals2.arrivals.services;

import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import home.crskdev.rattarrivals2.arrivals.model.Arrival;
import home.crskdev.rattarrivals2.arrivals.model.Route;
import home.crskdev.rattarrivals2.arrivals.services.ArrivalParseServiceImpl.InternalState;
import home.crskdev.rattarrivals2.util.time.TimeEstimated;

import static home.crskdev.rattarrivals2.arrivals.services.ArrivalParseServiceImpl.InternalState.TO1;
import static home.crskdev.rattarrivals2.arrivals.services.ArrivalParseServiceImpl.InternalState.TO2;
import static org.junit.Assert.*;

/**
 * Created by criskey on 23/8/2017.
 */
public class InternalStateTest {
    @Test
    public void next() throws Exception {
        InternalState state = new InternalState(TO1, Collections.emptyList());

        InternalState next = state.next();
        assertEquals(TO2, next.way);
        next = next.next();
        assertEquals(TO1, next.way);
        next = next.next();
        assertEquals(TO2, next.way);
        next = next.next();
        assertEquals(TO1, next.way);
    }

    @Test
    public void mapInternalStateToRoute() throws Exception {
        List<Arrival> arrivals = Arrays.asList(
                //way1
                new Arrival(1, "a", TimeEstimated.nonEstimated(0L), "to1", 2L),
                new Arrival(1, "b", TimeEstimated.nonEstimated(0L), "to1", 2L),
                //way2
                new Arrival(1, "b", TimeEstimated.nonEstimated(0L), "to2", 2L),
                new Arrival(1, "a", TimeEstimated.nonEstimated(0L), "to2", 2L)
        );

        InternalState state = new InternalState(arrivals);

        Route route = InternalState.mapInternalStateToRoute(state);
        assertEquals(1, route.lineId);
        assertEquals("to1", route.to);
        assertEquals("to2", route.from);
        assertEquals(
                Arrays.asList(
                        new Arrival(1, "a", TimeEstimated.nonEstimated(0L), "to1", 2L),
                        new Arrival(1, "b", TimeEstimated.nonEstimated(0L), "to1", 2L)
                ),
                route.arrivals
        );
        assertEquals(2L, route.lastUpdated);

        state = state.next();
        route = InternalState.mapInternalStateToRoute(state);
        assertEquals(1, route.lineId);
        assertEquals("to2", route.to);
        assertEquals("to1", route.from);
        assertEquals(
                Arrays.asList(
                        new Arrival(1, "b", TimeEstimated.nonEstimated(0L), "to2", 2L),
                        new Arrival(1, "a", TimeEstimated.nonEstimated(0L), "to2", 2L)
                ),
                route.arrivals
        );
        assertEquals(2L, route.lastUpdated);

    }

}