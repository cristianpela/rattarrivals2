package home.crskdev.rattarrivals2;

import org.junit.Before;

import java.io.File;
import java.net.URL;
import java.util.Collection;

import home.crskdev.rattarrivals2.util.asserts.Asserts;
import home.crskdev.rattarrivals2.util.asserts.ThreadingAsserts;
import io.reactivex.Scheduler;
import io.reactivex.plugins.RxJavaPlugins;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.schedulers.TestScheduler;
import timber.log.Timber;

/**
 * .
 * Created by criskey on 10/8/2017.
 */

public class BaseTest {

    static {

        Timber.plant(new Timber.Tree() {

            final int ERROR = 6;

            @Override
            protected void log(int priority, String tag, String message, Throwable t) {
               if(priority == ERROR){
                   System.err.println(tag+":"+t.getMessage());
               }else{
                   System.out.println(tag+":"+message);
               }
            }
        });

        Asserts.threadingAsserts = new ThreadingAsserts() {
            @Override
            public boolean assertIsOnMainThread() {
                return true;
            }

            @Override
            public boolean assertIsBackgroundThread() {
                return true;
            }
        };
    }


    public TestScheduler testScheduler = new TestScheduler();

    public Scheduler uiScheduler = Schedulers.trampoline();

    @Before
    public void setUp() throws Exception {
        Timber.tag(this.getClass().getSimpleName());
        RxJavaPlugins.setIoSchedulerHandler(__ -> Schedulers.trampoline());
        RxJavaPlugins.setComputationSchedulerHandler(__ -> testScheduler);
    }

    protected File loadFileFromResources(String fileName) {
        URL resource = getClass().getClassLoader().getResource(fileName);
        return new File(resource.getPath());
    }

    public <T> void printList(Collection<T> c) {
        for (T t : c) {
            System.out.println(t +"\n");
        }
    }

}
