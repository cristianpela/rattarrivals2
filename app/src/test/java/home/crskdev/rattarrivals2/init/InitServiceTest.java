package home.crskdev.rattarrivals2.init;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import java.lang.Error;
import java.util.Collections;
import java.util.List;

import home.crskdev.rattarrivals2.BaseTest;
import home.crskdev.rattarrivals2.lines.model.Line;
import home.crskdev.rattarrivals2.lines.model.LineType;
import home.crskdev.rattarrivals2.lines.services.LineParseService;
import home.crskdev.rattarrivals2.repository.CheckService;
import home.crskdev.rattarrivals2.lines.repository.LineRepository;
import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.observers.TestObserver;

import static home.crskdev.rattarrivals2.init.InitState.*;
import static home.crskdev.rattarrivals2.init.InitState.eq;
import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

/**
 * Created by criskey on 10/8/2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class InitServiceTest extends BaseTest {

    @Mock
    CheckService lineCheckService;

    @Mock
    LineParseService lineParseService;

    @Mock
    LineRepository lineRepository;

    InitService initService;


    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();

    }

    @Test
    public void it_should_download_when_check_is_false() {
        doReturn(Observable.just(Collections.emptyList()))
                .when(lineParseService).parseLine(any(LineType.class));
        doReturn(Completable.complete()).when(lineRepository).create(ArgumentMatchers.anyList());
        doReturn(Single.just(false)).when(lineCheckService).isInit();

        initService = new InitService(lineParseService, lineRepository, lineCheckService);

        TestObserver<InitState> test = initService.init().test();

        test.assertNoErrors();
        List<InitState> states = test.values();

        assertEquals(4, states.size());
        assertEquals(true, eq(states.get(0), Reset.class));
        for (int i = 1; i < 4; i++) {
            assertEquals(true, eq(states.get(i), Download.class));
        }
    }


    @Test
    public void it_should_skip_download_when_check_is_true() {
        doReturn(Observable.just(Collections.emptyList()))
                .when(lineParseService).parseLine(any(LineType.class));
        doReturn(Single.just(true)).when(lineCheckService).isInit();

        initService = new InitService(lineParseService, lineRepository, lineCheckService);

        TestObserver<InitState> test = initService.init().test();

        test.assertNoErrors();
        List<InitState> states = test.values();

        assertEquals(1, states.size());
        assertEquals(true, eq(states.get(0), Initialized.class));

    }

    @Test
    public void it_should_stream_error() {
        doReturn(Observable.error(new Error()))
                .when(lineParseService).parseLine(any(LineType.class));
        doReturn(Single.just(false)).when(lineCheckService).isInit();

        initService = new InitService(lineParseService, lineRepository, lineCheckService);

        TestObserver<InitState> test = initService.init().test();

        List<InitState> states = test.values();

        test.assertError(Error.class);

        assertEquals(1, states.size());
        assertEquals(true, eq(states.get(0), Reset.class));
    }


    @Test
    public void it_should_stream_error_and_download() {
        doAnswer(new Answer<Observable<List<Line>>>() {
            @Override
            public Observable<List<Line>> answer(InvocationOnMock invocation) throws Throwable {
                LineType type = invocation.getArgument(0);
                return (type == LineType.TRAM)
                        ? Observable.error(new Error())
                        : Observable.just(Collections.emptyList());
            }
        }).when(lineParseService).parseLine(any(LineType.class));
        doReturn(Completable.complete()).when(lineRepository).create(ArgumentMatchers.anyList());
        doReturn(Single.just(false)).when(lineCheckService).isInit();

        initService = new InitService(lineParseService, lineRepository, lineCheckService);

        TestObserver<InitState> test = initService.init().test();

        List<InitState> states = test.values();

        test.assertError(Error.class);
        assertEquals(2, states.size());
        assertEquals(true, eq(states.get(0), Reset.class));
        assertEquals(true, eq(states.get(1), Download.class));

    }
}
