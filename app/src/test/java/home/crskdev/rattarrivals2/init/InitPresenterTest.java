package home.crskdev.rattarrivals2.init;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collections;

import home.crskdev.rattarrivals2.BaseTest;
import home.crskdev.rattarrivals2.lines.model.LineType;
import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 * Created by criskey on 10/8/2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class InitPresenterTest extends BaseTest {

    @Mock
    InitService initSvc;

    @Mock
    InitView view;

    InitPresenter presenter;

    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
        presenter = new InitPresenter(Schedulers.trampoline(), initSvc);
        //test
    }

    @Test
    public void testView() throws Exception {
        doReturn(Observable.fromArray(
                new InitState.Reset(),
                new InitState.Download(LineType.AUTO, 1, 3),
                new InitState.Download(LineType.TRAM, 2, 3),
                new InitState.Download(LineType.TROLLEY, 3, 3)))
                .when(initSvc).init();
        presenter.attachView(view, Collections.emptyMap(), Collections.emptyMap());

        verify(view).onReset();
        verify(view).onDownloadAuto();
        verify(view).onDownloadTram();
        verify(view).onDownloadTrolley();
        verify(view).onInitDone();
        verify(view, times(4)).onDownloadStep(anyInt(), eq(3));
    }

    @Test
    public void testViewWhenChecked() throws Exception {
        doReturn(Observable.just(new InitState.Initialized()))
                .when(initSvc).init();
        presenter.attachView(view, Collections.emptyMap(), Collections.emptyMap());

        verify(view, times(0)).onReset();
    }


    @Test
    public void testViewError() throws Exception {
        doReturn(Observable.error(new Error("Foorror"))).when(initSvc).init();
        presenter.attachView(view,  Collections.emptyMap(), Collections.emptyMap());

        verify(view).onError(anyString());

    }
}