package home.crskdev.rattarrivals2.repository;

import android.support.annotation.Nullable;

import java.util.Map;
import java.util.Set;

/**
 * .
 * Created by criskey on 30/8/2017.
 */
public interface LocalStorage {

    Map<String, ?> getAll();

    @Nullable
    String getString(String key, @Nullable String defValue);

    int getInt(String key, int defValue);

    long getLong(String key, long defValue);

    float getFloat(String key, float defValue);

    boolean getBoolean(String key, boolean defValue);

    boolean contains(String key);

    void save(Map<String, ?> valueMap);

    void remove(String... keys);

    void clear();

}
