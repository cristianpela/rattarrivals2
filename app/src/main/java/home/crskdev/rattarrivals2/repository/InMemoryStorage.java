package home.crskdev.rattarrivals2.repository;

import android.support.annotation.MainThread;
import android.support.annotation.Nullable;

import java.util.Map;

/**
 *
 * Created by criskey on 19/10/2017.
 */
@MainThread
public class InMemoryStorage implements LocalStorage {

    //todo implement me

    @Override
    public Map<String, ?> getAll() {
        return null;
    }

    @Nullable
    @Override
    public String getString(String key, @Nullable String defValue) {
        return null;
    }

    @Override
    public int getInt(String key, int defValue) {
        return 0;
    }

    @Override
    public long getLong(String key, long defValue) {
        return 0;
    }

    @Override
    public float getFloat(String key, float defValue) {
        return 0;
    }

    @Override
    public boolean getBoolean(String key, boolean defValue) {
        return false;
    }

    @Override
    public boolean contains(String key) {
        return false;
    }

    @Override
    public void save(Map<String, ?> valueMap) {

    }

    @Override
    public void remove(String... keys) {

    }

    @Override
    public void clear() {

    }
}
