package home.crskdev.rattarrivals2.repository;

import android.content.SharedPreferences;
import android.support.annotation.Nullable;

import java.util.Map;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * .
 * Created by criskey on 31/8/2017.
 */
@Singleton
public class LocalStorageImpl implements LocalStorage {

    private SharedPreferences preferences;

    @Inject
    public LocalStorageImpl(SharedPreferences preferences) {
        this.preferences = preferences;
    }

    @Override
    public Map<String, ?> getAll() {
        return preferences.getAll();
    }

    @Nullable
    @Override
    public String getString(String key, @Nullable String defValue) {
        return preferences.getString(key, defValue);
    }

    @Override
    public int getInt(String key, int defValue) {
        return preferences.getInt(key, defValue);
    }

    @Override
    public long getLong(String key, long defValue) {
        return preferences.getLong(key, defValue);
    }

    @Override
    public float getFloat(String key, float defValue) {
        return preferences.getFloat(key, defValue);
    }

    @Override
    public boolean getBoolean(String key, boolean defValue) {
        return preferences.getBoolean(key, defValue);
    }

    @Override
    public boolean contains(String key) {
        return preferences.contains(key);
    }

    @Override
    public void save(Map<String, ?> valueMap) {
        transaction(editor -> {
            for (Map.Entry<String, ?> entry : valueMap.entrySet()) {
                put(editor, entry);
            }
        });
    }

    @Override
    public void remove(String... keys) {
        transaction(editor -> {
            for (String key : keys) {
                editor.remove(key);
            }
        });
    }

    @Override
    public void clear() {
        transaction(SharedPreferences.Editor::clear);
    }

    private void put(SharedPreferences.Editor editor, Map.Entry<String, ?> entry) {
        Object value = entry.getValue();
        if (value.getClass().equals(Boolean.class)) {
            editor.putBoolean(entry.getKey(), (Boolean) value);
        } else if (value.getClass().equals(String.class)) {
            editor.putString(entry.getKey(), (String) value);
        } else if (value.getClass().equals(Integer.class)) {
            editor.putInt(entry.getKey(), (Integer) value);
        } else if (value.getClass().equals(Float.class)) {
            editor.putFloat(entry.getKey(), (Float) value);
        } else {
            throw new IllegalStateException("Unimplemented edit");
        }
    }

    private void transaction(Transaction<SharedPreferences.Editor> transaction) {
        SharedPreferences.Editor editor = preferences.edit();
        try {
            transaction.run(editor);
        } catch (Exception e) {
            //no-op
        }
        editor.apply();
    }

    private interface Transaction<T> {
        void run(T arg);
    }
}
