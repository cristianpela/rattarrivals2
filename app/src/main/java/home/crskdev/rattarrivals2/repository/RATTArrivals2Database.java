package home.crskdev.rattarrivals2.repository;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;
import android.arch.persistence.room.migration.Migration;

import home.crskdev.rattarrivals2.arrivals.repository.ArrivalDAO;
import home.crskdev.rattarrivals2.arrivals.repository.ArrivalEntity;
import home.crskdev.rattarrivals2.lines.repository.LineDAO;
import home.crskdev.rattarrivals2.lines.repository.LineEntity;
import home.crskdev.rattarrivals2.lines.repository.LineTypeConverter;

/**
 *
 * Created by criskey on 13/8/2017.
 */
@Database(entities = {LineEntity.class, ArrivalEntity.class}, version = 1)
@TypeConverters(LineTypeConverter.class)
public abstract class RATTArrivals2Database extends RoomDatabase {

//    public static final Migration MIGRATION_1_2 = new Migration(1, 2) {
//        @Override
//        public void migrate(SupportSQLiteDatabase database) {
//            database.execSQL("CREATE TABLE IF NOT EXISTS `Arrival` " +
//                    "(`lineId` INTEGER, `stationName` TEXT," +
//                    " `toWay` TEXT, `timeStr` INTEGER, " +
//                    "`lastUpdated` INTEGER, PRIMARY KEY(`lineId`, `stationName`, `toWay`))");
//        }
//    };

    public abstract LineDAO getLineRepository();

    public abstract ArrivalDAO getPinnedArrivalRepository();

}
