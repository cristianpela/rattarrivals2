package home.crskdev.rattarrivals2.repository;

import home.crskdev.rattarrivals2.util.FluentMap;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.subjects.BehaviorSubject;

/**
 * Created by criskey on 11/8/2017.
 */
public class CheckServiceImpl implements CheckService {

    private static final String CHECKED = "CHECKED";
    private CheckService delegate;
    private LocalStorage localStorage;

    public CheckServiceImpl(LocalStorage localStorage) {
        this.localStorage = localStorage;
        delegate = new Delegate();
    }

    @Override
    public Single<Boolean> isInit() {
        return Single.create(s -> {
            boolean checked = localStorage.getBoolean(CHECKED, false);
            s.onSuccess(checked);
            ((Delegate) delegate).changeInit(checked);
        });
    }

    @Override
    public Observable<Boolean> initChanges() {
        return delegate.initChanges();
    }

    @Override
    public void setInit() {
        localStorage.save(new FluentMap()
                .put(CHECKED, true).get());
        delegate.setInit();
    }

    /**
     * Use this class in testings instead of {@link CheckServiceImpl}
     */
    //TODO: now that LocalStorage is abstracted; is this class it?
    public static class Delegate implements CheckService {

        private BehaviorSubject<Boolean> initSubject;


        public Delegate() {
            initSubject = BehaviorSubject.create();
        }

        /**
         * do not delegate this method in {@link CheckServiceImpl}
         *
         * @return
         */
        @Override
        public Single<Boolean> isInit() {
            return Single.just(false);
        }

        @Override
        public Observable<Boolean> initChanges() {
            return initSubject.serialize().filter(c -> c != null).distinctUntilChanged();
        }

        @Override
        public void setInit() {
            initSubject.onNext(true);
        }

        public void changeInit(boolean changed) {
            initSubject.onNext(changed);
        }
    }
}
