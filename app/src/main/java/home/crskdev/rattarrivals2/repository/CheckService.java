package home.crskdev.rattarrivals2.repository;

import io.reactivex.Observable;
import io.reactivex.Single;

/**
 * Created by criskey on 10/8/2017.
 */

public interface CheckService {

    Single<Boolean> isInit();

    Observable<Boolean> initChanges();

    void setInit();
}
