package home.crskdev.rattarrivals2.net;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;

/**
 * Created by criskey on 11/8/2017.
 */
public class NetworkClient implements Client {

    private static final String AGENT = "Mozilla/5.0 (Windows NT 6.3; WOW64; rv:43.0) Gecko/20100101 Firefox/43.0";
    private static final String REFERRER = "http://www.google.com";
    private static final int TIMEOUT = 12000;
    private static final int HTTP_OK = 200;

    @Override
    public Document request(String url) throws IOException {
        Connection.Response response = Jsoup.connect(url)
                .ignoreContentType(true)
                .userAgent(AGENT)
                .referrer(REFERRER)
                .timeout(TIMEOUT)
                .followRedirects(true)
                .execute();
        if (response.statusCode() != HTTP_OK)
            throw new IOException("Http error: [" + response.statusCode() + "]" +
                    response.statusMessage());
        return response.parse();
    }


}
