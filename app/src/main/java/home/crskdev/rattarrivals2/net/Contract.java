package home.crskdev.rattarrivals2.net;

import home.crskdev.rattarrivals2.lines.model.Line;
import home.crskdev.rattarrivals2.lines.model.LineType;

/**
 * Created by criskey on 11/8/2017.
 */

public interface Contract {

    //not working now: 86.122.170.105:61978
    //now working: 86.125.113.218:61978
    String IP_DOMANIN ="86.125.113.218:61978";

    String BASE_URL = "http://"+IP_DOMANIN+"/html/timpi/";

    final class Endpoints {

        private Endpoints() {
            throw new IllegalStateException();
        }

        public static String getLineLink(Line line) {
            return getLineLink(line.id);
        }

        public static String getLineLink(long lineId) {
            return BASE_URL + "trasee.php?param1=" + lineId;
        }

        public static String getLineTypeLink(LineType lineType) {
            return BASE_URL + lineType.suffix;
        }

    }

}
