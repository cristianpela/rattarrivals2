package home.crskdev.rattarrivals2.net;

import org.jsoup.nodes.Document;

import java.io.IOException;

/**
 * Created by criskey on 11/8/2017.
 */
public interface Client {

    Document request(String url) throws IOException;

}
