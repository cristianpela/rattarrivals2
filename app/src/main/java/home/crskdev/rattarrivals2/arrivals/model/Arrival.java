package home.crskdev.rattarrivals2.arrivals.model;

import home.crskdev.rattarrivals2.util.time.TimeEstimated;

/**
 * Created by criskey on 12/8/2017.
 */
//todo introduce estimation
public final class Arrival  {

    public static final Arrival NO_ARRIVAL = new Arrival(-1, "", "", TimeEstimated.estimate(0L, 0L), -1, false);

    public final long lineId;

    public final String stationName;

    public final TimeEstimated timeEstimated;

    public final String to;

    public final long lastUpdated;

    public final boolean pinned;

    public Arrival(long lineId, String stationName, String to, TimeEstimated timeEstimated,
                   long lastUpdated, boolean pinned) {
        this.lineId = lineId;
        this.stationName = stationName;
        this.timeEstimated = timeEstimated;
        this.to = to;
        this.lastUpdated = lastUpdated;
        this.pinned = pinned;
    }

    public Arrival(long lineId, String stationName, TimeEstimated timeEstimated, String to,
                   long lastUpdated) {
        this(lineId, stationName, to, timeEstimated, lastUpdated, false);
    }

    public final Arrival pinned(boolean pinned) {
        return new Arrival(lineId, stationName, to, timeEstimated, lastUpdated, pinned);
    }

    public final Arrival togglePinned(){
        return new Arrival(lineId, stationName, to, timeEstimated, lastUpdated, !pinned);
    }

    public final Arrival lastUpdated(long lastUpdated) {
        return new Arrival(lineId, stationName, to, timeEstimated, lastUpdated, pinned);
    }

    public final Arrival time(TimeEstimated time) {
        return new Arrival(lineId, stationName, to, time, lastUpdated, pinned);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Arrival arrival = (Arrival) o;

        if (lineId != arrival.lineId) return false;
        if (stationName != null ? !stationName.equals(arrival.stationName) : arrival.stationName != null)
            return false;
        return to != null ? to.equals(arrival.to) : arrival.to == null;

    }

    @Override
    public int hashCode() {
        int result = (int) (lineId ^ (lineId >>> 32));
        result = 31 * result + (stationName != null ? stationName.hashCode() : 0);
        result = 31 * result + (to != null ? to.hashCode() : 0);
        return result;
    }



    @Override
    public String toString() {
        return "Arrival{" +
                "lineId=" + lineId +
                ", stationName='" + stationName + '\'' +
                ", timeEstimated='" + timeEstimated.time + '\'' +
                ", to='" + to + '\'' +
                ", lastUpdatedStr=" + lastUpdated +
                ", pinned=" + pinned +
                '}';
    }

    public int getId() {
        return hashCode();
    }
}
