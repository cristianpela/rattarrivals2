package home.crskdev.rattarrivals2.arrivals.model;

import home.crskdev.rattarrivals2.lines.model.LineType;
import home.crskdev.rattarrivals2.util.time.TimeEstimated;

/**
 * .
 * Created by criskey on 12/8/2017.
 */

public final class ArrivalExtra {

    public final long lineId;

    public final String title;

    public final LineType type;

    public final boolean favorite;

    public final String stationName;

    public final TimeEstimated time;

    public final String toWay;

    public final long lastUpdated;

    public final boolean pinned;

    public ArrivalExtra(long lineId, String stationName, String toWay, String title, LineType type,
                        boolean favorite, TimeEstimated timeEstimated, long lastUpdated, boolean pinned) {
        this.title = title;
        this.type = type;
        this.favorite = favorite;
        this.lineId = lineId;
        this.stationName = stationName;
        this.time = timeEstimated;
        this.toWay = toWay;
        this.lastUpdated = lastUpdated;
        this.pinned = pinned;
    }

    public ArrivalExtra(long lineId, String stationName, String toWay, String title, LineType type,
                        boolean favorite, TimeEstimated timeEstimated, long lastUpdated) {
        this(lineId, stationName, toWay, title, type, favorite, timeEstimated, lastUpdated, true);
    }


    public ArrivalExtra update(TimeEstimated timeEstimated, long lastUpdated) {
        return new ArrivalExtra(lineId, stationName, toWay, title, type, favorite, timeEstimated, lastUpdated, pinned);
    }

    public ArrivalExtra pinned(boolean pinned) {
        return new ArrivalExtra(lineId, stationName, toWay, title, type, favorite, time, lastUpdated, pinned);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ArrivalExtra that = (ArrivalExtra) o;

        if (lineId != that.lineId) return false;
        if (stationName != null ? !stationName.equals(that.stationName) : that.stationName != null)
            return false;
        return toWay != null ? toWay.equals(that.toWay) : that.toWay == null;

    }

    @Override
    public int hashCode() {
        int result = (int) (lineId ^ (lineId >>> 32));
        result = 31 * result + (stationName != null ? stationName.hashCode() : 0);
        result = 31 * result + (toWay != null ? toWay.hashCode() : 0);
        return result;
    }

    public int getId() {
        return hashCode();
    }

    public String getIdStr() {
        return lineId + stationName + toWay;
    }



    public ArrivalExtra togglePinned() {
        return new ArrivalExtra(lineId, stationName, toWay, title, type, false, time, lastUpdated, !pinned);
    }

    @Override
    public String toString() {
        return "ArrivalExtra{" +
                "lineId=" + lineId +
                ", stationName='" + stationName + '\'' +
                ", toWay='" + toWay + '\'' +
                '}';
    }
}
