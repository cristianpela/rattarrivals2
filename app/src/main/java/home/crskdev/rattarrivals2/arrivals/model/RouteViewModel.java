package home.crskdev.rattarrivals2.arrivals.model;

import java.util.List;

import home.crskdev.rattarrivals2.model.BaseViewModel;

/**
 *
 * Created by criskey on 9/10/2017.
 */

public class RouteViewModel extends BaseViewModel {

    public final long lineId;

    public final String from;

    public final String to;

    /**
     * Note: must be immutable/unmodifiable
     */
    public final List<ArrivalViewModel> arrivals;

    public final long lastUpdated;

    public RouteViewModel(long lineId, String from, String to, List<ArrivalViewModel> arrivals, long lastUpdated) {
        this.lineId = lineId;
        this.from = from;
        this.to = to;
        this.arrivals = arrivals;
        this.lastUpdated = lastUpdated;
    }

}
