package home.crskdev.rattarrivals2.arrivals.presentation;

import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.jakewharton.rxbinding2.support.v7.widget.RxToolbar;
import com.jakewharton.rxbinding2.view.RxView;

import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import home.crskdev.rattarrivals2.R;
import home.crskdev.rattarrivals2.arrivals.model.ArrivalViewModel;
import home.crskdev.rattarrivals2.core.BaseActivity;
import home.crskdev.rattarrivals2.model.Action.Pinned;
import home.crskdev.rattarrivals2.model.BaseViewModel;
import home.crskdev.rattarrivals2.lines.model.Line;
import home.crskdev.rattarrivals2.util.ResourceFinder;
import home.crskdev.rattarrivals2.util.ResourceFinderImpl;
import home.crskdev.rattarrivals2.util.recyclerview.TapableRecyclerView;
import io.reactivex.Observable;

import static home.crskdev.rattarrivals2.arrivals.ArrivalsConverter.composeWithLineIgnored;
import static home.crskdev.rattarrivals2.arrivals.ArrivalsConverter.toDTOFromVM;
import static home.crskdev.rattarrivals2.arrivals.ArrivalsConverter.toVMExtraFromDTOExtra;
import static home.crskdev.rattarrivals2.model.Action.Back;
import static home.crskdev.rattarrivals2.model.Action.ChangeWayAction;
import static home.crskdev.rattarrivals2.model.Action.FavoriteAction;
import static home.crskdev.rattarrivals2.model.Action.LineAction;
import static home.crskdev.rattarrivals2.model.Action.LoadAction;
import static home.crskdev.rattarrivals2.util.recyclerview.TapableRecyclerView.TapKind.DOUBLE_TAP;

/**
 * Created by criskey on 24/8/2017.
 */
public class ArrivalsActivity extends BaseActivity<ArrivalsView, ArrivalsPresenter>
        implements ArrivalsView {


    @BindView(R.id.coordinator_lines)
    ViewGroup coordinatorLayout;

    @BindView(R.id.fab_refresh)
    FloatingActionButton btnRefresh;

    @BindView(R.id.toggle_btn_fav)
    ToggleButton btnFav;

    @BindView(R.id.btn_switch_ways)
    ImageButton btnSwitchWay;

    @BindView(R.id.recycler_arrivals)
    TapableRecyclerView recyclerArrivals;

    @BindView(R.id.txt_line)
    TextView txtLine;

    @BindView(R.id.txtFrom)
    TextView txtFrom;

    @BindView(R.id.txtTo)
    TextView txtTo;

    @BindView(R.id.prog_wait)
    ProgressBar progWait;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.app_bar_lines)
    AppBarLayout appBarLayout;

    @Inject
    ArrivalsPresenter presenter;

    public static Intent newIntent(Context context, long lineId) {
        return new Intent(context, ArrivalsActivity.class)
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                .putExtra(ArrivalsPresenter.KEY_LINE_ID, lineId);
    }

    @Override
    protected void onPreAttachPresenter() {
        setSupportActionBar(toolbar);
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setDisplayShowTitleEnabled(false);
            supportActionBar.setDisplayUseLogoEnabled(false);
            supportActionBar.setDisplayHomeAsUpEnabled(false);
            appBarLayout.addOnOffsetChangedListener((__, offset) -> supportActionBar
                    .setDisplayHomeAsUpEnabled((Math.abs(offset) - appBarLayout.getTotalScrollRange()) == 0)
            );
        }

        txtLine.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20f);

        progWait.getIndeterminateDrawable()
                .setColorFilter(ContextCompat
                        .getColor(this, R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
        progWait.getProgressDrawable()
                .setColorFilter(ContextCompat
                        .getColor(this, R.color.colorWarning), PorterDuff.Mode.SRC_ATOP);
        recyclerArrivals.setLayoutManager(new LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL, false));
        recyclerArrivals.setAdapter(new ArrivalsAdapter());

    }

    @Override
    protected void onPostAttachPresenter() {
        bindActions();
    }

    private void bindActions() {

        final long lineId = getIntent().getLongExtra(ArrivalsPresenter.KEY_LINE_ID, -1);

        //todo: refactor this; this should be on presenter side
        final ResourceFinder resourceFinder = new ResourceFinderImpl(getApplicationContext());

        //noinspection unchecked
        presenter.bindActions(Observable.merge(Arrays.asList(
                //init
                Observable.just(new LoadAction(lineId)),
                //init
                Observable.just(new LineAction(lineId)),
                //change ways
                RxView.clicks(btnSwitchWay).map(__ -> new ChangeWayAction()),
                //refresh clicks
                RxView.clicks(btnRefresh).map(__ -> new LoadAction(lineId)),
                //fav clicks
                RxView.clicks(btnFav).map(__ -> new FavoriteAction(lineId)),
                //home back button
                RxToolbar.navigationClicks(toolbar).map(__ -> Back.BACK),
                //remove or add station as pinned
                recyclerArrivals.tapObservable()
                        .filter(ev -> ev.kind == DOUBLE_TAP
                                && ((ArrivalViewModel) ev.extra).enabled)
                        .map(ev -> new Pinned //todo ugly ahead
                                (toVMExtraFromDTOExtra
                                        (composeWithLineIgnored
                                                (toDTOFromVM((ArrivalViewModel) ev.extra)), resourceFinder, false))))));
    }

    @Override
    public void onDisplayArrivals(List<ArrivalViewModel> list, String from, String to) {
        getAdapter().setItems(list);
        txtFrom.setText(from);
        txtTo.setText(to);
    }

    @Override
    public void onChangePinnedArrival(ArrivalViewModel arrival) {
        getAdapter().upsert(arrival);
    }

    @Override
    public void onGoBack() {
        finish();
    }

    @Override
    public void onChangeFavoriteLine(Line line) {
        onLoadLine(line);
    }

    @Override
    public void onLoadLine(Line line) {
        txtLine.setText(line.title);
        btnFav.setChecked(line.favorite);
    }

    @Override
    public void onWait() {
        progWait.setVisibility(View.VISIBLE);
        btnRefresh.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onDone() {
    }

    @Override
    public void onError(Throwable e) {
        progWait.setVisibility(View.GONE);
        btnRefresh.show();
        Snackbar.make(recyclerArrivals, e.toString() + " " + e.getMessage(), Snackbar.LENGTH_INDEFINITE)
                .setAction("Retry", (__) -> bindActions())
                .show();
    }

    @Override
    public ArrivalsPresenter presenter() {
        return presenter;
    }

    @Override
    public int layoutId() {
        return R.layout.arrivals_activity_layout;
    }

    private ArrivalsAdapter getAdapter() {
        return (ArrivalsAdapter) recyclerArrivals.getAdapter();
    }

    @Override
    public void onCoolDownStart(int maxValue) {
        btnRefresh.setVisibility(View.INVISIBLE);
        progWait.setIndeterminate(false);
        progWait.setVisibility(View.VISIBLE);
        progWait.setMax(maxValue);
    }

    @Override
    public void onCoolDown(int value) {
        progWait.setProgress(value);
    }

    @Override
    public void onCoolDownEnd() {
        progWait.setVisibility(View.GONE);
        progWait.setIndeterminate(true);
        btnRefresh.setVisibility(View.VISIBLE);
    }
}
