package home.crskdev.rattarrivals2.arrivals.services;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import home.crskdev.rattarrivals2.arrivals.model.Arrival;
import home.crskdev.rattarrivals2.util.asserts.Asserts;
import home.crskdev.rattarrivals2.util.time.TimeConverter;
import home.crskdev.rattarrivals2.util.time.TimeEstimated;

/**
 * Created by criskey on 19/8/2017.
 */
public class ArrivalParser {

    private static final int COLUMN_STATION_WIDTH = 200;
    private static final int COLUMN_ARRIVAL_TIME_WIDTH = 60;
    private static final String HEADER_TABLE_WORD_STATION = "Stația";

    private Document document;
    private long lineId;
    private long lastUpdated;
    private TimeConverter timeConverter;

    public ArrivalParser(Document document, long lineId, long lastUpdated,
                         TimeConverter timeConverter) {
        this.document = document;
        this.lineId = lineId;
        this.lastUpdated = lastUpdated;
        this.timeConverter = timeConverter;
    }

    public List<Arrival> parseArrivals() throws IOException {

        List<String> stationNames = getStationNamesBothWay();
        List<String> arrivalTimes = getArrivalTimesBothWay();

        List<Arrival> arrivals = new ArrayList<>();

        String[] tos = new String[2]; //  find "to" point stations;
        for (int i = 0, t = 0, size = stationNames.size(); i < size; i++) {
            String stationName = stationNames.get(i);
            if (isHeaderStation(stationName)) {
                tos[t++] = stationNames.get(i + 1);
            }
        }
        Asserts.basicAssert(tos.length == 2, "'to way' array must have length of 2");

        // we're starting from index one cause first "station" has the header HEADER_TABLE_WORD_STATION
        for (int i = 1, t = tos.length - 1, size = stationNames.size(); i < size; i++) {
            String stationName = stationNames.get(i);
            TimeEstimated timeEstimated = timeConverter.format(arrivalTimes.get(i));
            if (isHeaderStation(stationName)) {
                t--;
                continue;// skip on cycle to avoid the other header HEADER_TABLE_WORD_STATION & SKIP_WORD_ARRIVAL
            }
            arrivals.add(new Arrival(lineId, stationName, timeEstimated, tos[t], lastUpdated));
        }
        return arrivals;
    }

    private List<String> getStationNamesBothWay() {
        return getRelevantColumn(COLUMN_STATION_WIDTH);
    }

    private List<String> getArrivalTimesBothWay() {
        return getRelevantColumn(COLUMN_ARRIVAL_TIME_WIDTH);
    }

    private boolean isHeaderStation(String stationName) {
        return stationName.equals(HEADER_TABLE_WORD_STATION) || stationName.equals("Sta?ia");
    }

    private List<String> getRelevantColumn(int columnWidth) {
        Elements tdElements = document.select("td[width=" + columnWidth + "]");
        List<String> data = new ArrayList<>();
        for (Element tdElement : tdElements) {
            String bText = tdElement.select("b").text().trim();
            // if (!bText.equals(SKIP_WORD_ARRIVAL) && !bText.equals(HEADER_TABLE_WORD_STATION))
            data.add(bText);
        }
        return data;
    }

}
