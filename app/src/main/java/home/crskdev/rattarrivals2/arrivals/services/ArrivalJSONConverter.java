package home.crskdev.rattarrivals2.arrivals.services;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import home.crskdev.rattarrivals2.arrivals.model.Arrival;

/**
 * Created by criskey on 31/8/2017.
 */
public interface ArrivalJSONConverter {

    String toJSON(Arrival arival) throws ConversionException;

    Arrival fromJSON(@Nullable String json, @NonNull Arrival failSafe) throws ConversionException;

    class ConversionException extends RuntimeException {
        public ConversionException(Throwable cause) {
            super(cause);
        }
    }

}
