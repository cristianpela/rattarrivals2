package home.crskdev.rattarrivals2.arrivals.presentation;

import home.crskdev.rattarrivals2.arrivals.model.ArrivalViewModel;
import home.crskdev.rattarrivals2.util.recyclerview.BaseAdapterOps;

/**
 * for now we leave it as is, we might introduce new operations to ArrivalsAdapterOps
 * Created by criskey on 2/10/2017.
 */
class ArrivalsAdapterOpsImpl extends BaseAdapterOps<ArrivalViewModel> implements ArrivalsAdapterOps {

}
