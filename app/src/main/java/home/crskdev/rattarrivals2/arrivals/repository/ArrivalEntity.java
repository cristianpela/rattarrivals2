package home.crskdev.rattarrivals2.arrivals.repository;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.support.annotation.NonNull;

/**
 * Created by criskey on 21/9/2017.
 */
@Entity(tableName = "Arrival", primaryKeys = {"lineId", "stationName", "toWay"})
public class ArrivalEntity {

    public final long lineId;

    @NonNull
    public final String stationName;

    @NonNull
    public final String toWay;

    public final long time;

    public final long lastUpdated;

    public ArrivalEntity(long lineId, String stationName, String toWay,
                         long time, long lastUpdated) {
        this.lineId = lineId;
        this.stationName = stationName;
        this.toWay = toWay;
        this.time = time;
        this.lastUpdated = lastUpdated;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ArrivalEntity that = (ArrivalEntity) o;

        if (lineId != that.lineId) return false;
        if (stationName != null ? !stationName.equals(that.stationName) : that.stationName != null)
            return false;
        return toWay != null ? toWay.equals(that.toWay) : that.toWay == null;

    }

    @Override
    public int hashCode() {
        int result = (int) (lineId ^ (lineId >>> 32));
        result = 31 * result + (stationName != null ? stationName.hashCode() : 0);
        result = 31 * result + (toWay != null ? toWay.hashCode() : 0);
        return result;
    }
}
