package home.crskdev.rattarrivals2.arrivals.services;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import org.json.JSONException;
import org.json.JSONObject;

import javax.inject.Inject;

import home.crskdev.rattarrivals2.arrivals.model.Arrival;
import home.crskdev.rattarrivals2.util.time.TimeEstimated;

/**
 * Created by criskey on 31/8/2017.
 */
public class ArrivalJSONConverterImpl implements ArrivalJSONConverter {

    private static final String LINE_ID = "lineId";
    private static final String STATION_NAME = "stationName";
    private static final String TIME = "timeStr";
    private static final String TO = "to";
    private static final String LAST_UPDATED = "lastUpdatedStr";
    private static final String PINNED = "pinned";

    @Inject
    public ArrivalJSONConverterImpl() {
    }

    @Override
    public String toJSON(@NonNull Arrival arrival) throws ConversionException {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(LINE_ID, arrival.lineId);
            jsonObject.put(STATION_NAME, arrival.stationName);
            jsonObject.put(TIME, arrival.timeEstimated);
            jsonObject.put(TO, arrival.to);
            jsonObject.put(LAST_UPDATED, arrival.lastUpdated);
            jsonObject.put(PINNED, arrival.pinned);
        } catch (JSONException e) {
            throw new ConversionException(e);
        }
        return jsonObject.toString();
    }

    @Override
    public Arrival fromJSON(@Nullable String json, @NonNull Arrival failSafe) throws ConversionException {
        if (json == null) {
            return failSafe;
        }
        final Arrival arrival;
        try {
            JSONObject jsonObject = new JSONObject(json);
            arrival = new Arrival(
                    jsonObject.getLong(LINE_ID),
                    jsonObject.getString(STATION_NAME),
                    jsonObject.getString(TO), TimeEstimated.estimate(jsonObject.getLong(TIME),jsonObject.getLong(TIME)),
                    jsonObject.getLong(LAST_UPDATED),
                    jsonObject.getBoolean(PINNED)
            );
        } catch (JSONException e) {
            throw new ConversionException(e);
        }
        return arrival;
    }

}
