package home.crskdev.rattarrivals2.arrivals.repository;

import java.util.List;

import home.crskdev.rattarrivals2.arrivals.model.Arrival;
import home.crskdev.rattarrivals2.arrivals.model.ArrivalExtra;
import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.Single;

/**
 * Created by criskey on 21/9/2017.
 */
public interface ArrivalRepository {

    Single<ArrivalExtra> pin(ArrivalExtra arrival);

    Single<ArrivalExtra> unpin(ArrivalExtra arrival);

    Observable<ArrivalExtra> observeUnpin();

    Single<List<ArrivalExtra>> getAllExtra();

    Single<List<ArrivalExtra>> tryRefresh(List<ArrivalExtra> arrivals);

    Single<ArrivalExtra> refresh(ArrivalExtra arrival);

    Observable<List<ArrivalExtra>> observePin();

    Flowable<Boolean> observeShowing();
}
