package home.crskdev.rattarrivals2.arrivals.presentation;

import home.crskdev.rattarrivals2.arrivals.model.ArrivalViewModel;
import home.crskdev.rattarrivals2.util.recyclerview.AdapterOps;

/**
 * Created by criskey on 2/10/2017.
 */

interface ArrivalsAdapterOps extends AdapterOps<ArrivalViewModel> {}
