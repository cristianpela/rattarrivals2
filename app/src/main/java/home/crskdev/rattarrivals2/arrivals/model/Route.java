package home.crskdev.rattarrivals2.arrivals.model;

import java.util.List;

import home.crskdev.rattarrivals2.arrivals.model.Arrival;

/**
 * Created by criskey on 23/8/2017.
 */
public class Route {

    public final long lineId;

    public final String from;

    public final String to;

    /**
     * Note: must be immutable/unmodifiable
     */
    public final List<Arrival> arrivals;

    public final long lastUpdated;

    public Route(long lineId, String from, String to, List<Arrival> arrivals, long lastUpdated) {
        this.lineId = lineId;
        this.from = from;
        this.to = to;
        this.arrivals = arrivals;
        this.lastUpdated = lastUpdated;
    }

    @Override
    public String toString() {
        StringBuilder b = new StringBuilder();
        for (int i = 0; i < arrivals.size(); i++) {
            b.append("\t").append(arrivals.get(i));
            if (i < arrivals.size() - 1) {
                b.append("\n");
            }
        }
        return "Route{" +
                "\nlineId=" + lineId +
                "\nfrom='" + from + '\'' +
                "\nto='" + to + '\'' +
                "\nlastUpdatedStr=" + lastUpdated +
                "\narrivals=\n" + b.toString() +
                '}';
    }
}
