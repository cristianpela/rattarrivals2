package home.crskdev.rattarrivals2.arrivals.services;

import org.jsoup.nodes.Document;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import home.crskdev.rattarrivals2.model.Action;
import home.crskdev.rattarrivals2.arrivals.model.Arrival;
import home.crskdev.rattarrivals2.model.Result;
import home.crskdev.rattarrivals2.arrivals.model.Route;
import home.crskdev.rattarrivals2.net.Client;
import home.crskdev.rattarrivals2.net.Contract;
import home.crskdev.rattarrivals2.util.time.TimeUtils;
import home.crskdev.rattarrivals2.util.time.TimeConverter;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by criskey on 19/8/2017.
 */
@Singleton
public class ArrivalParseServiceImpl implements ArrivalParseService {

    private InternalState internalState;

    private Client client;

    @Inject
    public ArrivalParseServiceImpl(Client client) {
        this.client = client;
        internalState = InternalState.UNDEFINED;
    }

    //todo make Observables methods to Single

    @Override
    public Observable<Result.RouteResult> fetchRoute(Action.LoadAction action) {
        return Observable.defer(() -> parseArrivals(action.lineId).toObservable()
                .map(list -> internalState.reCreate(list))
                .doOnNext(is -> internalState = is)
                .map(is -> new Result.RouteResult(InternalState.mapInternalStateToRoute(is))))
                .subscribeOn(Schedulers.io());
    }

    @Override
    public Observable<Result.RouteResult> changeWay() {
        return Observable.defer(() -> Observable
                .just(internalState.next())
                .doOnNext(is -> internalState = is)
                .map(is -> new Result.RouteResult(InternalState.mapInternalStateToRoute(is))))
                .subscribeOn(Schedulers.io());
    }

    @Override
    public Observable<Result.StationResult> fetchStation(Arrival arrival) {
        return Observable.defer(() -> parseArrivals(arrival.lineId).toObservable()
                .map(list -> extractArrival(arrival, list))
                .filter(a -> !a.equals(Arrival.NO_ARRIVAL))
                .map(Result.StationResult::new))
                .subscribeOn(Schedulers.io());
    }

    private Arrival extractArrival(Arrival arrival, List<Arrival> arrivals) {
        for (Arrival a : arrivals) {
            if (a.equals(arrival))
                //we make sure "a" is pinning is up to date
                return a.pinned(arrival.pinned);
        }
        return Arrival.NO_ARRIVAL;
    }

    private Single<List<Arrival>> parseArrivals(long lineId) {
        return Single.create(s -> {
            String url = Contract.Endpoints.getLineLink(lineId);
            Document doc = client.request(url);
            long lastUpdated = System.currentTimeMillis();
            ArrivalParser parser = new ArrivalParser(doc, lineId, lastUpdated,
                    new TimeConverter(lastUpdated, TimeUtils.ROMANIAN_TIME_ZONE));
            s.onSuccess(parser.parseArrivals());
        });
    }


    static class InternalState {

        static final InternalState UNDEFINED = new InternalState(Collections.emptyList());

        static final int TO1 = 0;

        static final int TO2 = 1;

        final int way;

        final List<Arrival> arrivals;

        InternalState(int way, List<Arrival> arrivals) {
            this.way = way;
            this.arrivals = arrivals;
        }

        InternalState(List<Arrival> arrivals) {
            this(TO1, arrivals);
        }

        static Route mapInternalStateToRoute(InternalState state) {
            if (state.arrivals.isEmpty()) // should not throw this
                throw new IllegalStateException("Internal state is Undefined. Call reCreate from current " +
                        " state. Make sure the list passed is no empty!");

            String from = null, to;
            List<Arrival> arrivals = new ArrayList<>();

            Arrival sample = state.arrivals.get(0);
            long lineId = sample.lineId;
            long lastUpdate = sample.lastUpdated;
            int lastIndex = state.arrivals.size() - 1;

            if (state.way == TO1) {
                to = sample.to;
                for (int i = 0, n = 1; i <= lastIndex; i++) {
                    Arrival next = state.arrivals.get(n++);
                    Arrival current = state.arrivals.get(i);
                    arrivals.add(current);
                    //the premise is that viewModel "to"s are ordered like
                    // to1, to1, to1, to2, to2, to2
                    if (!current.to.equals(next.to)) {
                        from = next.to;
                        break;
                    }
                }
            } else {
                to = state.arrivals.get(lastIndex).to;
                for (int i = lastIndex, n = lastIndex - 1; i >= 0; i--) {
                    Arrival next = state.arrivals.get(n--);
                    Arrival current = state.arrivals.get(i);
                    arrivals.add(current);
                    if (!current.to.equals(next.to)) {
                        from = next.to;
                        break;
                    }
                }
                Collections.reverse(arrivals);
            }

            if (from == null)
                throw new IllegalStateException("Internal State imbalanced: " +
                        "Could not create 'FROM' in Route. Make sure parsing is done correct!");

            return new Route(lineId, from, to, arrivals, lastUpdate);
        }

        InternalState reCreate(List<Arrival> arrivals) {
            return new InternalState(TO1, arrivals);
        }

        InternalState next() {
            return new InternalState(Math.abs(way - 1), arrivals);
        }

    }
}
