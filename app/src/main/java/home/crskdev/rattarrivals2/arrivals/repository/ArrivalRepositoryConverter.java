package home.crskdev.rattarrivals2.arrivals.repository;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import home.crskdev.rattarrivals2.arrivals.ArrivalsConverter;
import home.crskdev.rattarrivals2.arrivals.model.Arrival;
import home.crskdev.rattarrivals2.arrivals.model.ArrivalExtra;
import home.crskdev.rattarrivals2.util.ImmLists;
import home.crskdev.rattarrivals2.util.time.TimeEstimated;

/**
 * Created by criskey on 21/9/2017.
 */
final class ArrivalRepositoryConverter {

    public static final int KEY_LINE_ID = 0;
    public static final int KEY_STATION_NAME = 1;
    public static final int KEY_TO = 2;

    private ArrivalRepositoryConverter() {
        throw new IllegalStateException();
    }

    static ArrivalEntity toEntityFromDTOExtra(ArrivalExtra arrivalExtra) {
        Arrival arrival = ArrivalsConverter.toDTOFromDTOExtra(arrivalExtra);
        return toEntityFromDTO(arrival);
    }

    static ArrivalEntity toEntityFromDTO(Arrival arrival) {
        return new ArrivalEntity(arrival.lineId,
                arrival.stationName, arrival.to, arrival.timeEstimated.time, arrival.lastUpdated);
    }

    static ArrivalExtra toDTOExtraFromEntityExtra(ArrivalExtraEntity entity) {
        return new ArrivalExtra(entity.lineId, entity.stationName, entity.toWay,
                entity.title, entity.type, entity.favorite, TimeEstimated.nonEstimated(entity.time),
                entity.lastUpdated);
    }

    static ArrivalEntity toEntityFromEntityExtra(ArrivalExtraEntity entity) {
        return new ArrivalEntity(entity.lineId, entity.stationName, entity.toWay,
                entity.time, entity.lastUpdated);
    }

    public static ArrivalExtraEntity toEntityExtraFromDTOExtra(ArrivalExtra extra) {
        return new ArrivalExtraEntity(extra.lineId, extra.stationName, extra.toWay, extra.title,
                extra.type, extra.favorite, extra.time.time, extra.lastUpdated);
    }

    public static List<List<?>> extractCompositeKeysFromList(List<Arrival> arrival) {
        List<List<?>> keys = new ArrayList<>(3);
        ArrayList<Long> lineIdKeys = new ArrayList<>();
        ArrayList<String> stationNameKeys = new ArrayList<>();
        ArrayList<String> toKeys = new ArrayList<>();
        for (Arrival a : arrival) {
            lineIdKeys.add(a.lineId);
            stationNameKeys.add(a.stationName);
            toKeys.add(a.to);
        }
        keys.add(Collections.unmodifiableList(lineIdKeys));
        keys.add(Collections.unmodifiableList(stationNameKeys));
        keys.add(Collections.unmodifiableList(toKeys));
        return Collections.unmodifiableList(keys);
    }

    public static List<List<?>> extractCompositeKeysFromListExtras(List<ArrivalExtra> arrival) {
        return extractCompositeKeysFromList(ImmLists.map(arrival, ArrivalsConverter::toDTOFromDTOExtra));
    }
}
