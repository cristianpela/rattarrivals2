package home.crskdev.rattarrivals2.arrivals.services;

import home.crskdev.rattarrivals2.arrivals.model.Arrival;
import home.crskdev.rattarrivals2.model.Action;
import home.crskdev.rattarrivals2.model.Result.RouteResult;
import home.crskdev.rattarrivals2.model.Result.StationResult;
import io.reactivex.Observable;

/**
 * Created by criskey on 23/8/2017.
 */
public interface ArrivalParseService {

    Observable<RouteResult> fetchRoute(Action.LoadAction action);

    Observable<RouteResult> changeWay();

    Observable<StationResult> fetchStation(Arrival arrival);
}
