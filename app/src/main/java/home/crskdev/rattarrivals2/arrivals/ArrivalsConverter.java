package home.crskdev.rattarrivals2.arrivals;

import java.util.Date;

import home.crskdev.rattarrivals2.arrivals.model.Arrival;
import home.crskdev.rattarrivals2.arrivals.model.ArrivalExtra;
import home.crskdev.rattarrivals2.arrivals.model.ArrivalExtraViewModel;
import home.crskdev.rattarrivals2.arrivals.model.ArrivalViewModel;
import home.crskdev.rattarrivals2.lines.model.Line;
import home.crskdev.rattarrivals2.util.ResourceFinder;
import home.crskdev.rattarrivals2.util.time.TimeUtils;
import home.crskdev.rattarrivals2.util.time.TimeEstimated;

/**
 * Created by criskey on 4/10/2017.
 */
public class ArrivalsConverter {

    private ArrivalsConverter() {
        throw new IllegalStateException();
    }

    public static ArrivalViewModel toVMFromDTO(Arrival arrival, ResourceFinder resourceFinder,
                                               boolean inverseEstimationColor) {
        int color = TimeEstimated.toColor(arrival.timeEstimated, resourceFinder, inverseEstimationColor);
        String timeStr = TimeUtils.HOUR_MINUTES_FORMAT.format(new Date(arrival.timeEstimated.time));
        String lastUpdatedStr = TimeUtils.DATE_FORMAT.format(new Date(arrival.lastUpdated));
        return new ArrivalViewModel(arrival.lineId, arrival.stationName, arrival.to,
                arrival.timeEstimated.time,
                timeStr,
                color,
                arrival.timeEstimated,
                arrival.lastUpdated,
                lastUpdatedStr,
                arrival.pinned);
    }

    public static ArrivalExtraViewModel toVMExtraFromDTOExtra(ArrivalExtra arrival, ResourceFinder res,
                                                              boolean inverseEstimationColor) {
        int lineImage;
        switch (arrival.type) {
            case NONE:
            default:
                lineImage = res.getImage("tram");
                break;
            case AUTO:
                lineImage = res.getImage("bus");
                break;
            case TROLLEY:
                lineImage = res.getImage("troleu");
                break;
            case TRAM:
                lineImage = res.getImage("tram");
                break;
        }

        int estimationColor = TimeEstimated.toColor(arrival.time, res, inverseEstimationColor);
        int staleColor = res.getColorInt("colorStaleNew");
        String timeStr = TimeUtils.HOUR_MINUTES_FORMAT.format(new Date(arrival.time.time));
        String lastUpdatedStr = TimeUtils.DATE_FORMAT.format(new Date(arrival.lastUpdated));

        return new ArrivalExtraViewModel(arrival.lineId, arrival.stationName, arrival.toWay,
                arrival.title,
                arrival.type,
                lineImage,
                arrival.favorite,
                arrival.time.time,
                timeStr,
                estimationColor,
                arrival.time,
                staleColor, arrival.lastUpdated,
                lastUpdatedStr,
                arrival.pinned);
    }

    public static Arrival toDTOFromVM(ArrivalViewModel arrival) {
        return new Arrival(arrival.lineId, arrival.stationName, arrival.to,
                arrival.timeEstimated,
                arrival.lastUpdated, arrival.pinned);
    }

    public static Arrival toDTOFromDTOExtra(ArrivalExtra arrival) {
        return new Arrival(arrival.lineId, arrival.stationName,
                arrival.toWay, arrival.time, arrival.lastUpdated, arrival.pinned);
    }

    public static ArrivalExtra toDTOExtraFromVMExtra(ArrivalExtraViewModel arrival) {
        return new ArrivalExtra(arrival.lineId, arrival.stationName, arrival.toWay, arrival.title,
                arrival.lineType, arrival.favorite,
                arrival.timeEstimated, arrival.lastUpdated, arrival.pinned);
    }

    public static ArrivalExtra compose(Arrival arrival, Line line) {
        return new ArrivalExtra(arrival.lineId, arrival.stationName, arrival.to, line.title, line.type, line.favorite,
                arrival.timeEstimated, arrival.lastUpdated, arrival.pinned);
    }

    public static ArrivalExtra composeWithLineIgnored(Arrival arrival) {
        return compose(arrival, Line.NO_LINE);
    }

    public static ArrivalViewModel toVMFromVMExtra(ArrivalExtraViewModel extra) {
        int color = extra.estimationTimeColor;
        String timeStr = TimeUtils.HOUR_MINUTES_FORMAT.format(new Date(extra.time));
        String lastUpdatedStr = TimeUtils.DATE_FORMAT.format(new Date(extra.lastUpdated));
        return new ArrivalViewModel(extra.lineId, extra.stationName, extra.toWay,
                extra.time,
                timeStr,
                color,
                extra.timeEstimated,
                extra.lastUpdated,
                lastUpdatedStr,
                extra.pinned);
    }
}
