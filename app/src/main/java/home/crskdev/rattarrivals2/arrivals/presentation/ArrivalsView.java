package home.crskdev.rattarrivals2.arrivals.presentation;

import java.util.List;

import home.crskdev.rattarrivals2.arrivals.model.ArrivalViewModel;
import home.crskdev.rattarrivals2.core.IView;
import home.crskdev.rattarrivals2.lines.model.Line;
import home.crskdev.rattarrivals2.lines.presentation.CoolDownAware;

/**
 * Created by criskey on 20/8/2017.
 */

public interface ArrivalsView extends IView, CoolDownAware {

    void onDisplayArrivals(List<ArrivalViewModel> list, String from, String to);

    void onChangePinnedArrival(ArrivalViewModel arrival);

    void onChangeFavoriteLine(Line line);

    void onLoadLine(Line line);

    void onWait();

    void onDone();

    void onError(Throwable e);

    void onGoBack();
}
