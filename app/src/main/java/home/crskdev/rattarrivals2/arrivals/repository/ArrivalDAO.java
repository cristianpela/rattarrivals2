package home.crskdev.rattarrivals2.arrivals.repository;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;

/**
 *
 * Created by criskey on 21/9/2017.
 */
@Dao
public interface ArrivalDAO {

    String GET_ALL_QUERY = "SELECT pa.*, l.title, l.type, l.favorite FROM Arrival AS pa, Line as l" +
            " WHERE pa.lineId = l.id ORDER BY pa.lastUpdated DESC";

    String GET_ALL_EXTRA_QUERY = "SELECT pa.*, l.title, l.type, l.favorite  FROM Arrival AS pa, Line as l" +
            " WHERE pa.lineId = l.id ORDER BY pa.lastUpdated DESC";

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void upsert(ArrivalEntity... entities);

    @Query(GET_ALL_QUERY)
    Single<List<ArrivalEntity>> getAllSingle();

    @Query(GET_ALL_QUERY)
    List<ArrivalEntity> getAll();

    @Query(GET_ALL_EXTRA_QUERY)
    List<ArrivalExtraEntity> getAllExtra();

    @Query(GET_ALL_EXTRA_QUERY)
    Single<List<ArrivalExtraEntity>> getAllExtraSingle();

    @Delete
    void delete(ArrivalEntity... entities);

    @Query("SELECT pa.*, l.title, l.type, l.favorite  FROM Arrival AS pa, Line as l" +
            " WHERE pa.lineId= :lineId" +
            " AND pa.toWay = :to AND pa.stationName= :stationName AND l.id = :lineId")
    ArrivalExtraEntity selectOne(long lineId, String stationName, String to);

    @Query("SELECT pa.*, l.title, l.type, l.favorite  FROM Arrival AS pa, Line as l" +
            " WHERE pa.lineId IN (:lineIds) AND pa.stationName IN(:stationNames)" +
            " AND pa.toWay IN (:tos) AND l.id IN (:lineIds)")
    List<ArrivalExtraEntity> selectSome(List<Long> lineIds, List<String> stationNames, List<String> tos);

    @Query("SELECT COUNT(*) FROM Arrival")
    Flowable<Integer> observeCount();
}
