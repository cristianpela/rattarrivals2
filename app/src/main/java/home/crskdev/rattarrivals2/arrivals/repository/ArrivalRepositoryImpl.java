package home.crskdev.rattarrivals2.arrivals.repository;

import java.util.Collections;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import home.crskdev.rattarrivals2.arrivals.model.ArrivalExtra;
import home.crskdev.rattarrivals2.util.ImmLists;
import home.crskdev.rattarrivals2.util.time.TimeEstimated;
import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;

import static home.crskdev.rattarrivals2.arrivals.repository.ArrivalRepositoryConverter.toEntityFromDTOExtra;

/**
 * Created by criskey on 21/9/2017.
 */
@Singleton
public class ArrivalRepositoryImpl implements ArrivalRepository {

    private ArrivalDAO dao;

    private PublishSubject<ArrivalExtra> deleteSubject;
    private PublishSubject<List<ArrivalExtra>> upsertSubject;

    @Inject
    public ArrivalRepositoryImpl(ArrivalDAO dao) {
        this.dao = dao;
        deleteSubject = PublishSubject.create();
        upsertSubject = PublishSubject.create();
    }

    @Override
    public Single<ArrivalExtra> pin(final ArrivalExtra arrival) {
        return Single.<ArrivalExtra>create(e -> {
            //extra check
            ArrivalExtra lArrival = arrival; // referencing locally
            if (!lArrival.pinned)
                lArrival = lArrival.togglePinned();
            dao.upsert(toEntityFromDTOExtra(lArrival));
            //from db
            ArrivalExtra output = ArrivalRepositoryConverter.toDTOExtraFromEntityExtra(dao.selectOne(
                    lArrival.lineId, lArrival.stationName, lArrival.toWay));
            //we make sure we have update the estimation with the one from parsing remote layer
            //because database layer can't get the right estimation
            /*
              {@link home.crskdev.rattarrivals2.util.time.TimeConverter}
             */
            output = output.update(lArrival.time, lArrival.lastUpdated);
            e.onSuccess(output);
        }).doOnSuccess(extra -> upsertSubject.onNext(Collections
                .singletonList(extra)))
                .subscribeOn(Schedulers.io());
    }

    @Override
    public Single<ArrivalExtra> unpin(final ArrivalExtra arrival) {
        return Single.<ArrivalExtra>create(e -> {
            //extra check
            ArrivalExtra lArrival = arrival; // referencing locally
            if (lArrival.pinned)
                lArrival = lArrival.togglePinned();
            dao.delete(toEntityFromDTOExtra(lArrival));
            e.onSuccess(arrival);
        }).doOnSuccess(extra -> deleteSubject.onNext(extra))
                .subscribeOn(Schedulers.io());
    }

    @Override
    public Observable<ArrivalExtra> observeUnpin() {
        return deleteSubject.serialize();
    }

    @Override
    public Single<List<ArrivalExtra>> getAllExtra() {
        return dao.getAllExtraSingle()
                .map(ael -> ImmLists.map(ael, ArrivalRepositoryConverter::toDTOExtraFromEntityExtra))
                .subscribeOn(Schedulers.io());
    }

    @Override
    public Single<List<ArrivalExtra>> tryRefresh(List<ArrivalExtra> extras) {
        return Single.<List<ArrivalExtra>>create(s -> {

            //extract a list of 3 list of fields: lineIDs , stationNames and to-s
            List<List<?>> compositeKeys = ArrivalRepositoryConverter
                    .extractCompositeKeysFromListExtras(extras);

            //getting pinned arrivals from database by intersecting with in-arrivals list
            @SuppressWarnings("unchecked")
            List<ArrivalExtraEntity> matchingArrivals = dao.selectSome(
                    (List<Long>) compositeKeys.get(ArrivalRepositoryConverter.KEY_LINE_ID),
                    (List<String>) compositeKeys.get(ArrivalRepositoryConverter.KEY_STATION_NAME),
                    (List<String>) compositeKeys.get(ArrivalRepositoryConverter.KEY_TO));

            if (!matchingArrivals.isEmpty()) {
                ImmLists.Chain<ArrivalExtraEntity> dbEntityChain = ImmLists.Chain
                        .from(matchingArrivals);

                //update database with new timeStr and lastUpdate
                dao.upsert(dbEntityChain
                        .setForEach(e -> updatedEntityExtra(e, extras))
                        .map(ArrivalRepositoryConverter::toEntityFromEntityExtra)
                        .toArray(ArrivalEntity.class));

                final List<ArrivalExtra> dbExtras = dbEntityChain
                        .map(ArrivalRepositoryConverter::toDTOExtraFromEntityExtra)
                        .get();
                //mark the original in - arrivals with pinned = true and updated timeStr accordingly
                // with matchingArrivals extras
                s.onSuccess(outputTryRefresh(extras, dbExtras));
                return;
            }
            //reach here when there is no match between in-arrivals and pinned arrivals from db
            s.onSuccess(extras);
        }).doOnSuccess(l -> upsertSubject.onNext(ImmLists.filter(l, a -> a.pinned)))
                .subscribeOn(Schedulers.io());
    }

    private List<ArrivalExtra> outputTryRefresh(final List<ArrivalExtra> extras, final List<ArrivalExtra> dbExtras) {
        return ImmLists.map(extras, in -> {
            for (ArrivalExtra db : dbExtras) {
                if (in.equals(db) && in.time.time != db.time.time) {
                    //we make sure we have update the estimation with the one from parsing remote layer
                    //because database layer can't get the right estimation
                    return db.update(in.time, in.lastUpdated);
                }
            }
            return in;
        });
    }

    private ArrivalExtraEntity updatedEntityExtra(ArrivalExtraEntity entity, List<ArrivalExtra> arrivals) {
        for (ArrivalExtra arrival : arrivals) {
            if (arrival.getId() == entity.getId()) {
                return ArrivalRepositoryConverter.toEntityExtraFromDTOExtra(arrival);
            }
        }
        throw new IllegalStateException("Should not reach here! Arrivals must contain the ArrivalExtra id");
    }

    @Override
    public Single<ArrivalExtra> refresh(ArrivalExtra arrival) {
        return pin(arrival);
    }

    @Override
    public Observable<List<ArrivalExtra>> observePin() {
        return upsertSubject
                .filter(l -> !l.isEmpty())
                .serialize();
    }

    @Override
    public Flowable<Boolean> observeShowing() {
        return dao.observeCount().map(n -> n > 0)
                .distinctUntilChanged().subscribeOn(Schedulers.io());
    }
}
