package home.crskdev.rattarrivals2.arrivals.model;

import android.support.annotation.ColorInt;
import android.support.annotation.DrawableRes;

import home.crskdev.rattarrivals2.model.BaseViewModel;
import home.crskdev.rattarrivals2.lines.model.LineType;
import home.crskdev.rattarrivals2.util.time.TimeEstimated;

/**
 * .
 * Created by criskey on 10/10/2017.
 */
public class ArrivalExtraViewModel extends BaseViewModel {

    public static final String PROPERTY_STALE_COLOR = "staleColor";

    //keys
    public final long lineId;

    public final String stationName;

    public final String toWay;

    //line
    public final String title;

    public final LineType lineType;

    @DrawableRes
    public final int lineImage;

    public final boolean favorite;

    //time
    public final long time;

    public final String timeStr;
    @ColorInt
    public final int estimationTimeColor;

    public final TimeEstimated timeEstimated;
    //last updated
    public final long lastUpdated;
    public final String lastUpdatedStr;
    //pinned
    public final boolean pinned;

    @ColorInt
    public int staleColor;

    public ArrivalExtraViewModel(long lineId, String stationName, String toWay,
                                 String title,
                                 LineType lineType,
                                 @DrawableRes int lineImage,
                                 boolean favorite,
                                 long time,
                                 String timeStr,
                                 @ColorInt int estimationTimeColor,
                                 TimeEstimated timeEstimated,
                                 @ColorInt int staleColor,
                                 long lastUpdated,
                                 String lastUpdatedStr,
                                 boolean pinned) {
        this.lineId = lineId;
        this.stationName = stationName;
        this.toWay = toWay;

        this.title = title;
        this.lineType = lineType;
        this.lineImage = lineImage;
        this.favorite = favorite;

        this.time = time;
        this.timeStr = timeStr;
        this.estimationTimeColor = estimationTimeColor;
        this.timeEstimated = timeEstimated;
        this.staleColor = staleColor;

        this.lastUpdated = lastUpdated;
        this.lastUpdatedStr = lastUpdatedStr;

        this.pinned = pinned;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ArrivalExtraViewModel that = (ArrivalExtraViewModel) o;

        return lineId == that.lineId && (stationName != null
                ? stationName.equals(that.stationName) : that.stationName == null
                && (toWay != null ? toWay.equals(that.toWay) : that.toWay == null));

    }

    @Override
    public int hashCode() {
        int result = (int) (lineId ^ (lineId >>> 32));
        result = 31 * result + (stationName != null ? stationName.hashCode() : 0);
        result = 31 * result + (toWay != null ? toWay.hashCode() : 0);
        return result;
    }

    public int getId() {
        return hashCode();
    }

    public String getIdStr() {
        return lineId + stationName + toWay;
    }

    public ArrivalExtraViewModel togglePinned() {
        return new ArrivalExtraViewModel(lineId, stationName, toWay,
                title, lineType, lineImage, favorite, time, timeStr, estimationTimeColor,
                timeEstimated, staleColor, lastUpdated, lastUpdatedStr, !pinned);
    }

    @Override
    public String toString() {
        return "ArrivalExtraViewModel{" +
                "lineId=" + lineId +
                ", stationName='" + stationName + '\'' +
                ", toWay='" + toWay + '\'' +
                ", title='" + title + '\'' +
                ", lineType=" + lineType +
                ", lineImage=" + lineImage +
                ", favorite=" + favorite +
                ", time=" + time +
                ", timeStr='" + timeStr + '\'' +
                ", estimationTimeColor=" + estimationTimeColor +
                ", staleColor=" + staleColor +
                ", lastUpdated=" + lastUpdated +
                ", lastUpdatedStr='" + lastUpdatedStr + '\'' +
                ", pinned=" + pinned +
                ", enabled=" + enabled +
                '}';
    }
}
