package home.crskdev.rattarrivals2.arrivals.presentation;

import android.support.annotation.VisibleForTesting;

import com.jakewharton.rxrelay2.PublishRelay;
import com.jakewharton.rxrelay2.Relay;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import home.crskdev.rattarrivals2.arrivals.ArrivalsConverter;
import home.crskdev.rattarrivals2.arrivals.model.ArrivalExtra;
import home.crskdev.rattarrivals2.arrivals.model.Route;
import home.crskdev.rattarrivals2.arrivals.services.ArrivalParseService;
import home.crskdev.rattarrivals2.core.RxBasePresenter;
import home.crskdev.rattarrivals2.di.PerActivity;
import home.crskdev.rattarrivals2.model.Action;
import home.crskdev.rattarrivals2.model.Result;
import home.crskdev.rattarrivals2.model.Result.Wait;
import home.crskdev.rattarrivals2.arrivals.repository.ArrivalRepository;
import home.crskdev.rattarrivals2.lines.repository.LineRepository;
import home.crskdev.rattarrivals2.util.ImmLists;
import home.crskdev.rattarrivals2.util.ResourceFinder;
import home.crskdev.rattarrivals2.util.asserts.Asserts;
import home.crskdev.rattarrivals2.util.time.CountValue;
import home.crskdev.rattarrivals2.util.time.CountValueCacheContainer;
import home.crskdev.rattarrivals2.util.time.IntervalCounter;
import home.crskdev.rattarrivals2.util.time.IntervalCounterImpl;
import home.crskdev.rattarrivals2.util.time.ResumableCountValue;
import home.crskdev.rattarrivals2.util.time.TimeUtils;
import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.disposables.Disposable;

import static home.crskdev.rattarrivals2.arrivals.ArrivalsConverter.toDTOExtraFromVMExtra;
import static home.crskdev.rattarrivals2.arrivals.ArrivalsConverter.toDTOFromDTOExtra;
import static home.crskdev.rattarrivals2.arrivals.ArrivalsConverter.toVMFromDTO;
import static home.crskdev.rattarrivals2.model.Result.BackResult;
import static home.crskdev.rattarrivals2.model.Result.LineResult;
import static home.crskdev.rattarrivals2.model.Result.PinnedResult;
import static home.crskdev.rattarrivals2.model.Result.Refresh;
import static home.crskdev.rattarrivals2.model.Result.RouteResult;
import static home.crskdev.rattarrivals2.model.Result.VOID_RESULT;
import static home.crskdev.rattarrivals2.model.Result.caseOf;
import static home.crskdev.rattarrivals2.util.LifecycleCompositeDisposable.DisposePoint.PAUSE;
import static home.crskdev.rattarrivals2.util.time.TimeUtils.DEBOUNCE_TIME_VALUE;
import static home.crskdev.rattarrivals2.util.time.TimeUtils.THROTTLE_TIME_VALUE;

/**
 * .
 * Created by criskey on 20/8/2017.
 */
@SuppressWarnings("WeakerAccess")
@PerActivity
public class ArrivalsPresenter extends RxBasePresenter<ArrivalsView> {

    public static final String KEY_LINE_ID = "KEY_LINE_ID";
    private static final String KEY_COOLDOWN_COUNT_CACHE = ArrivalsPresenter.class + "#cooldown";
    private ArrivalParseService arrivalSvc;
    private ArrivalRepository pinnedSvc;
    private LineRepository repository;

    private CountValueCacheContainer countValueContainer;
    private IntervalCounter countdownCooldown;

    private ResourceFinder resourceFinder;

    @Inject
    public ArrivalsPresenter(Scheduler uiScheduler,
                             ArrivalParseService arrivalSvc,
                             ArrivalRepository pinnedSvc,
                             LineRepository repository,
                             CountValueCacheContainer countValueContainer,
                             ResourceFinder resourceFinder) {
        super(uiScheduler);
        init(arrivalSvc, pinnedSvc, repository, countValueContainer, resourceFinder,
                new IntervalCounterImpl(computationScheduler));
    }

    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    ArrivalsPresenter(Scheduler uiScheduler,
                      ArrivalParseService arrivalSvc,
                      ArrivalRepository pinnedSvc,
                      LineRepository repository,
                      CountValueCacheContainer countValueContainer,
                      ResourceFinder resourceFinder, IntervalCounter countDown) {
        super(uiScheduler);
        init(arrivalSvc, pinnedSvc, repository, countValueContainer, resourceFinder, countDown);
    }

    private void init(ArrivalParseService arrivalSvc, ArrivalRepository pinnedSvc, LineRepository repository,
                      CountValueCacheContainer countValueContainer, ResourceFinder resourceFinder,
                      IntervalCounter countdownCooldown) {
        this.repository = repository;
        this.arrivalSvc = arrivalSvc;
        this.pinnedSvc = pinnedSvc;
        this.countValueContainer = countValueContainer;
        this.countdownCooldown = countdownCooldown;
        this.resourceFinder = resourceFinder;
    }

    @Override
    protected void onPostAttachView() {
        final Long lineId = getLineId();
        //get the line selected line from db
        disposables.add(repository.getLinesById(lineId)
                .compose(scheduleOnUISingleTransformer())
                .subscribe(view::onLoadLine));
        //observe the changes done for the selected line(favorite/unfavorite)
        disposables.add(repository.observeChanges()
                .compose(scheduleOnUIFlowableTransformer())
                .filter(line -> line.id == lineId)
                .subscribe(view::onChangeFavoriteLine));
    }

    private Long getLineId() {
        return (Long) extras.get(KEY_LINE_ID);
    }

    private void goBack() {
        view.onGoBack();
    }

    private void showRouteResult(RouteResult result) {
        Route route = result.data;
        view.onDisplayArrivals(ImmLists.map(route.arrivals,
                (arrival) -> ArrivalsConverter.toVMFromDTO(arrival, resourceFinder, false)),
                route.from, route.to);
    }

    private void startCooldown(CountValue countValue) {
        view.onDone();
        view.onCoolDownEnd();
        //right now we eliminate cooldown
//        //todo when coming while cooldown, how about load the screen automatically after countdown?
//        disposables.add(countdownCooldown.startCount(countValue)
//                .observeOn(uiScheduler)
//                .doOnTerminate(() -> view.onCoolDownEnd())
//                .subscribe(c -> {
//                    if (c.isFlag()) {
//                        view.onCoolDownStart((int) c.max);
//                    } else {
//                        view.onCoolDown((int) c.getValue());
//                    }
//                }), PAUSE);
    }

    private void showPinnedResult(PinnedResult result) {
        view.onChangePinnedArrival(toVMFromDTO(toDTOFromDTOExtra(result.data), resourceFinder, false));
    }

    private void showError(Result.Error result) {
        view.onError(result.error);
        view.onDone();
    }

    private void showWaitDone(Result result) {
        if (result == Wait.NO_WAIT) {
            view.onDone();
        } else {
            view.onWait();
        }
    }

    public void bindActions(Observable<Action> actionObs) {
        Disposable actionsDisposable = actionObs
                .publish(obs -> {
                    Observable<Result> favorite = obs.ofType(Action.FavoriteAction.class)
                            .flatMap(action -> repository
                                    .getLinesById(getLineId())
                                    .flatMapObservable(line -> repository
                                            .update(line.toggle())
                                            .andThen(VOID_RESULT.observable())));

                    Observable<Result> loadRoute = obs.ofType(Action.LoadAction.class)
                            .filter(__ -> {
                                //we make sure that is not a cooldown in progress already
                                ResumableCountValue rcv = countValueContainer.get(KEY_COOLDOWN_COUNT_CACHE);
                                return rcv == null || rcv.resume().hasReachedEnd();
                            })
                            //todo throttle might be superseeded here
                            .throttleFirst(THROTTLE_TIME_VALUE.value, THROTTLE_TIME_VALUE.unit,
                                    computationScheduler)
                            .switchMap((action) -> Observable.concat(
                                    Wait.WAIT.observable(),
                                    arrivalSvc.fetchRoute(action)
                                            .switchMap(this::getUpdatedRouteResultObservable),
                                    Refresh.REFRESH.observable()));

                    Observable<Result> changePinned = obs.ofType(Action.Pinned.class)
                            .debounce(DEBOUNCE_TIME_VALUE.value, DEBOUNCE_TIME_VALUE.unit,
                                    computationScheduler)
                            .switchMap((action) -> {
                                final ArrivalExtra togglePinned = toDTOExtraFromVMExtra(action.viewModel)
                                        .togglePinned();
                                final Observable<ArrivalExtra> result = togglePinned.pinned
                                        ? pinnedSvc.pin(togglePinned).toObservable()
                                        : pinnedSvc.unpin(togglePinned).toObservable();
                                return result.map(a -> new PinnedResult(togglePinned));
                            });

                    Observable<Result> changeWays = obs.ofType(Action.ChangeWayAction.class)
                            .flatMap((action) -> arrivalSvc
                                    .changeWay()
                                    .switchMap(this::getUpdatedRouteResultObservable));

                    Observable<Result> back = obs.ofType(Action.Back.class)
                            .map(__ -> BackResult.BACK);

                    return Observable
                            .merge(Arrays.asList(favorite, loadRoute, changePinned, changeWays, back));
                })
                .onErrorReturn(Result.Error::new)
                .observeOn(uiScheduler)
                .subscribe(result -> {
                    if (caseOf(result, Wait.class)) {
                        showWaitDone(result);
                    } else if (caseOf(result, Result.Error.class)) {
                        showError((Result.Error) result);
                    } else if (caseOf(result, PinnedResult.class)) {
                        showPinnedResult((PinnedResult) result);
                    } else if (caseOf(result, Refresh.class)) {
                        startCooldown(CountValue.startAtMax(0,
                                TimeUtils.COOLDOWN_TIME_VALUE.value,
                                TimeUtils.COOLDOWN_TIME_VALUE.unit,
                                CountValue.BACKWARD));
                    } else if (caseOf(result, RouteResult.class)) {
                        showRouteResult((RouteResult) result);
                    } else if (caseOf(result, BackResult.class)) {
                        goBack();
                    }
                });
        disposables.add(actionsDisposable);
    }

    private Observable<Result.RouteResult> getUpdatedRouteResultObservable(RouteResult route) {
        return Observable.defer(() -> pinnedSvc.tryRefresh(ImmLists.map(route.data.arrivals,
                ArrivalsConverter::composeWithLineIgnored))
                .map(l -> updateRoute(route, l)).toObservable());
    }

    /**
     * updates the remotely fetched routeResult with the pinned Arrivals from db
     *
     * @param routeResult original route
     * @param arrivals    updated arrivals from db
     * @return transformed result
     */
    private RouteResult updateRoute(RouteResult routeResult, List<ArrivalExtra> arrivals) {
        Route updatedRoute = new Route(routeResult.data.lineId,
                routeResult.data.from,
                routeResult.data.to,
                ImmLists.map(arrivals, ArrivalsConverter::toDTOFromDTOExtra),
                routeResult.data.lastUpdated);
        return new RouteResult(updatedRoute);
    }


    @Override
    public void onViewPaused() {
        super.onViewPaused();
        CountValue cooldownValue = countdownCooldown.getValue();
        if (cooldownValue != null)
            countValueContainer.put(KEY_COOLDOWN_COUNT_CACHE,
                    ResumableCountValue.from(cooldownValue));
    }

    @Override
    public void onViewResumed() {
        ResumableCountValue rCoolDownValue = countValueContainer.get(KEY_COOLDOWN_COUNT_CACHE);
        if (rCoolDownValue != null) {
            CountValue countValue = rCoolDownValue.resumeAt(uiScheduler.now(TimeUnit.MILLISECONDS));
            if (!countValue.hasReachedEnd()) {
                startCooldown(countValue);
            } else {
                view.onCoolDownEnd();
            }
        }
    }


}
