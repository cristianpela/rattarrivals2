package home.crskdev.rattarrivals2.arrivals.model;

import android.support.annotation.ColorInt;

import home.crskdev.rattarrivals2.model.BaseViewModel;
import home.crskdev.rattarrivals2.util.time.TimeEstimated;

/**
 * Created by criskey on 4/10/2017.
 */
@SuppressWarnings("WeakerAccess")
public class ArrivalViewModel extends BaseViewModel {

    public final long lineId;

    public final String stationName;

    public final String to;

    public String timeStr;

    public long time;

    @ColorInt
    public int estimationTimeColor;

    public final TimeEstimated timeEstimated;

    public String lastUpdatedStr;

    public long lastUpdated;

    public boolean pinned;

    public ArrivalViewModel(long lineId, String stationName, String to,
                            long time,
                            String timeStr,
                            @ColorInt int estimationTimeColor,
                            TimeEstimated timeEstimated,
                            long lastUpdated,
                            String lastUpdatedStr,
                            boolean pinned) {
        this.lineId = lineId;
        this.stationName = stationName;
        this.to = to;

        this.time = time;
        this.timeStr = timeStr;
        this.estimationTimeColor = estimationTimeColor;
        this.timeEstimated = timeEstimated;

        this.lastUpdated = lastUpdated;
        this.lastUpdatedStr = lastUpdatedStr;

        this.pinned = pinned;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ArrivalViewModel that = (ArrivalViewModel) o;

        if (lineId != that.lineId) return false;
        if (stationName != null ? !stationName.equals(that.stationName) : that.stationName != null)
            return false;
        return to != null ? to.equals(that.to) : that.to == null;

    }

    @Override
    public int hashCode() {
        int result = (int) (lineId ^ (lineId >>> 32));
        result = 31 * result + (stationName != null ? stationName.hashCode() : 0);
        result = 31 * result + (to != null ? to.hashCode() : 0);
        return result;
    }

    public int getId() {
        return hashCode();
    }

    @Override
    public String toString() {
        return "ArrivalViewModel{" +
                "lineId=" + lineId +
                ", stationName='" + stationName + '\'' +
                ", to='" + to + '\'' +
                ", timeStr='" + timeStr + '\'' +
                ", estimationTimeColor=" + estimationTimeColor +
                ", lastUpdatedStr='" + lastUpdatedStr + '\'' +
                ", pinned=" + pinned +
                '}';
    }
}
