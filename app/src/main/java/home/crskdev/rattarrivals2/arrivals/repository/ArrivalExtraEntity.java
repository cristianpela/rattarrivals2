package home.crskdev.rattarrivals2.arrivals.repository;

import home.crskdev.rattarrivals2.arrivals.model.ArrivalExtra;
import home.crskdev.rattarrivals2.lines.model.LineType;

/**
 *
 * Created by criskey on 10/10/2017.
 */
public class ArrivalExtraEntity {

    public final long lineId;

    public final String title;

    public final LineType type;

    public final boolean favorite;

    public final String stationName;

    public final long time;

    public final String toWay;

    public final long lastUpdated;

    public ArrivalExtraEntity(long lineId, String stationName, String toWay, String title, LineType type,
                              boolean favorite, long time, long lastUpdated) {
        this.title = title;
        this.type = type;
        this.favorite = favorite;
        this.lineId = lineId;
        this.stationName = stationName;
        this.time = time;
        this.toWay = toWay;
        this.lastUpdated = lastUpdated;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ArrivalExtra that = (ArrivalExtra) o;

        if (lineId != that.lineId) return false;
        if (stationName != null ? !stationName.equals(that.stationName) : that.stationName != null)
            return false;
        return toWay != null ? toWay.equals(that.toWay) : that.toWay == null;

    }

    @Override
    public int hashCode() {
        int result = (int) (lineId ^ (lineId >>> 32));
        result = 31 * result + (stationName != null ? stationName.hashCode() : 0);
        result = 31 * result + (toWay != null ? toWay.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "ArrivalExtraEntity{" +
                "lineId=" + lineId +
                ", stationName='" + stationName + '\'' +
                ", timeStr=" + time +
                ", toWay='" + toWay + '\'' +
                '}';
    }

    public int getId() {
        return hashCode();
    }

}
