package home.crskdev.rattarrivals2.arrivals.presentation;

import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import home.crskdev.rattarrivals2.R;
import home.crskdev.rattarrivals2.arrivals.model.ArrivalViewModel;
import home.crskdev.rattarrivals2.util.ButterKnifeViewHolder;
import home.crskdev.rattarrivals2.util.recyclerview.AbstractRecyclerViewAdapter;
import home.crskdev.rattarrivals2.util.recyclerview.DefaultViewHolderBinder;

/**
 * Created by criskey on 24/8/2017.
 */
class ArrivalsAdapter extends AbstractRecyclerViewAdapter<ArrivalViewModel, ArrivalsAdapter.ArrivalsVH> {

    ArrivalsAdapter() {
        super(new ArrivalsVHBinder());
    }


    public static class ArrivalsVH extends ButterKnifeViewHolder {

        @BindView(R.id.txt_station_name)
        TextView txtStationName;
        @BindView(R.id.txt_time)
        TextView txtTime;
        @BindView(R.id.img_pinned)
        ImageView imgPinned;

        public ArrivalsVH(View itemView) {
            super(itemView);
            imgPinned.setColorFilter(ContextCompat.getColor(itemView.getContext(), R.color.colorAccent));
        }
    }

    private static class ArrivalsVHBinder extends DefaultViewHolderBinder<ArrivalsVH, ArrivalViewModel> {

        ArrivalsVHBinder() {
            super(R.layout.list_arrivals_item);
        }

        @Override
        public void onBind(ArrivalsVH holder, ArrivalViewModel item) {
            holder.itemView.setTag(item);
            holder.txtStationName.setText(item.stationName);
            holder.txtTime.setText(item.timeStr);
            holder.txtTime.setTextColor(item.estimationTimeColor);
            holder.imgPinned.setVisibility(item.pinned ? View.VISIBLE : View.INVISIBLE);
        }

        @Override
        public Class<ArrivalsVH> getViewHolderClass() {
            return ArrivalsVH.class;
        }
    }

}
