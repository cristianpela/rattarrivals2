package home.crskdev.rattarrivals2;

import dagger.android.AndroidInjector;
import dagger.android.support.DaggerApplication;
import home.crskdev.rattarrivals2.di.DaggerRATTArrivals2AppComponent;
import timber.log.Timber;

/**
 * .
 * Created by criskey on 8/8/2017.
 */
public class RATTArrivals2App extends DaggerApplication {

    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        return DaggerRATTArrivals2AppComponent.builder().create(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        if(BuildConfig.DEBUG){
            Timber.plant(new Timber.DebugTree());
            Timber.tag("CRSK-");
        }
    }

}
