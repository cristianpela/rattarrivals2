package home.crskdev.rattarrivals2.init;

import android.content.Intent;
import android.graphics.drawable.TransitionDrawable;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import home.crskdev.rattarrivals2.R;
import home.crskdev.rattarrivals2.core.BaseActivity;
import home.crskdev.rattarrivals2.lines.presentation.LineActivity;

/**
 * Created by criskey on 8/8/2017.
 */

public class InitActivity extends BaseActivity<InitView, InitPresenter> implements InitView {

    @Inject
    InitPresenter presenter;

    @BindView(R.id.imgDownloadAuto)
    ImageView imgDownloadAuto;

    @BindView(R.id.imgDownloadTram)
    ImageView imgDownloadTram;

    @BindView(R.id.imgDownloadTrolley)
    ImageView imgDownloadTrolley;

    @BindView(R.id.cardError)
    ViewGroup frameError;

    @BindView(R.id.frameProgress)
    ViewGroup frameProgress;

    @BindView(R.id.layoutNotification)
    ViewGroup frameNotification;

    @BindView(R.id.init_activity_layout)
    ViewGroup frameRoot;

    @BindView(R.id.txtError)
    TextView txtError;

    @BindView(R.id.txtStep)
    TextView txtStep;

    @Override
    public void onReset() {
        resetCheckTransitions();
        frameRoot.setVisibility(View.VISIBLE);
        frameProgress.setVisibility(View.VISIBLE);
        frameError.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onInitDone() {
        Animation fadeOutAnim = AnimationUtils.loadAnimation(this, android.R.anim.fade_out);
        fadeOutAnim.setFillAfter(true);
        frameProgress.startAnimation(fadeOutAnim);
        launchMainActivity();
    }

    private void launchMainActivity() {
        startActivity(new Intent(getApplicationContext(), LineActivity.class)
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
    }

    @Override
    public void onDownloadStep(int step, int total) {
        txtStep.setText(step + "/" + total);
    }

    @Override
    public void onDownloadAuto() {
        startCheckTransition(imgDownloadAuto);
    }

    @Override
    public void onDownloadTram() {
        startCheckTransition(imgDownloadTram);
    }

    @Override
    public void onDownloadTrolley() {
        startCheckTransition(imgDownloadTrolley);
    }

    @Override
    public void onError(String error) {
        txtError.setText(error);
        frameProgress.setVisibility(View.INVISIBLE);
        frameError.setVisibility(View.VISIBLE);
        frameError.startAnimation(AnimationUtils
                .loadAnimation(this, R.anim.slide_in_from_right));
    }

    @Override
    public InitPresenter presenter() {
        return presenter;
    }

    @Override
    public int layoutId() {
        return R.layout.init_activity_layout;
    }

    @OnClick(R.id.btn_init_retry)
    public void actionRetry() {
        presenter.retry();
    }

    private void resetCheckTransition(ImageView imageView) {
        TransitionDrawable drawable = (TransitionDrawable) imageView.getDrawable();
        drawable.resetTransition();
    }

    private void startCheckTransition(ImageView imageView) {
        TransitionDrawable drawable = (TransitionDrawable) imageView.getDrawable();
        drawable.startTransition(getResources().getInteger(R.integer.animDurationMillis));
    }

    private void resetCheckTransitions() {
        resetCheckTransition(imgDownloadAuto);
        resetCheckTransition(imgDownloadTram);
        resetCheckTransition(imgDownloadTrolley);
    }


    private abstract static class SimpleAnimationListener implements Animation.AnimationListener {

        @Override
        public void onAnimationStart(Animation animation) {

        }

        @Override
        public void onAnimationEnd(Animation animation) {

        }

        @Override
        public void onAnimationRepeat(Animation animation) {

        }
    }


}
