package home.crskdev.rattarrivals2.init;

import home.crskdev.rattarrivals2.lines.model.LineType;

import static home.crskdev.rattarrivals2.lines.model.LineType.NONE;

/**
 * Created by criskey on 10/8/2017.
 */

public abstract class InitState {

    public static <I extends InitState> boolean eq(InitState state, Class<?> clazz) {
        if (state == null || clazz == null) return false;
        return state.getClass().equals(clazz);
    }

    public static class Download extends InitState {

        public static final Download DEFAULT = new Download(NONE, 0, 3);

        public final LineType type;
        public final int step;
        public final int totalSteps;

        public Download(LineType type, int step, int totalSteps) {
            this.type = type;
            this.step = step;
            this.totalSteps = totalSteps;
        }

        public Download increment(LineType type) {
            return new Download(type, step + 1, totalSteps);
        }

        @Override
        public String toString() {
            return "Download{" +
                    "type=" + type +
                    ", step=" + step +
                    ", totalSteps=" + totalSteps +
                    '}';
        }
    }

    public static class Reset extends InitState {
        public Reset() {
        }

        @Override
        public String toString() {
            return "Reset{}";
        }
    }

    public static class Initialized extends InitState {
        public Initialized() {
        }

        @Override
        public String toString() {
            return "Initialized{}";
        }
    }

    public static class Error extends InitState {
        public final Throwable error;

        public Error(Throwable error) {
            this.error = error;
        }

        @Override
        public String toString() {
            return "Error{" +
                    "error=" + error +
                    '}';
        }
    }



}
