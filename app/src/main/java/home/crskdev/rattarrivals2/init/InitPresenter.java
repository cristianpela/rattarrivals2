package home.crskdev.rattarrivals2.init;

import javax.inject.Inject;

import home.crskdev.rattarrivals2.core.RxBasePresenter;
import home.crskdev.rattarrivals2.di.PerActivity;
import home.crskdev.rattarrivals2.init.InitState.Initialized;
import io.reactivex.Scheduler;

import static home.crskdev.rattarrivals2.init.InitState.Download;
import static home.crskdev.rattarrivals2.init.InitState.Reset;
import static home.crskdev.rattarrivals2.init.InitState.eq;

/**
 * Created by criskey on 10/8/2017.
 */
@PerActivity
public class InitPresenter extends RxBasePresenter<InitView> {

    private final InitService initService;

    @Inject
    public InitPresenter(Scheduler uiScheduler, InitService initService) {
        super(uiScheduler);
        this.initService = initService;
    }

    @Override
    protected void onPostAttachView() {
        checkInitService();
    }

    public void retry() {
        checkInitService();
    }

    private void checkInitService() {
        initService.init()
                .doOnNext(System.out::println)
                .observeOn(uiScheduler)
                .subscribe(state -> view(v -> {
                            if (eq(state, Download.class)) {
                                Download d = (Download) state;
                                v.onDownloadStep(d.step, d.totalSteps);
                                switch (d.type) {
                                    case AUTO:
                                        v.onDownloadAuto();
                                        break;
                                    case TRAM:
                                        v.onDownloadTram();
                                        break;
                                    case TROLLEY:
                                        v.onDownloadTrolley();
                                        break;
                                }
                            } else if (eq(state, Reset.class)) {
                                v.onDownloadStep(Download.DEFAULT.step, Download.DEFAULT.totalSteps);
                                v.onReset();
                            } else if (eq(state, Initialized.class)){

                            }
                        }),
                        err -> view(v -> v.onError(err.getMessage())),
                        () -> view(InitView::onInitDone));
    }
}
