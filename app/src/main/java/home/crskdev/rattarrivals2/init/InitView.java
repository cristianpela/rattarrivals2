package home.crskdev.rattarrivals2.init;

import home.crskdev.rattarrivals2.core.IView;

/**
 * Created by criskey on 10/8/2017.
 */

interface InitView extends IView {
    void onReset();

    void onInitDone();

    void onDownloadStep(int step, int total);

    void onDownloadAuto();

    void onDownloadTram();

    void onDownloadTrolley();

    void onError(String error);
}
