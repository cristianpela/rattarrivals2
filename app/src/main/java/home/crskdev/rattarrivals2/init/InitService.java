package home.crskdev.rattarrivals2.init;

import java.util.Collections;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import home.crskdev.rattarrivals2.di.PerActivity;
import home.crskdev.rattarrivals2.lines.model.Line;
import home.crskdev.rattarrivals2.lines.model.LineType;
import home.crskdev.rattarrivals2.init.InitState.Download;
import home.crskdev.rattarrivals2.init.InitState.Initialized;
import home.crskdev.rattarrivals2.lines.services.LineParseService;
import home.crskdev.rattarrivals2.repository.CheckService;
import home.crskdev.rattarrivals2.lines.repository.LineRepository;
import home.crskdev.rattarrivals2.util.Pair;
import io.reactivex.Observable;
import io.reactivex.ObservableTransformer;
import io.reactivex.schedulers.Schedulers;

import static home.crskdev.rattarrivals2.init.InitState.Reset;


/**
 * Created by criskey on 10/8/2017.
 */
@PerActivity
public class InitService {

    private final LineRepository lineRepository;
    private final CheckService lineCheckService;
    private LineParseService lineParseService;

    @Inject
    public InitService(@Named("LPS-CACHE") LineParseService lineParseService,
                       LineRepository lineRepository,
                       CheckService lineCheckService) {
        this.lineParseService = lineParseService;
        this.lineRepository = lineRepository;
        this.lineCheckService = lineCheckService;
    }

    public Observable<InitState> init() {

        Observable<Pair<LineType, List<Line>>> autoLines = lineParseService.parseLine(LineType.AUTO)
                .map(l -> Pair.of(LineType.AUTO, l));
        Observable<Pair<LineType, List<Line>>> tramLines = lineParseService.parseLine(LineType.TRAM)
                .map(l -> Pair.of(LineType.TRAM, l));
        Observable<Pair<LineType, List<Line>>> trolleyLines = lineParseService.parseLine(LineType.TROLLEY)
                .map(l -> Pair.of(LineType.TROLLEY, l));

        Observable<Pair<LineType, List<Line>>> downloads = Observable.merge(autoLines, tramLines, trolleyLines);
        Observable<Pair<InitState, List<Line>>> tracker = downloads
                .scan(Pair.of(Download.DEFAULT, Collections.<Line>emptyList()),
                        (acc, pairLine) -> Pair.of(((Download) acc.first).increment(pairLine.first),
                                pairLine.second));
        ObservableTransformer<Pair<InitState, List<Line>>, InitState> saver =
                upstream -> upstream.flatMap(pair -> lineRepository.create(pair.second)
                        .andThen(Observable.just(pair.first)));
        Observable<InitState> checker = lineCheckService.isInit()
                .flatMapObservable(check -> (check)
                        ? Observable.just(new Initialized())
                        : tracker.skip(1).compose(saver).startWith(new Reset()));

        return checker
                .doOnComplete(lineCheckService::setInit)
                .subscribeOn(Schedulers.io());
    }

}
