package home.crskdev.rattarrivals2.init;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
import home.crskdev.rattarrivals2.di.PerActivity;
import home.crskdev.rattarrivals2.lines.services.LineParseService;
import home.crskdev.rattarrivals2.net.Client;
import home.crskdev.rattarrivals2.lines.services.CacheLineParseService;
import home.crskdev.rattarrivals2.lines.services.LineParseServiceImpl;
import home.crskdev.rattarrivals2.repository.CheckService;

/**
 * Created by criskey on 8/8/2017.
 */
@Module
public abstract class InitModule {

    @Provides
    @Named("LPS-SRC")
    public static LineParseService provideLineParseService(Client client) {
        return new LineParseServiceImpl(client);
    }

    @PerActivity
    @Named("LPS-CACHE")
    @Provides
    public static LineParseService
    provideCacheLineParseService(@Named("LPS-SRC") LineParseService src, CheckService checkService) {
        return new CacheLineParseService(src);
    }


}
