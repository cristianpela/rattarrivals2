package home.crskdev.rattarrivals2.model;

import java.util.concurrent.TimeUnit;

import home.crskdev.rattarrivals2.arrivals.model.Arrival;
import home.crskdev.rattarrivals2.arrivals.model.ArrivalExtra;
import home.crskdev.rattarrivals2.arrivals.model.Route;
import home.crskdev.rattarrivals2.lines.model.Line;
import home.crskdev.rattarrivals2.util.recyclerview.TapableRecyclerView;
import io.reactivex.Observable;

/**
 * Created by criskey on 23/8/2017.
 */

public abstract class Result {

    public static final TransientResult VOID_RESULT = new TransientResult() {
    };

    public static boolean caseOf(Result result, Class<? extends Result> toCompare) {
        return result.getClass().equals(toCompare);
    }

    public Observable<Result> observable() {
        return Observable.just(this);
    }

    public static final class Wait extends TransientResult {

        public static final Wait NO_WAIT = new Wait("NO_WAIT");

        public static final Wait WAIT = new Wait("WAIT");

        private final String tag;

        private Wait(String tag) {
            this.tag = tag;
        }

        @Override
        public String toString() {
            return "Wait{" + tag + "}";
        }
    }

    public static final class Cooldown extends TransientResult {

        public static final int FLAG_START_VALUE = Integer.MIN_VALUE;

        public final int value;

        public final int max;

        public Cooldown(int value, int max) {
            this.value = value;
            this.max = max;
        }

        public static Cooldown asStartFlag(int max) {
            return new Cooldown(FLAG_START_VALUE, max);
        }

        public static Cooldown just(int startValue) {
            return new Cooldown(startValue, startValue);
        }

        public boolean hasEnd() {
            return value == 0;
        }


        public boolean isRunning(){
            return value > 0 && value < max;
        }

        public boolean isStart() {
            return value == FLAG_START_VALUE;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Cooldown cooldown = (Cooldown) o;

            return value == cooldown.value && max == cooldown.max;

        }

        @Override
        public int hashCode() {
            int result = value;
            result = 31 * result + max;
            return result;
        }

        @Override
        public String toString() {
            return "Cooldown{" +
                    "value=" + value +
                    '}';
        }
    }

    public static class TapResult extends Data<TapableRecyclerView.TapEvent>{

        public TapResult(TapableRecyclerView.TapEvent data) {
            super(data);
        }
    }

    public static final class Error extends TransientResult {

        public static final Error NO_ERROR = new Error(NoError.SINGLETON);
        public final Throwable error;

        public Error(Throwable error) {
            this.error = error;
        }


        @Override
        public String toString() {
            return "Error{" + "error=" + error.getMessage() + '}';
        }

        @Override
        public boolean equals(Object o) {
            if (o != null && o instanceof NoError) {
                return !(this.error instanceof NoError);
            }
            return super.equals(o);
        }

        private static class NoError extends Throwable {

            @SuppressWarnings("ThrowableInstanceNeverThrown")
            private static final NoError SINGLETON = new NoError("NO ERROR");

            private NoError(String message) {
                super(message);
            }
        }
    }

    public static abstract class Data<T> extends Result {

        public final T data;

        public Data(T data) {
            this.data = data;
        }

        @Override
        public String toString() {
            return this.getClass().getSimpleName() + "{" + data + '}';
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Data<?> data1 = (Data<?>) o;

            return data != null ? data.equals(data1.data) : data1.data == null;

        }

        @Override
        public int hashCode() {
            return data != null ? data.hashCode() : 0;
        }
    }

    public static abstract class TransientData<T> extends TransientResult {

        public final T data;

        public TransientData(T data) {
            this.data = data;
        }

        @Override
        public String toString() {
            return this.getClass().getSimpleName() + "{" + data + '}';
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            TransientData<?> data1 = (TransientData<?>) o;

            return data != null ? data.equals(data1.data) : data1.data == null;

        }

        @Override
        public int hashCode() {
            return data != null ? data.hashCode() : 0;
        }
    }

    /**
     * Created by criskey on 30/8/2017.
     */
    public static final class RouteResult extends TransientData<Route> {
        public RouteResult(Route data) {
            super(data);
        }
    }

    /**
     * Created by criskey on 30/8/2017.
     */
    public static final class StationResult extends TransientData<Arrival> {
        public StationResult(Arrival arrival) {
            super(arrival);
        }
    }

    public static final class LineResult extends TransientData<Line> {
        public LineResult(Line line) {
            super(line);
        }
    }

    public static class PinnedResult extends TransientData<ArrivalExtra> {
        public PinnedResult(ArrivalExtra data) {
            super(data);
        }
    }

    //@formatter:off
    public static class Refresh extends TransientResult{
        public static final Refresh REFRESH = new Refresh();
        private Refresh(){}
    }

    public static class PinnedRemoved extends TransientResult{
        public static final PinnedRemoved PINNED_REMOVED = new PinnedRemoved();
        private PinnedRemoved(){}}

    public static class BackResult extends TransientResult{
        public static final BackResult BACK = new BackResult();
        private BackResult(){}}

    public static abstract class TransientResult extends Result {}
    //@formatter:on
}
