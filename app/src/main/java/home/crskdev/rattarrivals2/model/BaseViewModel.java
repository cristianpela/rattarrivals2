package home.crskdev.rattarrivals2.model;

/**
 * Created by criskey on 4/10/2017.
 */
@SuppressWarnings("WeakerAccess")
public abstract class BaseViewModel {

    public static final String PROPERTY_ENABLED = "enabled";

    public boolean enabled;

    public BaseViewModel(boolean enabled) {
        this.enabled = enabled;
    }

    public BaseViewModel() {
        this(true);
    }

}
