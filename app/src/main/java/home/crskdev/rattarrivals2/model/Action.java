package home.crskdev.rattarrivals2.model;

import home.crskdev.rattarrivals2.arrivals.model.ArrivalExtraViewModel;
import home.crskdev.rattarrivals2.arrivals.model.ArrivalViewModel;
import home.crskdev.rattarrivals2.util.recyclerview.TapableRecyclerView;

/**
 * Created by criskey on 23/8/2017.
 */

public abstract class Action {

    public static boolean caseOf(Action action, Class<? extends Action> toCompare){
        return action.getClass().equals(toCompare);
    }

    public static final class Error{
        public final Throwable error;

        public Error(Throwable error) {
            this.error = error;
        }
    }

    public static final class TapableAction extends Action{
        public final TapableRecyclerView.TapEvent tapEvent;

        public TapableAction(TapableRecyclerView.TapEvent tapEvent) {
            this.tapEvent = tapEvent;
        }
    }

    /**
     * Created by criskey on 30/8/2017.
     */
    public static final class ChangeWayAction extends Action {

        public static final ChangeWayAction CHANGE_WAY_ACTION = new ChangeWayAction();

        public ChangeWayAction() {

        }
    }

    /**
     * Created by criskey on 30/8/2017.
     */
    public static class FavoriteAction extends Action {
        public final long lineId;

        public FavoriteAction(long lineId) {
            this.lineId = lineId;
        }
    }

    /**
     * Created by criskey on 30/8/2017.
     */
    public static class LineAction extends Action {
        public final long lineId;

        public LineAction(long lineId) {
            this.lineId = lineId;
        }
    }

    /**
     * Created by criskey on 30/8/2017.
     */
    public static final class LoadAction extends Action {

        public final long lineId;

        public LoadAction(long lineId) {
            this.lineId = lineId;
        }
    }

    /**
     * Created by criskey on 30/8/2017.
     */
    public static class Pinned extends Action {
        public final ArrivalExtraViewModel viewModel;

        public Pinned(ArrivalExtraViewModel viewModel) {
            this.viewModel = viewModel;
        }

    }

    /**
     * Created by criskey on 30/8/2017.
     */ // bindActions
    public static final class StationAction extends Action {

        public final ArrivalViewModel viewModel;

        public StationAction(ArrivalViewModel viewModel) {
            this.viewModel = viewModel;
        }
    }


    public static class Back extends Action{

        public static final Back BACK = new Back();

        private Back(){

        }
    }
}
