package home.crskdev.rattarrivals2.core;

import android.support.annotation.CallSuper;
import android.support.annotation.VisibleForTesting;

import home.crskdev.rattarrivals2.util.LifecycleCompositeDisposable;
import io.reactivex.FlowableTransformer;
import io.reactivex.MaybeTransformer;
import io.reactivex.ObservableTransformer;
import io.reactivex.Scheduler;
import io.reactivex.SingleTransformer;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

import static home.crskdev.rattarrivals2.util.LifecycleCompositeDisposable.DisposePoint.*;

/**
 * .
 * Created by criskey on 10/8/2017.
 */
public class RxBasePresenter<V extends IView> extends BasePresenter<V> {

    @VisibleForTesting(otherwise = VisibleForTesting.PROTECTED)
    public Scheduler computationScheduler;
    protected Scheduler uiScheduler;
    protected PresenterState state;

    protected LifecycleCompositeDisposable disposables;

    public RxBasePresenter(Scheduler uiScheduler) {
        super();
        this.uiScheduler = uiScheduler;
        this.computationScheduler = Schedulers.computation();
        this.disposables = new LifecycleCompositeDisposable();
        this.state = new PresenterState();
    }

    @Override
    @CallSuper
    protected void onPreDetachView() {
        disposables.dispose();
    }

    @Override
    @CallSuper
    public void onViewPaused() {
        disposables.dispose(PAUSE);
    }

    protected <T> ObservableTransformer<T, T> scheduleOnUIObservableTransformer() {
        return up -> up.subscribeOn(Schedulers.io()).observeOn(uiScheduler);
    }

    protected <T> FlowableTransformer<T, T> scheduleOnUIFlowableTransformer() {
        return up -> up.subscribeOn(Schedulers.io()).observeOn(uiScheduler);
    }

    protected <T> SingleTransformer<T, T> scheduleOnUISingleTransformer() {
        return up -> up.subscribeOn(Schedulers.io()).observeOn(uiScheduler);
    }

    protected <T> MaybeTransformer<T, T> scheduleOnUIMaybeTransformer() {
        return up -> up.subscribeOn(Schedulers.io()).observeOn(uiScheduler);
    }

}
