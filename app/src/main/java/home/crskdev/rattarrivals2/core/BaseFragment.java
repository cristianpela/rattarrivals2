package home.crskdev.rattarrivals2.core;

import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;
import dagger.android.support.DaggerFragment;
import home.crskdev.rattarrivals2.util.ViewUtils;

/**
 * Created by criskey on 13/8/2017.
 */
public abstract class BaseFragment<V extends IView, P extends IPresenter<V>> extends DaggerFragment
        implements IView {

    public abstract P presenter();

    @LayoutRes
    public abstract int layoutId();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(layoutId(), container, false);
        ButterKnife.bind(this, view);
        attachPresenter(savedInstanceState);
        return view;
    }

    @Override
    public void onDestroyView() {
        detachPresenter();
        super.onDestroyView();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        ViewUtils.toBundle(outState, presenter().getSavedInstance());
    }

    private void attachPresenter(Bundle savedInstanceState) {
        onPreAttachPresenter();
        //noinspection unchecked
        presenter().attachView((V) this, ViewUtils.toMap(savedInstanceState), ViewUtils.toMap(getArguments()));
        onPostAttachPresenter();
    }

    private void detachPresenter() {
        onPreDetachPresenter();
        presenter().detachView();
    }

    @Override
    @CallSuper
    public void onResume() {
        super.onResume();
        presenter().onViewResumed();
    }

    @Override
    @CallSuper
    public void onPause() {
        super.onPause();
        presenter().onViewPaused();
    }


    protected void onPostAttachPresenter() {
    }

    protected void onPreAttachPresenter() {
    }

    protected void onPreDetachPresenter() {
    }

}
