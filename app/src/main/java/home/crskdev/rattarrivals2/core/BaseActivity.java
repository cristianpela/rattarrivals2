package home.crskdev.rattarrivals2.core;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.LayoutRes;

import butterknife.ButterKnife;
import dagger.android.support.DaggerAppCompatActivity;
import home.crskdev.rattarrivals2.util.ViewUtils;

/**
 * Created by criskey on 8/8/2017.
 */
public abstract class BaseActivity<V extends IView, P extends IPresenter<V>> extends
        DaggerAppCompatActivity implements IView {

    public abstract P presenter();

    @LayoutRes
    public abstract int layoutId();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //only portrait
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(layoutId());
        ButterKnife.bind(this);
        onPreAttachPresenter();
        //noinspection unchecked
        presenter().attachView((V) this, ViewUtils.toMap(savedInstanceState), ViewUtils.toMap(getIntent().getExtras()));
        onPostAttachPresenter();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter().detachView();
    }

    @Override
    @CallSuper
    protected void onResume() {
        super.onResume();
        presenter().onViewResumed();
    }

    @Override
    @CallSuper
    protected void onPause() {
        super.onPause();
        presenter().onViewPaused();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        ViewUtils.toBundle(outState, presenter().getSavedInstance());
    }

    protected void onPostAttachPresenter() {
    }

    protected void onPreAttachPresenter() {
    }

}
