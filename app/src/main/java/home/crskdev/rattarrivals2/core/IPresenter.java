package home.crskdev.rattarrivals2.core;

import android.support.annotation.NonNull;

import java.util.Map;

/**
 * Created by criskey on 10/8/2017.
 */

public interface IPresenter<V extends IView> {

    void attachView(@NonNull V view,@NonNull Map<String, ?> savedInstance, @NonNull Map<String, ?> extras);

    void detachView();

    void onViewResumed();

    void onViewPaused();

    Map<String, ?> getSavedInstance();

}
