package home.crskdev.rattarrivals2.core;

import android.support.annotation.CallSuper;
import android.support.annotation.NonNull;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import home.crskdev.rattarrivals2.model.Result;
import io.reactivex.subjects.BehaviorSubject;
import io.reactivex.subjects.Subject;
import timber.log.Timber;

/**
 * .
 * Created by criskey on 10/8/2017.
 */

public class BasePresenter<V extends IView> implements IPresenter<V> {

    protected V view;

    protected Map<String, Object> saveInstance;
    protected Map<String, ?> extras;

    public BasePresenter() {
    }

    @Override
    public void attachView(@NonNull V view, @NonNull Map<String, ?> savedInstance, @NonNull Map<String, ?> extras) {
        this.view = view;
        this.saveInstance = new HashMap<>(savedInstance);
        this.extras = extras;
        onPostAttachView();
    }

    @Override
    @CallSuper
    public void detachView() {
        onPreDetachView();
        view = null;
        onPostDetachView();
    }

    @Override
    public void onViewResumed() {

    }

    @Override
    public void onViewPaused() {

    }

    @Override
    public Map<String, ?> getSavedInstance() {
        return Collections.unmodifiableMap(saveInstance);
    }

    protected void onPostAttachView() {

    }

    protected void onPostDetachView() {

    }

    protected void onPreDetachView() {

    }

    protected void view(@NonNull NotNull<V> n) {
        if (view != null)
            n.execute(view);
    }


    protected interface NotNull<T> {
        void execute(T instance);
    }
}