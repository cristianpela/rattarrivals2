package home.crskdev.rattarrivals2.core;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import home.crskdev.rattarrivals2.model.Result;
import home.crskdev.rattarrivals2.util.FluentMap;
import home.crskdev.rattarrivals2.util.Pair;
import home.crskdev.rattarrivals2.util.asserts.Asserts;
import io.reactivex.Observable;
import io.reactivex.subjects.BehaviorSubject;

import static home.crskdev.rattarrivals2.model.Result.Wait;
import static home.crskdev.rattarrivals2.model.Result.caseOf;
import static home.crskdev.rattarrivals2.util.asserts.Asserts.assertIsOnMainThread;
import static home.crskdev.rattarrivals2.util.asserts.Asserts.basicAssert;

/**
 * Created by criskey on 7/9/2017.
 */
public class PresenterState {

    private BehaviorSubject<Map<Class<? extends Result>, Result>> stateSubject;

    PresenterState() {
        stateSubject = BehaviorSubject.createDefault(new HashMap<>());
    }

    public Observable<Collection<Result>> observable() {
        //the idea behind this is to send downstream a list with most up date results
        //this way we avoid to redraw the same old values on the view
        //--basic a distinct_until_changed applied to the state map--

        //pairing: (f,s) -> (s, current)
        //init : (*,*)
        //0 : (*, *), 0 -> (*, 0)
        //1 : (*, 0), 1 -> (0, 1)
        //2 : (0, 1), 2 -> (1, 2)
//        final Pair<HashMap<Class<? extends Result>, Result>, HashMap<Class<? extends Result>, Result>>
//                initialValue = Pair.of(new HashMap<Class<? extends Result>, Result>(),
//                new HashMap<Class<? extends Result>, Result>());
//        return stateSubject.scan(initialValue,
//                (acc, curr) -> Pair.of(acc.second, new HashMap<>(curr)))
//                .map(p -> mostUpToDateResults(p.first, p.second))
//                .filter(l -> !l.isEmpty())
//                .serialize();

        return stateSubject.map(Map::values).serialize();
    }

    /**
     * filter exclusion of unchanged values. Note: make sure wrapped result values have correctly
     * implemented their equals contract
     *
     * @param prev    state
     * @param current state
     * @return latest list of results
     */
    private Collection<Result> mostUpToDateResults(@NonNull HashMap<Class<? extends Result>, Result> prev,
                                                   HashMap<Class<? extends Result>, Result> current) {
        List<Result> latestValues = new ArrayList<>();
        for (Class<? extends Result> clazz : current.keySet()) {
            Result rAcc = prev.get(clazz);
            Result rCurr = current.get(clazz);
            if (rAcc == null || !rAcc.equals(rCurr)) {
                latestValues.add(rCurr);
            }
        }
        return latestValues;
    }


    public void next(Result result) {
        //we won't keep transient results on the persistence map
        assertIsOnMainThread();
        basicAssert(!caseOf(result, Result.Error.class), "use nextError method for errors");

        Map<Class<? extends Result>, Result> persistenceMap = stateSubject.getValue();
        @SuppressWarnings("unchecked")
        FluentMap<Class<? extends Result>, Result> sendingMap = new FluentMap<>()
                .put(result.getClass(), result);
        if (!caseOf(result, Wait.class)) {
            //sendingMap.put(Wait.class, Wait.NO_WAIT);
        } else if (!(result instanceof Result.TransientResult)) {
            //saving result for future subscriptions
           // persistenceMap.put(result.getClass(), result);
        }
        stateSubject.onNext(sendingMap.get());
    }

    public void nextError(Result.Error error) {
        assertIsOnMainThread();
        @SuppressWarnings("unchecked")
        FluentMap<Class<? extends Result>, Result> map = new FluentMap<>()
                .put(error.getClass(), error);
              //  .put(Wait.class, Wait.NO_WAIT);
        stateSubject.onNext(map.get());
    }

    public void nextError(@NonNull Throwable error) {
        nextError(new Result.Error(error));
    }

}
