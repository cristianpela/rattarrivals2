package home.crskdev.rattarrivals2.di;

import javax.inject.Singleton;

import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;
import home.crskdev.rattarrivals2.RATTArrivals2App;

/**
 * Created by criskey on 8/8/2017.
 */
@Singleton
@Component(modules = {Modules.BindingModule.class,
        AndroidSupportInjectionModule.class})
public interface RATTArrivals2AppComponent extends AndroidInjector<RATTArrivals2App> {
    @Component.Builder
    abstract class Builder extends AndroidInjector.Builder<RATTArrivals2App>{}
}
