package home.crskdev.rattarrivals2.di;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by criskey on 18/8/2017.
 */
@Scope
@Retention(RetentionPolicy.SOURCE)
public @interface PerFragment {
}
