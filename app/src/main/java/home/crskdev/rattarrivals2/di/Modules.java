package home.crskdev.rattarrivals2.di;

import android.arch.persistence.room.Room;
import android.content.Context;
import android.content.SharedPreferences;

import javax.inject.Singleton;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import dagger.android.ContributesAndroidInjector;
import home.crskdev.rattarrivals2.RATTArrivals2App;
import home.crskdev.rattarrivals2.arrivals.presentation.ArrivalsActivity;
import home.crskdev.rattarrivals2.arrivals.services.ArrivalJSONConverter;
import home.crskdev.rattarrivals2.arrivals.services.ArrivalJSONConverterImpl;
import home.crskdev.rattarrivals2.arrivals.services.ArrivalParseServiceImpl;
import home.crskdev.rattarrivals2.arrivals.services.ArrivalParseService;
import home.crskdev.rattarrivals2.init.InitActivity;
import home.crskdev.rattarrivals2.init.InitModule;
import home.crskdev.rattarrivals2.lines.presentation.LineActivity;
import home.crskdev.rattarrivals2.lines.LineModule;
import home.crskdev.rattarrivals2.lines.repository.LineRepository;
import home.crskdev.rattarrivals2.lines.repository.LineRepositoryImpl;
import home.crskdev.rattarrivals2.net.Client;
import home.crskdev.rattarrivals2.net.NetworkClient;
import home.crskdev.rattarrivals2.arrivals.repository.ArrivalRepositoryImpl;
import home.crskdev.rattarrivals2.arrivals.repository.ArrivalRepository;
import home.crskdev.rattarrivals2.repository.CheckService;
import home.crskdev.rattarrivals2.repository.LocalStorage;
import home.crskdev.rattarrivals2.repository.CheckServiceImpl;
import home.crskdev.rattarrivals2.repository.LocalStorageImpl;
import home.crskdev.rattarrivals2.repository.RATTArrivals2Database;
import home.crskdev.rattarrivals2.util.ResourceFinder;
import home.crskdev.rattarrivals2.util.ResourceFinderImpl;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;

/**
 * Created by criskey on 8/8/2017.
 */
public interface Modules {

    @Singleton
    @Module(includes = {AppModule.class, ServicesModule.class, RxModule.class})
    abstract class BindingModule {

        @PerActivity
        @ContributesAndroidInjector(modules = InitModule.class)
        abstract InitActivity bindInitActivity();

        @PerActivity
        @ContributesAndroidInjector(modules = LineModule.class)
        abstract LineActivity bindLineActivity();

        @PerActivity
        @ContributesAndroidInjector
        abstract ArrivalsActivity bindArrivalsActivity();

    }

    @Module
    abstract class AppModule {
        @Provides
        public static Context provideContext(RATTArrivals2App app) {
            return app.getApplicationContext();
        }

        @Provides
        @Singleton
        public static SharedPreferences providePreferences(Context context) {
            return context.getSharedPreferences("RATTArrivals2_Prefs", Context.MODE_PRIVATE);
        }

        @Binds
        public abstract ResourceFinder bindsResourceFinder(ResourceFinderImpl impl);
    }

    @Module
    abstract class RxModule {

        @Singleton
        @Provides
        public static Scheduler provideUiScheduler() {
            return AndroidSchedulers.mainThread();
        }

    }

    @Module
    abstract class ServicesModule {

        @Provides
        @Singleton
        public static LineRepository provideLineRepository(RATTArrivals2Database db) {
            return new LineRepositoryImpl(db);
        }

        @Provides
        public static Client provideClient() {
            return new NetworkClient();
        }

        @Provides
        @Singleton
        public static CheckService provideLineCheckService(LocalStorage localStorage) {
            return new CheckServiceImpl(localStorage);
        }

        @Provides
        @Singleton
        public static ArrivalParseService provideArrivalParseService(Client client) {
            return new ArrivalParseServiceImpl(client);
        }

        @Provides
        @Singleton
        public static ArrivalRepository providePinnedArrivalService2(RATTArrivals2Database db) {
            return new ArrivalRepositoryImpl(db.getPinnedArrivalRepository());
        }

        @Provides
        @Singleton
        public static RATTArrivals2Database provideDB(Context context) {
            return Room.databaseBuilder(context, RATTArrivals2Database.class, "ratt-arrivals2.db")
                   // .addMigrations(RATTArrivals2Database.MIGRATION_1_2)
                    .fallbackToDestructiveMigration()
                    .build();
        }

        @Provides
        @Singleton
        public static LocalStorage provideStorage(SharedPreferences preferences) {
            return new LocalStorageImpl(preferences);
        }

        @Binds
        public abstract ArrivalJSONConverter bindsArrivalJSON(ArrivalJSONConverterImpl impl);

    }
}
