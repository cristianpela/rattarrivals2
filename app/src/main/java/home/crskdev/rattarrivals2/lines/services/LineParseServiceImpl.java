package home.crskdev.rattarrivals2.lines.services;

import org.jsoup.nodes.Document;

import java.util.List;

import home.crskdev.rattarrivals2.lines.model.Line;
import home.crskdev.rattarrivals2.lines.model.LineType;
import home.crskdev.rattarrivals2.net.Client;
import home.crskdev.rattarrivals2.net.Contract;
import io.reactivex.Observable;

/**
 * Created by criskey on 11/8/2017.
 */
public class LineParseServiceImpl implements LineParseService {

    private Client client;

    public LineParseServiceImpl(Client client) {
        this.client = client;
    }

    @Override
    public Observable<List<Line>> parseLine(LineType lineType) {
        return Observable.create(e -> {
            Document document = client.request(Contract.Endpoints.getLineTypeLink(lineType));
            LinesParser parser = new LinesParser(document, lineType);
            e.onNext(parser.getLines());
            e.onComplete();
        });
    }
}