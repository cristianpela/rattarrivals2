package home.crskdev.rattarrivals2.lines.services;

/**
 * Created by criskey on 1/9/2017.
 */
class LineFormatter {

    String format(String lineTitle) {
        String ln = lineTitle.trim();
        String[] splt = ln.split(" ");
        if (splt.length >= 2) {
            if (splt[0].toLowerCase().startsWith("metro")) {
                ln = "M" + splt[1];    //metropolitana - line
            } else if (splt[0].toLowerCase().startsWith("expres")) {
                ln = "E" + splt[1];  // express line
                if(splt.length == 3 && splt[2].toLowerCase().equals("barat")){
                    ln +="b";
                }
            } else if (splt[1].toLowerCase().equals("barat")) {
                ln = splt[0] + "b"; // barat line
            }
        }
        return ln;
    }

}
