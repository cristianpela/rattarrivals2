package home.crskdev.rattarrivals2.lines.presentation;

import java.util.List;

import home.crskdev.rattarrivals2.core.IView;
import home.crskdev.rattarrivals2.lines.model.Line;

/**
 * Created by criskey on 13/8/2017.
 */

public interface ListLinesView extends IView {
    void onDisplayLines(List<Line> lines);

    void onAddFavorite(Line line);

    void onRemoveFavorite(Line line);

    void onUpdateFavorite(Line line);

    void onOpenLineArrivals(long lineId);

}
