package home.crskdev.rattarrivals2.lines.repository;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import home.crskdev.rattarrivals2.lines.model.LineType;

/**
 * Created by criskey on 13/8/2017.
 */
@Entity(tableName = "Line")
public class LineEntity {

    @PrimaryKey
    public final long id;

    public final String title;

    public final LineType type;

    public final boolean favorite;

    public final long modified;

    public LineEntity(long id, String title, LineType type, boolean favorite, long modified) {
        this.id = id;
        this.title = title;
        this.type = type;
        this.favorite = favorite;
        this.modified = modified;
    }

    public LineEntity setFavorite(boolean favorite, long modified){
        return new LineEntity(id, title, type, favorite, modified);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LineEntity that = (LineEntity) o;

        return id == that.id;

    }

    @Override
    public int hashCode() {
        return (int) id;
    }

    @Override
    public String toString() {
        return "LineEntity{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", type=" + type +
                ", favorite=" + favorite +
                ", modified=" + modified +
                '}';
    }
}
