package home.crskdev.rattarrivals2.lines;

import android.support.v4.app.FragmentManager;

import dagger.Module;
import dagger.Provides;
import dagger.android.ContributesAndroidInjector;
import home.crskdev.rattarrivals2.di.PerFragment;
import home.crskdev.rattarrivals2.lines.presentation.LineActivity;
import home.crskdev.rattarrivals2.lines.presentation.ListLinesFragment;

/**
 * Created by criskey on 18/8/2017.
 */
@Module
public abstract class LineModule {

    @Provides
    public static FragmentManager provideFragmentManager(LineActivity activity) {
        return activity.getSupportFragmentManager();
    }

    @PerFragment
    @ContributesAndroidInjector
    abstract ListLinesFragment bindLineLinesFragment();

}
