package home.crskdev.rattarrivals2.lines.presentation;

import com.jakewharton.rxrelay2.PublishRelay;
import com.jakewharton.rxrelay2.Relay;

import org.reactivestreams.Publisher;

import java.util.List;
import java.util.concurrent.TimeUnit;

import home.crskdev.rattarrivals2.util.ImmLists;
import home.crskdev.rattarrivals2.util.time.TimeValue;
import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.FlowableTransformer;
import io.reactivex.Scheduler;
import io.reactivex.functions.Function;
import timber.log.Timber;

import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static java.util.concurrent.TimeUnit.SECONDS;

/**
 * .
 * Created by criskey on 31/10/2017.
 */
public final class StaleDataWatcher<Key> {

    private static final Object TRIGGER_EVENT = new Object();

    private final TimeValue period;

    private final Scheduler scheduler;

    private Relay<Object> triggerRelay;

    public StaleDataWatcher(TimeValue period, Scheduler scheduler) {
        this.period = period;
        this.scheduler = scheduler;
        this.triggerRelay = PublishRelay.create();
    }

    public Flowable<List<StaleData<Key>>> watch(Publisher<List<StaleData<Key>>> intervalCallee) {
        Flowable<List<StaleData<Key>>> counter = Flowable
                .interval(period.value, period.unit, scheduler)
                .startWith(0L)//immediate start no delay
                .flatMap(__ -> Flowable.fromPublisher(intervalCallee))
                .map(data -> ImmLists.map(data, d -> {
                    long now = scheduler.now(d.timeValue.unit);
                    return new StaleData<>(d.key, new TimeValue(now - d.timeValue.value, d.timeValue.unit));
                }));
        return triggerRelay
                .toFlowable(BackpressureStrategy.LATEST)
                .startWith(TRIGGER_EVENT)
                .switchMap(__ -> counter);
    }

    public void restart() {
        Timber.d("Stale Data Watcher: trigger restart event");
        triggerRelay.accept(TRIGGER_EVENT);
    }

    public FlowableTransformer<List<StaleData<Key>>, List<StaleData<Key>>> timeTransformer(TimeUnit newTimeUnit) {
        return upstream -> upstream.map(l -> ImmLists.map(l, sd -> sd.convert(newTimeUnit)));
    }

    public <U> FlowableTransformer<List<U>, List<StaleData<Key>>> mapTransformer(Function<U, StaleData<Key>> mapper) {
        return upstream -> upstream.map(l -> ImmLists.map(l, mapper));
    }

    public static class StaleData<Key> {
        public final Key key;
        public final TimeValue timeValue;

        public StaleData(Key key, TimeValue timeValue) {
            this.key = key;
            this.timeValue = timeValue;
        }

        public static <Key> StaleData asMillisData(Key key, long timeMillis) {
            return new StaleData<>(key, new TimeValue(timeMillis, MILLISECONDS));
        }

        public StaleData<Key> convert(TimeUnit unit) {
            return new StaleData<>(key, timeValue.convert(unit));
        }

        @Override
        public String toString() {
            return "StaleData{" +
                    "key=" + key +
                    ", timeValue=" + timeValue +
                    '}';
        }
    }

}
