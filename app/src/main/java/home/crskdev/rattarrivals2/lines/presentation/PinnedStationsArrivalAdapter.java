package home.crskdev.rattarrivals2.lines.presentation;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.TextViewCompat;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextSwitcher;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Date;

import butterknife.BindView;
import home.crskdev.rattarrivals2.R;
import home.crskdev.rattarrivals2.arrivals.ArrivalsConverter;
import home.crskdev.rattarrivals2.arrivals.model.ArrivalExtraViewModel;
import home.crskdev.rattarrivals2.model.Action;
import home.crskdev.rattarrivals2.util.ButterKnifeViewHolder;
import home.crskdev.rattarrivals2.util.recyclerview.AbstractRecyclerViewAdapter;
import home.crskdev.rattarrivals2.util.recyclerview.AdapterOps;
import home.crskdev.rattarrivals2.util.recyclerview.DefaultViewHolderBinder;
import home.crskdev.rattarrivals2.util.time.TimeConverter;
import home.crskdev.rattarrivals2.util.time.TimeUtils;
import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;
import io.reactivex.subjects.Subject;

/**
 * .
 * Created by criskey on 18/9/2017.
 */
public class PinnedStationsArrivalAdapter extends
        AbstractRecyclerViewAdapter<ArrivalExtraViewModel, PinnedStationsArrivalAdapter.PinnedStationsArrivalVH>
        implements PinnedStationsArrivalOps {

    private PublishSubject<Action> actionSubject;

    public PinnedStationsArrivalAdapter() {
        actionSubject = PublishSubject.create();
        postInit((current, comparingKey) -> current.getIdStr().equals(comparingKey),
                new PinnedStationsArrivalVHB(actionSubject, this));
    }


    @Override
    public Observable<Action> observableAction() {
        return actionSubject.filter(a -> a != null);
    }

    public static class PinnedStationsArrivalVH extends ButterKnifeViewHolder {

        @BindView(R.id.txtLine)
        TextView txtLine;
        @BindView(R.id.txtStationName)
        TextView txtStationName;
        @BindView(R.id.txtArrival)
        TextSwitcher txtTimeSwitcher;
        @BindView(R.id.txtRoute)
        TextView txtTo;

        @BindView(R.id.imageBtnRefresh)
        ImageButton btnRefresh;
        @BindView(R.id.toggleBtnPinned)
        ImageButton btnPinned;

        @BindView(R.id.imageLine)
        ImageView imgLine;

        public PinnedStationsArrivalVH(final View itemView) {
            super(itemView);
            txtTimeSwitcher.setFactory(() -> {
                //android:textAppearance="@style/TextAppearance.AppCompat.Large"
                //android:textColor="@android:color/white"
                Context context = itemView.getContext();
                TextView txt = new TextView(context);
                TextViewCompat.setTextAppearance(txt,
                        android.support.v7.appcompat.R.style.TextAppearance_AppCompat_Large);
                txt.setTextColor(ContextCompat.getColor(context, android.R.color.white));
                return txt;
            });
        }
    }

    private static class PinnedStationsArrivalVHB
            extends DefaultViewHolderBinder<PinnedStationsArrivalVH, ArrivalExtraViewModel> {

        private Subject<Action> action;
        private AdapterOps<ArrivalExtraViewModel> ops;

        @SuppressWarnings("WeakerAccess")
        public PinnedStationsArrivalVHB(Subject<Action> action, AdapterOps<ArrivalExtraViewModel> ops) {
            super(R.layout.pinned_station_arrival_layout);
            this.action = action;
            this.ops = ops;
        }

        @Override
        public void onBind(PinnedStationsArrivalVH holder, ArrivalExtraViewModel pinned) {
            //        textColorAnimator = ValueAnimator.ofInt(txtTime.getCurrentTextColor(), colorHex)
//                .setDuration(500);
//        textColorAnimator.setInterpolator(new LinearOutSlowInInterpolator());
//        textColorAnimator.setEvaluator(new ArgbEvaluator());
//        textColorAnimator.addUpdateListener(valueAnimator -> {
//            txtTime.setTextColor((Integer) valueAnimator.getAnimatedValue());
//        });
//        textColorAnimator.start();

            holder.txtLine.setText(pinned.title);
            holder.txtStationName.setText(pinned.stationName);
            holder.txtStationName.setTextColor(pinned.staleColor);
            holder.txtTimeSwitcher.setText(pinned.timeStr);
            ((TextView) holder.txtTimeSwitcher.getCurrentView()).setTextColor(pinned.estimationTimeColor);
            ((TextView) holder.txtTimeSwitcher.getNextView()).setTextColor(pinned.estimationTimeColor);
            holder.txtTo.setText(holder.itemView.getResources().getString(R.string.arrival_to, pinned.toWay));
            holder.imgLine.setImageResource(pinned.lineImage);

            holder.btnRefresh.setVisibility(pinned.enabled ? View.VISIBLE : View.INVISIBLE);
            holder.btnPinned.setOnClickListener((v) -> {
                int pos = holder.getAdapterPosition();
                ArrivalExtraViewModel vm = ops.getItemAt(pos);
                action.onNext(new Action.Pinned(vm));
            });
            holder.btnRefresh.setOnClickListener((v) -> {
                int pos = holder.getAdapterPosition();
                ArrivalExtraViewModel vm = ops.getItemAt(pos);
                action.onNext(new Action.StationAction(ArrivalsConverter.toVMFromVMExtra(vm)));
            });
            holder.txtStationName.setOnClickListener((v) -> {
                Toast.makeText(holder.itemView.getContext(), pinned.lastUpdatedStr, Toast.LENGTH_SHORT).show();
            });
        }

        @Override
        public Class<PinnedStationsArrivalVH> getViewHolderClass() {
            return PinnedStationsArrivalVH.class;
        }
    }


}
