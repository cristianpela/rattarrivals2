package home.crskdev.rattarrivals2.lines.presentation;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import javax.inject.Inject;

import home.crskdev.rattarrivals2.R;
import home.crskdev.rattarrivals2.di.PerActivity;

/**
 * Created by criskey on 13/8/2017.
 */
@PerActivity
public class LinePagerAdapter extends FragmentPagerAdapter {


    private ListLinesFragment[] fragments;

    private String[] titles;

    @Inject
    public LinePagerAdapter(Context context, FragmentManager fm) {
        super(fm);
        titles = context.getResources().getStringArray(R.array.tab_titles);
        fragments = new ListLinesFragment[]{
                new ListLinesFragment(),
                new ListLinesFragment(),
                new ListLinesFragment(),
                new ListLinesFragment(),
        };
    }

    @Override
    public Fragment getItem(int position) {
        return fragments[position];
    }

    public void changeFilter(int position){
        fragments[position].setFilterType(FilterType.get(position));
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return titles[position];
    }

    @Override
    public int getCount() {
        return titles.length;
    }

}
