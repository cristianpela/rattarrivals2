package home.crskdev.rattarrivals2.lines.presentation;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.ToggleButton;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import home.crskdev.rattarrivals2.R;
import home.crskdev.rattarrivals2.lines.model.Line;
import home.crskdev.rattarrivals2.util.ButterKnifeViewHolder;
import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;

/**
 * Created by criskey on 18/8/2017.
 */

public class ListLinesRecyclerAdapter extends RecyclerView.Adapter<ListLinesRecyclerAdapter.ListLinesVH> {

    private List<Line> lines;

    private PublishSubject<Line> favSubject;

    public ListLinesRecyclerAdapter() {
        lines = new ArrayList<>();
        setHasStableIds(true);
        favSubject = PublishSubject.create();
    }

    public void addLines(List<Line> lines) {
        this.lines.clear();
        this.lines.addAll(lines);
        notifyDataSetChanged();
    }

    @Override
    public ListLinesVH onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_lines_item, parent, false);
        return new ListLinesVH(v);
    }

    @Override
    public void onBindViewHolder(ListLinesVH holder, int position) {
        Line l = lines.get(position);
        holder.txtLine.setText(l.title);
        holder.toggleButtonFav.setChecked(l.favorite);
        holder.toggleButtonFav.setOnClickListener(v -> {
            Line line = lines.get(holder.getAdapterPosition()).toggle();
            favSubject.onNext(line);
        });
    }

    @Override
    public long getItemId(int position) {
        return lines.get(position).id;
    }

    @Override
    public int getItemCount() {
        return lines.size();
    }

    public Observable<Line> toggleFavoriteObservble() {
        return favSubject;
    }

    public void addFavorite(Line line) {
        lines.add(line);
        notifyItemInserted(lines.size() - 1);
    }

    public void removeFavorite(Line line) {
        int position = getPosition(line);
        if (position != -1) {
            lines.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void updateFavorite(Line line) {
        int position = getPosition(line);
        if (position != -1) {
            lines.set(position, line);
            notifyItemChanged(position);
        }
    }

    private int getPosition(Line line) {
        for (int i = 0; i < lines.size(); i++) {
            Line l = lines.get(i);
            if (l.equals(line)) {
                return i;
            }
        }
        return -1;
    }

    public Line getItem(int position) {
        return lines.get(position);
    }


    static class ListLinesVH extends ButterKnifeViewHolder {

        @BindView(R.id.txt_line)
        TextView txtLine;

        @BindView(R.id.toggle_btn_fav)
        ToggleButton toggleButtonFav;

        public ListLinesVH(View itemView) {
            super(itemView);
        }
    }
}
