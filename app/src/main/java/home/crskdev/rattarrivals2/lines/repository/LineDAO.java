package home.crskdev.rattarrivals2.lines.repository;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import home.crskdev.rattarrivals2.lines.model.LineType;
import io.reactivex.Flowable;
import io.reactivex.Single;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

/**
 * Created by criskey on 13/8/2017.
 */
@Dao
public interface LineDAO {

    @Insert(onConflict = REPLACE)
    Long[] create(List<LineEntity> lines);

    @Insert(onConflict = REPLACE)
    Long createOne(LineEntity line);

    @Update
    void updateLine(LineEntity line);

    @Query("SELECT * FROM Line WHERE type = :lineType ORDER BY title ASC")
    Single<List<LineEntity>> getLinesByType(LineType lineType);

    @Query("SELECT * FROM Line WHERE favorite = 1 ORDER BY title ASC")
    Single<List<LineEntity>> getLinesByFavorite();

    @Query("SELECT * FROM Line WHERE modified >= :since ORDER BY modified DESC LIMIT 1")
    Flowable<LineEntity> observeChanges(long since);

    @Query("SELECT * FROM Line WHERE id = :id")
    Single<LineEntity> getLineById(long id);

}
