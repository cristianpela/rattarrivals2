package home.crskdev.rattarrivals2.lines.model;

/**
 * Created by criskey on 10/8/2017.
 */

public enum LineType {

    NONE(""), AUTO("auto"), TROLLEY("trol"), TRAM("tram");

    public final String suffix;

    LineType(String suffix) {
        this.suffix = suffix + ".php";
    }

}
