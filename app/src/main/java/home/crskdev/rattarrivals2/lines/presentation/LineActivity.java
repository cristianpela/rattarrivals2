package home.crskdev.rattarrivals2.lines.presentation;

import android.content.DialogInterface;
import android.graphics.PorterDuff;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PagerSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ProgressBar;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import home.crskdev.rattarrivals2.R;
import home.crskdev.rattarrivals2.arrivals.model.ArrivalExtraViewModel;
import home.crskdev.rattarrivals2.core.BaseActivity;
import home.crskdev.rattarrivals2.model.Action;
import home.crskdev.rattarrivals2.model.BaseViewModel;
import home.crskdev.rattarrivals2.util.ImmLists;
import home.crskdev.rattarrivals2.util.ViewUtils;
import home.crskdev.rattarrivals2.util.asserts.SpacesItemDecoration;
import home.crskdev.rattarrivals2.util.recyclerview.TapableRecyclerView;
import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;

/**
 * .
 * Created by criskey on 13/8/2017.
 */

public class LineActivity extends BaseActivity<LineView, LinePresenter>
        implements LineView {

    @Inject
    LinePresenter presenter;

    @Inject
    LinePagerAdapter linePagerAdapter;

    @BindView(R.id.tabs_line)
    TabLayout tabLayout;

    @BindView(R.id.view_pager_lines)
    ViewPager viewPager;

    @BindView(R.id.recy_pinned_stations_arrival)
    TapableRecyclerView recyclerPinnedArrival;

    @BindView(R.id.prog_wait)
    ProgressBar progWait;

    //ValueAnimator textColorAnimator;

    @Override
    public LinePresenter presenter() {
        return presenter;
    }

    @Override
    public int layoutId() {
        return R.layout.lines_activity_layout;
    }

    @Override
    protected void onPreAttachPresenter() {
        recyclerPinnedArrival.setLayoutManager(new LinearLayoutManager(this,
                LinearLayoutManager.HORIZONTAL, false));
        recyclerPinnedArrival.setAdapter(new PinnedStationsArrivalAdapter());
        DividerItemDecoration decor = new DividerItemDecoration(this, DividerItemDecoration.HORIZONTAL);
        decor.setDrawable(ContextCompat.getDrawable(this, android.R.drawable.divider_horizontal_bright));
        recyclerPinnedArrival.addItemDecoration(decor);
        new PagerSnapHelper().attachToRecyclerView(recyclerPinnedArrival);

        progWait.getIndeterminateDrawable()
                .setColorFilter(ContextCompat
                        .getColor(this, R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
        progWait.getProgressDrawable()
                .setColorFilter(ContextCompat
                        .getColor(this, R.color.colorWarning), PorterDuff.Mode.SRC_ATOP);
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    protected void onPostAttachPresenter() {
        viewPager.setAdapter(linePagerAdapter);
        viewPager.setOffscreenPageLimit(4);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                linePagerAdapter.changeFilter(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                linePagerAdapter.changeFilter(tab.getPosition());
            }
        });
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.getTabAt(0).select();
        bindActions();
    }


    @Override
    public void onLoadPinnedArrivals(List<ArrivalExtraViewModel> pinned) {
        getAdapter().setItems(pinned);
    }

    @Override
    public void addOrUpdatePinnedArrivals(List<ArrivalExtraViewModel> pinned) {
        getAdapter().upsert(ImmLists.toArray(pinned, ArrivalExtraViewModel.class));
    }

    @Override
    public void removePinnedArrival(ArrivalExtraViewModel pinned) {
        getAdapter().remove(pinned);
    }

    @Override
    public void onShowPinned(boolean showPinned) {
        recyclerPinnedArrival.setVisibility((showPinned) ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onWait() {
        progWait.setVisibility(View.VISIBLE);
        getAdapter().changePropertyAll(BaseViewModel.PROPERTY_ENABLED, false);
    }

    @Override
    public void onDone() {
        progWait.setVisibility(View.GONE);
    }

    @Override
    public void onError(Throwable e) {
        onDone();
        getAdapter().changePropertyAll(BaseViewModel.PROPERTY_ENABLED, true);
        String message = e.getClass().getSimpleName() + ":" + e.getMessage();
        ViewUtils.multiLineSnackBar(viewPager,
                message,
                R.string.action_retry,
                (__) -> bindActions(),
                Snackbar.LENGTH_INDEFINITE,
                5).show();
    }

    @Override
    public void onShowQuickSelectPinnedDialog(TapableRecyclerView.TapEvent tapResult) {
        View view = getLayoutInflater()
                .inflate(R.layout.quick_pinned_arrival_recyclerview_layout, null);

        TapableRecyclerView quickRecycler = view.findViewById(R.id.recyQ);
        quickRecycler.setAdapter(new QuickPinnedStationsArrivalAdapter(ImmLists
                .fromIterator(getAdapter().iterator())));
        quickRecycler.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        quickRecycler.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));

        final Disposable quickDisposable = quickRecycler
                .tapObservable().filter(ev -> ev.kind == TapableRecyclerView.TapKind.SINGLE_TAP)
                .subscribe(tapEvent -> {
                    recyclerPinnedArrival.scrollToPosition(tapEvent.position);
                });
        AlertDialog dialog = new AlertDialog.Builder(this)
                .setCancelable(true)
                .setView(view)
                .setOnCancelListener(__ -> quickDisposable.dispose())
                .create();

        dialog.show();
        //window dialog size relative to screen size
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        int w = dm.widthPixels - dm.widthPixels / 4 + 50;
        int h = dm.heightPixels / 2 + 50;
        //noinspection ConstantConditions
        dialog.getWindow().setLayout(w, h);
    }

    @Override
    public void onStaleColor(String id, int colorHex) {
        getAdapter().changeProperty(id, ArrivalExtraViewModel.PROPERTY_STALE_COLOR, colorHex);
    }

    @Override
    public void onCoolDownStart(int maxValue) {
        getAdapter().changePropertyAll(BaseViewModel.PROPERTY_ENABLED, false);
        progWait.setIndeterminate(false);
        progWait.setVisibility(View.VISIBLE);
        progWait.setMax(maxValue);
    }

    @Override
    public void onCoolDown(int value) {
        progWait.setProgress(value);
    }

    @Override
    public void onCoolDownEnd() {
        getAdapter().changePropertyAll(BaseViewModel.PROPERTY_ENABLED, true);
        progWait.setVisibility(View.GONE);
        progWait.setIndeterminate(true);
    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
//        if (textColorAnimator != null && textColorAnimator.isRunning()) {
//            textColorAnimator.cancel();
//        }
        super.onDestroy();
    }

    private void bindActions() {
        presenter.bindActions(Observable.merge(
                getAdapter().observableAction(),
                recyclerPinnedArrival.tapObservable()
                        .filter(ev -> ev.kind == TapableRecyclerView.TapKind.LONG_TAP)
                        .map(Action.TapableAction::new)
        ));
    }

    private PinnedStationsArrivalAdapter getAdapter() {
        return (PinnedStationsArrivalAdapter) recyclerPinnedArrival.getAdapter();
    }

}
