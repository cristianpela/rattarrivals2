package home.crskdev.rattarrivals2.lines.repository;

import java.util.List;

import home.crskdev.rattarrivals2.lines.model.Line;
import home.crskdev.rattarrivals2.lines.model.LineType;
import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Single;

/**
 * Created by criskey on 10/8/2017.
 */

public interface LineRepository {

    Completable create(List<Line> lines);

    Completable update(Line line);

    Single<List<Line>> getLinesByType(LineType lineType);

    Single<List<Line>> getLinesByFavorite();

    Flowable<Line> observeChanges();

    Single<Line> getLinesById(long id);

}
