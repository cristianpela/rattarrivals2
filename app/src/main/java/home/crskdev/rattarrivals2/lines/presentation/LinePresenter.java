package home.crskdev.rattarrivals2.lines.presentation;

import android.support.annotation.VisibleForTesting;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import home.crskdev.rattarrivals2.arrivals.repository.ArrivalRepository;
import home.crskdev.rattarrivals2.arrivals.services.ArrivalParseService;
import home.crskdev.rattarrivals2.core.RxBasePresenter;
import home.crskdev.rattarrivals2.di.PerActivity;
import home.crskdev.rattarrivals2.lines.presentation.StaleDataWatcher.StaleData;
import home.crskdev.rattarrivals2.model.Action;
import home.crskdev.rattarrivals2.model.Result;
import home.crskdev.rattarrivals2.model.Result.PinnedRemoved;
import home.crskdev.rattarrivals2.model.Result.Refresh;
import home.crskdev.rattarrivals2.model.Result.Wait;
import home.crskdev.rattarrivals2.util.ImmLists;
import home.crskdev.rattarrivals2.util.ResourceFinder;
import home.crskdev.rattarrivals2.util.asserts.Asserts;
import home.crskdev.rattarrivals2.util.time.CompositeCountValue;
import home.crskdev.rattarrivals2.util.time.CountValue;
import home.crskdev.rattarrivals2.util.time.CountValueCacheContainer;
import home.crskdev.rattarrivals2.util.time.FeedingIntervalCounter;
import home.crskdev.rattarrivals2.util.time.FeedingIntervalCounterImpl;
import home.crskdev.rattarrivals2.util.time.IntervalCounter;
import home.crskdev.rattarrivals2.util.time.IntervalCounterImpl;
import home.crskdev.rattarrivals2.util.time.OwnedCountValue;
import home.crskdev.rattarrivals2.util.time.ResumableCountValue;
import home.crskdev.rattarrivals2.util.time.TimeUtils;
import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.disposables.Disposable;
import timber.log.Timber;

import static home.crskdev.rattarrivals2.arrivals.ArrivalsConverter.composeWithLineIgnored;
import static home.crskdev.rattarrivals2.arrivals.ArrivalsConverter.toDTOExtraFromVMExtra;
import static home.crskdev.rattarrivals2.arrivals.ArrivalsConverter.toDTOFromVM;
import static home.crskdev.rattarrivals2.arrivals.ArrivalsConverter.toVMExtraFromDTOExtra;
import static home.crskdev.rattarrivals2.model.Result.caseOf;
import static home.crskdev.rattarrivals2.util.LifecycleCompositeDisposable.DisposePoint.PAUSE;
import static home.crskdev.rattarrivals2.util.time.TimeUtils.THROTTLE_TIME_VALUE;

/**
 * .
 * Created by criskey on 13/8/2017.
 */
@PerActivity
public class LinePresenter extends RxBasePresenter<LineView> {

    private static final String KEY_COOLDOWN_COUNT_CACHE = LinePresenter.class + "#cooldown";
    private static final String KEY_STALE_WATCHER_COUNT_CACHE = LinePresenter.class + "#stale";

    private ArrivalRepository arrivalRepository;
    private ArrivalParseService arrivalParseService;

    private ResourceFinder resourceFinder;

    private CountValueCacheContainer countValueContainer;
    private IntervalCounter countdownCooldown;
    private StaleDataWatcher<String> staleDataWatcher;

    @Inject
    public LinePresenter(Scheduler uiScheduler,
                         ArrivalRepository arrivalRepository,
                         ArrivalParseService arrivalParseService,
                         CountValueCacheContainer countValueContainer,
                         ResourceFinder resourceFinder) {
        super(uiScheduler);
        init(arrivalRepository, arrivalParseService, countValueContainer,
                new IntervalCounterImpl(computationScheduler),
                new StaleDataWatcher<>(TimeUtils.STALE_TIME_VALUE, computationScheduler),
                resourceFinder);
    }


    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    LinePresenter(Scheduler uiScheduler,
                  ArrivalRepository arrivalRepository,
                  ArrivalParseService arrivalParseService,
                  CountValueCacheContainer countValueContainer,
                  IntervalCounter countdownCooldown,
                  StaleDataWatcher<String> staleDataWatcher,
                  ResourceFinder resourceFinder) {
        super(uiScheduler);
        init(arrivalRepository, arrivalParseService, countValueContainer,
                countdownCooldown, staleDataWatcher, resourceFinder);
    }

    private void init(ArrivalRepository arrivalRepository, ArrivalParseService arrivalParseService,
                      CountValueCacheContainer countValueContainer, IntervalCounter countdownCooldown,
                      StaleDataWatcher<String> staleDataWatcher, ResourceFinder resourceFinder) {
        this.arrivalRepository = arrivalRepository;
        this.arrivalParseService = arrivalParseService;
        this.countValueContainer = countValueContainer;
        this.resourceFinder = resourceFinder;
        this.countdownCooldown = countdownCooldown;
        this.staleDataWatcher = staleDataWatcher;
    }

    @Override
    protected void onPostAttachView() {

        disposables.add(arrivalRepository.getAllExtra()
                .observeOn(uiScheduler).subscribe(ael -> {
                    Asserts.assertIsOnMainThread();
                    view.onLoadPinnedArrivals(ImmLists
                            .map(ael, ae -> toVMExtraFromDTOExtra(ae, resourceFinder, true)));
                }));

        disposables.add(arrivalRepository.observePin()
                .observeOn(uiScheduler).subscribe(l -> {
                    Asserts.assertIsOnMainThread();
                    view.addOrUpdatePinnedArrivals(
                            ImmLists.map(l, ae -> toVMExtraFromDTOExtra(ae, resourceFinder, true)));
                    staleDataWatcher.restart();
                }));

        disposables.add(arrivalRepository.observeUnpin()
                .observeOn(uiScheduler).subscribe(a -> {
                    Asserts.assertIsOnMainThread();
                    view.removePinnedArrival(toVMExtraFromDTOExtra(a, resourceFinder, true));
                    staleDataWatcher.restart();
                }));

        disposables.add(arrivalRepository.observeShowing()
                .observeOn(uiScheduler).subscribe((showPinned) -> {
                    Asserts.assertIsOnMainThread();
                    view.onShowPinned(showPinned);
                    if (!showPinned) {
                        //when are no pinned arrivals we
                        //dispose any active subscriptions bound to on pause lifecyle
                        //usually these are timers or other streams that
                        //should not run while the view is paused

                        //right now we have IntervalCounter stream bound to pause/pause lifecyle
                        //it stops on pause stores value in CountValueCacheContainer
                        //then starts again on resume with a CountValue previously saved
                        stopTimers();
                    }
                }));

    }

    private void stopTimers() {
        disposables.dispose(PAUSE);
        view.onDone();
    }

    private void showWaitDone(Result result) {
        if (result == Wait.NO_WAIT) {
            view.onDone();
        } else {
            view.onWait();
        }
    }

    private void showError(Result.Error result) {
        if (result != Result.Error.NO_ERROR) {
            view.onError(result.error);
            view.onDone();
        }
    }

    private void startObserveStaleData() {
        //disposing on PAUSE lifecycle - no need to spin counters and get ui notifications in background

        Flowable<List<StaleData<String>>> deferredQuery = Flowable.defer(() -> arrivalRepository
                .getAllExtra()
                .toFlowable()
                .compose(staleDataWatcher.mapTransformer((arr ->
                        StaleData.asMillisData(arr.getIdStr(), arr.lastUpdated))))
        );

        disposables.add(staleDataWatcher
                        .watch(deferredQuery)
                        .compose(staleDataWatcher.timeTransformer(TimeUnit.MINUTES))
                        .observeOn(uiScheduler)
                        .subscribe(staleDataList -> {
                            Timber.d("STREAM STALE UPDATE: %s", staleDataList);
                            for (StaleData<String> staleData : staleDataList) {
                                int minutes = (int)staleData.timeValue.value;
                                String id = staleData.key;
                                switch (minutes) {
                                    case 0:
                                    case 1:
                                        view.onStaleColor(id, resourceFinder.getColorInt("colorStaleNew"));
                                        break; // white - fresh data
                                    case 2:
                                    case 3:
                                    case 4:
                                        view.onStaleColor(id, resourceFinder.getColorInt("colorStaleAboutTo"));
                                        break; // yellow - about to stale data
                                    default:
                                        view.onStaleColor(id, resourceFinder.getColorInt("colorStaleFull"));
                                        // red - stale data need to refresh
                                }
                            }
                        })
                , PAUSE);
    }

    @Override
    public void onViewPaused() {
        super.onViewPaused();
        CountValue cooldownValue = countdownCooldown.getValue();
        if (cooldownValue != null)
            countValueContainer.put(KEY_COOLDOWN_COUNT_CACHE,
                    ResumableCountValue.from(cooldownValue));
    }

    @Override
    public void onViewResumed() {
        ResumableCountValue rCoolDownValue = countValueContainer.get(KEY_COOLDOWN_COUNT_CACHE);
        if (rCoolDownValue != null) {
            CountValue countValue = rCoolDownValue.resumeAt(uiScheduler.now(TimeUnit.MILLISECONDS));
            if (!countValue.hasReachedEnd()) {
                startCooldown(countValue);
            }else{
                view.onCoolDownEnd();
            }
        }
        startObserveStaleData();
    }

    private void startCooldown(CountValue countValue) {
        //todo disabled cooldown
        view.onCoolDownEnd();
//        disposables.add(countdownCooldown.startCount(countValue)
//                .observeOn(uiScheduler)
//                .doOnTerminate(() -> view.onCoolDownEnd())
//                .subscribe(c -> {
//                    if (c.isFlag()) {
//                        view.onCoolDownStart((int) c.max);
//                    } else {
//                        view.onCoolDown((int) c.getValue());
//                    }
//                }), PAUSE);
    }

    void bindActions(Observable<Action> actions) {
        Disposable disposable = actions
                .publish(obs -> {
                    Observable<Result> station = obs.ofType(Action.StationAction.class)
                            .throttleFirst(THROTTLE_TIME_VALUE.value, THROTTLE_TIME_VALUE.unit, computationScheduler)
                            .flatMap((action) -> Wait.WAIT.observable().concatWith(arrivalParseService
                                    .fetchStation(toDTOFromVM(action.viewModel))
                                    .flatMapSingle(r -> arrivalRepository
                                            .refresh(composeWithLineIgnored(r.data))
                                            .map(__ -> Refresh.REFRESH))));

                    Observable<Result> pinnedRemoval = obs.ofType(Action.Pinned.class)
                            .flatMapSingle(pa -> arrivalRepository
                                    .unpin(toDTOExtraFromVMExtra(pa.viewModel))
                                    .map(__ -> PinnedRemoved.PINNED_REMOVED));

                    Observable<Result> tapResult = obs.ofType(Action.TapableAction.class)
                            .map(action -> new Result.TapResult(action.tapEvent));


                    return Observable.merge(Arrays.asList(station, pinnedRemoval, tapResult));
                })
                .onErrorReturn(Result.Error::new)
                .observeOn(uiScheduler)
                .subscribe(result -> {
                    if (caseOf(result, Wait.class)) {
                        showWaitDone(result);
                    } else if (caseOf(result, Result.Error.class)) {
                        showError((Result.Error) result);
                    } else if (caseOf(result, Refresh.class)) {
                        startCooldown(CountValue.startAtMax(0,
                                TimeUtils.COOLDOWN_TIME_VALUE.value,
                                TimeUtils.COOLDOWN_TIME_VALUE.unit,
                                CountValue.BACKWARD));
                    }else if(caseOf(result, Result.TapResult.class)){
                        view.onShowQuickSelectPinnedDialog(((Result.TapResult)result).data);
                    }
                });
        disposables.add(disposable);
    }

    @Override
    protected void onPostDetachView() {
        ResumableCountValue remove = countValueContainer.remove(KEY_STALE_WATCHER_COUNT_CACHE);
        if (remove != null)
            Timber.d("DESTROY REMOVING KEY %s", remove.changeTimeUnit(TimeUnit.MINUTES));

    }
}
