package home.crskdev.rattarrivals2.lines.presentation;

/**
 * .
 * Created by criskey on 18/10/2017.
 */
public interface CoolDownAware {
    void onCoolDownStart(int max);

    void onCoolDown(int value);

    void onCoolDownEnd();
}
