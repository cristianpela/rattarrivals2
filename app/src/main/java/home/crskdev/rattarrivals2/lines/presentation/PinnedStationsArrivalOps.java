package home.crskdev.rattarrivals2.lines.presentation;

import home.crskdev.rattarrivals2.arrivals.model.ArrivalExtraViewModel;
import home.crskdev.rattarrivals2.model.Action;
import home.crskdev.rattarrivals2.util.recyclerview.AdapterOps;
import io.reactivex.Observable;

/**
 *
 * Created by criskey on 2/10/2017.
 */

public interface PinnedStationsArrivalOps extends AdapterOps<ArrivalExtraViewModel> {

    Observable<Action> observableAction();

}
