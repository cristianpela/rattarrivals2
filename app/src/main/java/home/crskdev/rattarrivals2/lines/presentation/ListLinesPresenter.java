package home.crskdev.rattarrivals2.lines.presentation;

import java.util.List;

import javax.inject.Inject;

import home.crskdev.rattarrivals2.core.RxBasePresenter;
import home.crskdev.rattarrivals2.lines.model.Line;
import home.crskdev.rattarrivals2.lines.model.LineType;
import home.crskdev.rattarrivals2.lines.repository.LineRepository;
import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.Single;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by criskey on 13/8/2017.
 */
public class ListLinesPresenter extends RxBasePresenter<ListLinesView> {

    public static final String FILTER_TYPE = "FILTER_TYPE";

    private final LineRepository lineRepository;

    private FilterType filterType;

    @Inject
    public ListLinesPresenter(Scheduler uiScheduler, LineRepository lineRepository) {
        super(uiScheduler);
        this.lineRepository = lineRepository;
    }

    @Override
    protected void onPostAttachView() {
        if (filterType == null) {
            Integer ordinal = (Integer) saveInstance.get("FILTER_TYPE");
            //getOrDefault is not implemented in java 7
            filterType = FilterType.get(ordinal != null ? ordinal : 0);
            fetchLines(filterType);
        }
        disposables.add(lineRepository.observeChanges()
                .subscribeOn(Schedulers.io())
                .observeOn(uiScheduler)
                .subscribe(lineConsumer()));
    }

    private Consumer<Line> lineConsumer() {
        return line -> {
            if (filterType == FilterType.FAV) {
                if (line.favorite) {
                    view.onAddFavorite(line);
                } else {
                    view.onRemoveFavorite(line);
                }
            } else {
                view.onUpdateFavorite(line);
            }
        };
    }

    public void fetchLines(FilterType filterType) {
        Single<List<Line>> linesSingle;
        switch (filterType) {
            case AUTO:
                linesSingle = lineRepository.getLinesByType(LineType.AUTO);
                break;
            case TRAM:
                linesSingle = lineRepository.getLinesByType(LineType.TRAM);
                break;
            case TROLLEY:
                linesSingle = lineRepository.getLinesByType(LineType.TROLLEY);
                break;
            default:
                linesSingle = lineRepository.getLinesByFavorite();
                break;
        }
        disposables.add(linesSingle.subscribeOn(Schedulers.io())
                .observeOn(uiScheduler)
                .subscribe((lines) -> {
                    this.filterType = filterType;
                    saveInstance.put(FILTER_TYPE, filterType.ordinal());
                    view.onDisplayLines(lines);
                }));
    }


    public void showArrivals(Observable<Line> tappedLineObservable) {
        disposables.add(tappedLineObservable
                .subscribe(line -> view.onOpenLineArrivals(line.id)));

    }

    public void toggleFavorite(Observable<Line> lineObservable) {
        disposables.add(lineObservable
                .switchMap(line -> lineRepository.update(line)
                        .subscribeOn(Schedulers.io())
                        .andThen(Observable.just(line)))
                .observeOn(uiScheduler)
                .subscribe(lineConsumer()));
    }
}
