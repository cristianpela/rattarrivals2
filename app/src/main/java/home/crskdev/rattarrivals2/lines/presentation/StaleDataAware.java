package home.crskdev.rattarrivals2.lines.presentation;

/**
 * .
 * Created by criskey on 26/10/2017.
 */
public interface StaleDataAware {
    void onStaleColor(String id, int colorHex);
}
