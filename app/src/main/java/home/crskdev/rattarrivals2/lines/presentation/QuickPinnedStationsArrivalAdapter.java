package home.crskdev.rattarrivals2.lines.presentation;

import android.view.View;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import home.crskdev.rattarrivals2.R;
import home.crskdev.rattarrivals2.arrivals.model.ArrivalExtraViewModel;
import home.crskdev.rattarrivals2.util.ButterKnifeViewHolder;
import home.crskdev.rattarrivals2.util.recyclerview.AbstractRecyclerViewAdapter;
import home.crskdev.rattarrivals2.util.recyclerview.DefaultViewHolderBinder;

/**
 * .
 * Created by criskey on 3/11/2017.
 */

public class QuickPinnedStationsArrivalAdapter extends
        AbstractRecyclerViewAdapter<ArrivalExtraViewModel, QuickPinnedStationsArrivalAdapter.QuickPinnedStationsArrivalVH> {


    protected QuickPinnedStationsArrivalAdapter(List<ArrivalExtraViewModel> items) {
        super(new QuickPinnedStationsArrivalVHB());
        setItems(items);
    }

    public static class QuickPinnedStationsArrivalVH extends ButterKnifeViewHolder {

        @BindView(R.id.txtQLineTitle)
        TextView txtQLineTitle;

        @BindView(R.id.txtQStationName)
        TextView txtQStationName;

        @BindView(R.id.txtQSpre)
        TextView txtQSpre;

        @BindView(R.id.txtQTime)
        TextView txtQTime;

        @BindView(R.id.txtQLastUpdated)
        TextView txtQLastUpdated;

        public QuickPinnedStationsArrivalVH(View itemView) {
            super(itemView);
        }
    }

    /*
       view holder binder
     */
    private static class QuickPinnedStationsArrivalVHB
            extends DefaultViewHolderBinder<QuickPinnedStationsArrivalVH, ArrivalExtraViewModel> {


        public QuickPinnedStationsArrivalVHB() {
            super(R.layout.quick_pinned_station_arrival_layout);
        }


        //todo add inverse colors
        @Override
        public void onBind(QuickPinnedStationsArrivalVH viewHolder, ArrivalExtraViewModel item) {
            viewHolder.txtQLineTitle.setText(item.title);
            viewHolder.txtQStationName.setText(item.stationName);
            viewHolder.txtQSpre.setText(item.toWay);
            viewHolder.txtQTime.setText(item.timeStr);
           // viewHolder.txtQTime.setTextColor(item.estimationTimeColor);
            viewHolder.txtQLastUpdated.setText(item.lastUpdatedStr);
            viewHolder.txtQLastUpdated.setTextColor(item.staleColor);
        }

        @Override
        public Class<QuickPinnedStationsArrivalVH> getViewHolderClass() {
            return QuickPinnedStationsArrivalVH.class;
        }
    }
}
