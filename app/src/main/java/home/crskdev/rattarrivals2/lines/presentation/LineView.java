package home.crskdev.rattarrivals2.lines.presentation;


import java.util.List;

import home.crskdev.rattarrivals2.arrivals.model.ArrivalExtraViewModel;
import home.crskdev.rattarrivals2.core.IView;
import home.crskdev.rattarrivals2.model.Result;
import home.crskdev.rattarrivals2.util.recyclerview.TapableRecyclerView;

/**
 * .
 * Created by criskey on 13/8/2017.
 */

public interface LineView extends IView, CoolDownAware, StaleDataAware {

    void onLoadPinnedArrivals(List<ArrivalExtraViewModel> pinned);

    void addOrUpdatePinnedArrivals(List<ArrivalExtraViewModel> pinned);

    void removePinnedArrival(ArrivalExtraViewModel pinned);

    void onShowPinned(boolean showPinned);

    void onWait();

    void onDone();

    void onError(Throwable e);

    void onShowQuickSelectPinnedDialog(TapableRecyclerView.TapEvent tapResult);
}
