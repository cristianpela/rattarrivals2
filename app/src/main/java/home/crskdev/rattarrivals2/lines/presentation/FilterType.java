package home.crskdev.rattarrivals2.lines.presentation;

import dagger.MapKey;

/**
 * Created by criskey on 18/8/2017.
 */
public enum FilterType {
    FAV, AUTO, TRAM, TROLLEY;


    static FilterType get(int ordinal) {
        return FilterType.values()[ordinal];
    }

    @MapKey
    @interface FilterKey {
        FilterType value();
    }
}
