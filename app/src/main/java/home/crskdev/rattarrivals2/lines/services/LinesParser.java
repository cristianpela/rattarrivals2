package home.crskdev.rattarrivals2.lines.services;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;

import home.crskdev.rattarrivals2.lines.model.Line;
import home.crskdev.rattarrivals2.lines.model.LineType;

/**
 * Created by criskey on 11/8/2017.
 */
public class LinesParser {

    private Document document;
    private LineType lineType;
    private LineFormatter formatter;

    public LinesParser(Document document, LineType lineType) {
        this.document = document;
        this.lineType = lineType;
        this.formatter = new LineFormatter();
    }

    public List<Line> getLines() {
        List<Line> lines = new ArrayList<>();
        Elements links = getLinks();
        for (Element link : links) {
            Line line = getLineFromLink(link);
            if (line != null)
                lines.add(line);
        }
        if(lineType == LineType.AUTO){
            lines.add(new Line(1547, "E8", LineType.AUTO, false));
        }
        //TODO fix this hard
        return lines;
    }

    //TODO parsing is not working for buses E8
    private Line getLineFromLink(Element link) {
        //<a href="sens0.php?param1=1106" target="dreapta1" title="Linia 1" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image9','','tram1-1.png',1)">
        String[] idSplit = link.attr("onclick").split("parent.dreapta1.location.href=");
        if (idSplit.length == 2) {
            final String trail = idSplit[1]
                    .replaceAll("'", "")
                    .replace(";", "");
            if (trail.contains("param1")) {
                int id = Integer.parseInt(trail.split("=")[1]);
                String title = link.attr("title");
                return title.equals("") ? null : new Line(id,
                        formatter.format(title.replace("Linia", "").trim()), lineType, false);
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    private Elements getLinks() {
        return document.select("a[href]");
    }
}
