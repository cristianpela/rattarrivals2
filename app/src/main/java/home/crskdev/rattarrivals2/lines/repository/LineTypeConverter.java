package home.crskdev.rattarrivals2.lines.repository;

import android.arch.persistence.room.TypeConverter;

import home.crskdev.rattarrivals2.lines.model.LineType;

/**
 * Created by criskey on 13/8/2017.
 */
public class LineTypeConverter {

    @TypeConverter
    public static LineType toLineType(String lineType){
        return LineType.valueOf(lineType);
    }
    @TypeConverter
    public static String toDBLineType(LineType lineType){
        return lineType.toString();
    }

}
