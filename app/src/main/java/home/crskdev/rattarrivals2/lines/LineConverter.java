package home.crskdev.rattarrivals2.lines;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import home.crskdev.rattarrivals2.lines.model.Line;
import home.crskdev.rattarrivals2.lines.repository.LineEntity;

/**
 * Created by criskey on 13/8/2017.
 */
public final class LineConverter {

    public static Line toDTO(@NonNull LineEntity entity) {
        return new Line(entity.id, entity.title, entity.type, entity.favorite);
    }

    public static LineEntity toEntity(@NonNull Line line, long modificationDate) {
        return new LineEntity(line.id, line.title, line.type, line.favorite, modificationDate);
    }

    public static List<Line> toDTOs(List<LineEntity> entities) {
        List<Line> list = new ArrayList<>(entities.size());
        for (LineEntity entity : entities) {
            list.add(toDTO(entity));
        }
        return Collections.unmodifiableList(list);
    }

    public static List<LineEntity> toEntities(List<Line> lines, long modificationDate) {
        List<LineEntity> list = new ArrayList<>(lines.size());
        for (Line line : lines) {
            list.add(toEntity(line, modificationDate));
        }
        return Collections.unmodifiableList(list);
    }
}
