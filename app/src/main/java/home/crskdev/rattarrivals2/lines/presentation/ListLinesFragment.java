package home.crskdev.rattarrivals2.lines.presentation;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import home.crskdev.rattarrivals2.R;
import home.crskdev.rattarrivals2.arrivals.presentation.ArrivalsActivity;
import home.crskdev.rattarrivals2.core.BaseFragment;
import home.crskdev.rattarrivals2.lines.model.Line;
import home.crskdev.rattarrivals2.util.recyclerview.TapableRecyclerView;
import home.crskdev.rattarrivals2.util.asserts.SpacesItemDecoration;
import io.reactivex.Observable;

/**
 * Created by criskey on 13/8/2017.
 */
public class ListLinesFragment extends BaseFragment<ListLinesView, ListLinesPresenter>
        implements ListLinesView {


    @BindView(R.id.recycler_list_lines)
    TapableRecyclerView recyclerView;

    @Inject
    ListLinesPresenter presenter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public ListLinesPresenter presenter() {
        return presenter;
    }

    @Override
    public int layoutId() {
        return R.layout.list_lines_fragment_layout;
    }

    @Override
    protected void onPreAttachPresenter() {
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 3));
//        if (Build.VERSION.SDK_INT >= 21) {
//            recyclerView.addItemDecoration(new SpacesItemDecoration(4));
//        }
        recyclerView.setAdapter(new ListLinesRecyclerAdapter());
    }

    @Override
    protected void onPostAttachPresenter() {
        Observable<Line> tappedLineObservable = recyclerView.tapObservable()
                .filter(tap -> tap.kind == TapableRecyclerView.TapKind.SINGLE_TAP)
                .map(e -> getAdapter().getItem(e.position));
        presenter.showArrivals(tappedLineObservable);
        presenter.toggleFavorite(getAdapter().toggleFavoriteObservble());
    }

    @Override
    public void onDisplayLines(List<Line> lines) {
        getAdapter().addLines(lines);
    }

    private ListLinesRecyclerAdapter getAdapter() {
        return (ListLinesRecyclerAdapter) recyclerView.getAdapter();
    }

    @Override
    public void onAddFavorite(Line line) {
        getAdapter().addFavorite(line);
    }

    @Override
    public void onRemoveFavorite(Line line) {
        getAdapter().removeFavorite(line);
    }

    @Override
    public void onUpdateFavorite(Line line) {
        getAdapter().updateFavorite(line);
    }

    @Override
    public void onOpenLineArrivals(long lineId) {
        Context ctx = getActivity().getApplicationContext();
        getActivity().startActivity(ArrivalsActivity.newIntent(ctx, lineId));
    }

    public void setFilterType(FilterType filterType) {
        if (presenter != null) {
            presenter.fetchLines(filterType);
        }
    }
}
