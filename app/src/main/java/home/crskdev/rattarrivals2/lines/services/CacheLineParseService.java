package home.crskdev.rattarrivals2.lines.services;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.inject.Inject;
import javax.inject.Named;

import home.crskdev.rattarrivals2.di.PerActivity;
import home.crskdev.rattarrivals2.lines.model.Line;
import home.crskdev.rattarrivals2.lines.model.LineType;
import io.reactivex.Observable;

/**
 * Created by criskey on 11/8/2017.
 */
@PerActivity
public class CacheLineParseService implements LineParseService {

    private Map<LineType, List<Line>> cache;
    private LineParseService source;

    @Inject
    public CacheLineParseService(@Named("LPS-SRC") LineParseService source) {
        this.source = source;
        cache = new ConcurrentHashMap<>();
    }

    @Override
    public Observable<List<Line>> parseLine(LineType lineType) {
        return Observable.defer(() -> {
            List<Line> lines = cache.get(lineType);
            synchronized (this) {
                if (lines != null) {
                    return Observable.just(lines);
                }
            }
            return source.parseLine(lineType)
                    .doOnNext(l -> cache.put(lineType, l))
                    .doOnDispose(() -> {
                        synchronized (this) {
                            //we dont take in consideration NONE
                            //if we have all line types minus NONE
                            //then we clear the cache
                            if (cache.keySet().size() == LineType.values().length - 1) {
                                cache.clear();
                            }
                        }
                    });
        });
    }

    public boolean isCacheEmpty(){
        return cache.isEmpty();
    }
}
