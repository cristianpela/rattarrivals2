package home.crskdev.rattarrivals2.lines.model;

/**
 * Created by criskey on 10/8/2017.
 */
public final class Line  {

    public static final Line NO_LINE = new Line(-1, "", LineType.NONE, false);

    public final long id;

    public final String title;

    public final LineType type;

    public final boolean favorite;

    public Line(long id, String title, LineType type, boolean favorite) {
        this.id = id;
        this.title = title;
        this.type = type;
        this.favorite = favorite;
    }

    public Line setFavorite(boolean favorite) {
        return new Line(id, title, type, favorite);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Line line = (Line) o;

        return id == line.id;

    }

    @Override
    public int hashCode() {
        return (int) id;
    }

    @Override
    public String toString() {
        return "Line{" +
                "id='" + id + '\'' +
                ", title='" + title + '\'' +
                ", type=" + type +
                ", favorite=" + favorite +
                '}';
    }

    public Line toggle() {
        return new Line(id, title, type, !favorite);
    }

}