package home.crskdev.rattarrivals2.lines.services;

import java.util.List;

import home.crskdev.rattarrivals2.lines.model.Line;
import home.crskdev.rattarrivals2.lines.model.LineType;
import io.reactivex.Observable;

/**
 * Created by criskey on 10/8/2017.
 */
public interface LineParseService {

    Observable<List<Line>> parseLine(LineType lineType);

}
