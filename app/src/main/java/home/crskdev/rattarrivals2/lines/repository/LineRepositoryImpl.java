package home.crskdev.rattarrivals2.lines.repository;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import home.crskdev.rattarrivals2.lines.model.Line;
import home.crskdev.rattarrivals2.lines.model.LineType;
import home.crskdev.rattarrivals2.lines.LineConverter;
import home.crskdev.rattarrivals2.repository.RATTArrivals2Database;
import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;

import static home.crskdev.rattarrivals2.lines.LineConverter.toEntities;
import static home.crskdev.rattarrivals2.lines.LineConverter.toEntity;

/**
 * Created by criskey on 13/8/2017.
 */
@Singleton
public class LineRepositoryImpl implements LineRepository {

    /**
     * starting startWatchingTime to watch modification from
     */
    private final long startWatchingTime;
    private LineDAO dao;

    @Inject
    public LineRepositoryImpl(RATTArrivals2Database db) {
        this(db, System.currentTimeMillis());
    }

    public LineRepositoryImpl(RATTArrivals2Database db, long timeMillis) {
        dao = db.getLineRepository();
        startWatchingTime = timeMillis;
    }

    private long now() {
        long now = System.currentTimeMillis();
        if (now < startWatchingTime) {
            throw new IllegalStateException("No startWatchingTime travel allowed:  now = " + now +
                    " is lesser then beginning startWatchingTime " + startWatchingTime);
        }
        return now;
    }

    @Override
    public Completable create(List<Line> lines) {
        return Completable.create(c -> {
            dao.create(toEntities(lines, now()));
            c.onComplete();
        }).subscribeOn(Schedulers.io());
    }

    @Override
    public Completable update(Line line) {
        return Completable.create(c -> {
            dao.updateLine(toEntity(line, now()));
            c.onComplete();
        }).subscribeOn(Schedulers.io());
    }

    @Override
    public Single<List<Line>> getLinesByType(LineType lineType) {
        return dao.getLinesByType(lineType).map(LineConverter::toDTOs).subscribeOn(Schedulers.io());
    }

    @Override
    public Single<List<Line>> getLinesByFavorite() {
        return dao.getLinesByFavorite().map(LineConverter::toDTOs).subscribeOn(Schedulers.io());
    }

    @Override
    public Single<Line> getLinesById(long id) {
        return dao.getLineById(id).map(LineConverter::toDTO).subscribeOn(Schedulers.io());
    }

    @Override
    public Flowable<Line> observeChanges() {
        return dao.observeChanges(startWatchingTime).map(LineConverter::toDTO).subscribeOn(Schedulers.io());
    }
}
