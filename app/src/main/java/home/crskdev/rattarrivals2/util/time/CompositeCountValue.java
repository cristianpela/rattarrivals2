package home.crskdev.rattarrivals2.util.time;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import home.crskdev.rattarrivals2.util.ImmLists;
import home.crskdev.rattarrivals2.util.asserts.Asserts;

/**
 * CountValue composition of other homogeneus count values.
 * This means they have the same {@link CountValue#min}, {@link CountValue#max}
 * The {@link CountValue#unit} and {@link CountValue#direction} is
 * dealt internally to a common ones for each, if needed.<br>
 * Created by criskey on 21/10/2017.
 */
public final class CompositeCountValue extends CountValue {

    private final String id = UUID.randomUUID().toString();

    private List<CountValue> countValues;

    //don't use this constructor internally, create objects via createHomogeneus method!
    private CompositeCountValue(long value, long min, long max, TimeUnit unit, @Dir int direction,
                                Collection<CountValue> values) {
        super(value, min, max, unit, direction);
        this.countValues = new ArrayList<>(values);
    }

    public static CompositeCountValue createHomogeneus(long min, long max, TimeUnit unit,
                                                       @Dir int direction,
                                                       Collection<CountValue> values) {

        //non CompositeCountValue values
        List<CountValue> filteredValues = ImmLists.Chain.from(values)
                .filter(v -> !v.getClass().equals(CompositeCountValue.class))
                .get();

        List<CountValue> homogeneusList = homogeneusList(min, max, unit, direction, filteredValues);
        long value = (homogeneusList.isEmpty())
                ? (direction == FORWARD) ? max : min
                : homogeneusList.get(0).getValue();
        CompositeCountValue compositeCountValue = new CompositeCountValue(value, min, max, unit, direction, homogeneusList);
        //flattening : we get the possible compositeCountValues from "values" param,
        //and we use compositeCountValue to compose with them through accumation(basically a reducer)
        //this way we assure that CompositeCountValue doesn't have other compositeCountValue in countValues
        //and compositeCountValue is within the rules of adding and overriding
        return ImmLists.Chain.from(values)
                .filter(v -> v instanceof CompositeCountValue)
                .cast(CompositeCountValue.class)
                .reduce(compositeCountValue, (acc, curr, index) ->
                        //we skip composing if min and max don't match the accumulation
                        curr.min != acc.min || curr.max != acc.max
                                ? acc
                                : acc.composeWith(curr));
    }

    private static List<CountValue> homogeneusList(long min, long max, TimeUnit unit,
                                                   @Dir int direction,
                                                   Collection<CountValue> countValues) {

        return ImmLists.Chain.from(countValues)
                // skip flags
                .filter(cv -> !cv.isFlag())
                //making direction and unit the same
                .map(cv -> {
                    cv = cv.changeDirection(direction).changeTimeUnit(unit);
                    Asserts.basicAssert(max == cv.max && min == cv.min,
                            "Count Value not homogeneus. Expected min: " + min + " actual " + cv.min +
                                    " and max: " + max + " actual " + cv.max);
                    return cv;
                })
                //sort by value based on direction: FORWARD -> ASC ; BACKWARD -> DESC
                .sort((cv1, cv2) -> direction * Long.compare(cv1.getValue(), cv2.getValue()))
                .get();
    }

    @Override
    public CompositeCountValue advance(long newValue, @Dir int direction) {
        //advance and then filter count values that reached end
        return CompositeCountValue.createHomogeneus(min, max, unit, direction,
                ImmLists.Chain.from(countValues)
                        .map(c -> c.advance(newValue, direction))
                        //.filter(c -> !c.hasReachedEnd())//todo we should spit the reached value just once
                        .get());
    }

    @Override
    public CompositeCountValue advance(long newValue) {
        return advance(newValue, direction);
    }

    @Override
    public CompositeCountValue changeTimeUnit(TimeUnit toUnit) {
        long cMin = TimeUtils.safeConvert(min, unit, toUnit);
        long cMax = TimeUtils.safeConvert(max, unit, toUnit);
        return CompositeCountValue.createHomogeneus(
                cMin,
                cMax,
                toUnit,
                direction,
                countValues);
    }

    @Override
    public CompositeCountValue changeDirection(@Dir int newDirection) {
        return CompositeCountValue.createHomogeneus(
                min,
                max,
                unit,
                newDirection,
                countValues);
    }

    public List<CountValue> getCountValues() {
        return Collections.unmodifiableList(new ArrayList<>(countValues));
    }

    public CompositeCountValue removeCountValue(@NonNull CountValue value) {
        boolean removed = countValues.remove(value.changeTimeUnit(unit).changeDirection(direction));
        return !removed ? this : CompositeCountValue.createHomogeneus(min, max, unit, direction, countValues);
    }

    public CompositeCountValue addCountValues(Collection<CountValue> values) {
        for (CountValue cv : values) {
            cv = cv.changeTimeUnit(unit);
            //overriding should occur only if value is lesser than the old one
            //or grater then older one based on direction
            int index = countValues.indexOf(cv);
            if (index != -1) {
                CountValue existing = countValues.get(index);
                if (direction == FORWARD && cv.getValue() < existing.getValue() ||
                        direction == BACKWARD && cv.getValue() > existing.getValue()) {
                    countValues.set(index, cv);
                }
            } else {
                countValues.add(cv);
            }
        }
        return CompositeCountValue.createHomogeneus(min, max, unit, direction, countValues);
    }

    public CompositeCountValue composeWith(CompositeCountValue other) {
        return addCountValues(other.getCountValues());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CompositeCountValue that = (CompositeCountValue) o;
        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }


    @Override
    public String toString() {
        return "CompositeCountValue{" + countValues + '}';
    }

    private String toStringCountValues() {
        StringBuilder b = new StringBuilder();
        for (CountValue countValue : countValues) {
            b.append(countValue.toString()).append("\n");
        }
        return b.toString();
    }
}
