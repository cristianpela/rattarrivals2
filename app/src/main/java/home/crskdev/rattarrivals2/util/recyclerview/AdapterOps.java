package home.crskdev.rattarrivals2.util.recyclerview;

import android.annotation.TargetApi;
import android.support.annotation.IntRange;

import java.util.List;
import java.util.stream.Stream;

/**
 * Created by criskey on 2/10/2017.
 */
public interface AdapterOps<T> extends Iterable<T> {

    int NO_INDEX = -1;

    void setItems(List<T> items);

    int getItemCount();

    T getItemAt(@IntRange(from = 0) int position);

    /**
     * change property of the underneath items
     *
     * @param items updated item
     * @return true if at least one item was updated
     */
    @SuppressWarnings("unchecked")
    boolean upsert(T... items);

    int remove(T item);

    int changeProperty(Object lookupKey, String propertyName, Object newValue);

    void changePropertyAll(String propertyName, Object newValue);

    ItemLookupStrategy<T> itemLookupStrategy();

    @TargetApi(24)
    Stream<T> itemStream();

}
