package home.crskdev.rattarrivals2.util.time;

import android.support.annotation.ColorInt;
import android.support.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.concurrent.TimeUnit;

import home.crskdev.rattarrivals2.util.ResourceFinder;

import static home.crskdev.rattarrivals2.util.time.TimeEstimated.Estimation.ABOUT_TO;
import static home.crskdev.rattarrivals2.util.time.TimeEstimated.Estimation.NOW;
import static home.crskdev.rattarrivals2.util.time.TimeEstimated.Estimation.NO_ESTIMATION;

/**
 *
 * Created by criskey on 9/10/2017.
 */
public final class TimeEstimated {

    public static final TimeEstimated NO_TIME = TimeEstimated.nonEstimated(0L);

    public static final String RES_COLOR_ESTIMATION_NO = "colorEstimationNo";
    public static final String RES_COLOR_ESTIMATION_NOW = "colorEstimationNow";
    public static final String RES_COLOR_ESTIMATION_ABOUT_TO = "colorEstimationAboutTo";

    private static final String INVERSE_SUFFIX = "_Inverse";

    @Estimation
    public final int estimation;

    public final long time;

    public TimeEstimated(long time, @Estimation int estimation) {
        this.time = time;
        this.estimation = estimation;
    }

    public static TimeEstimated nonEstimated(long time) {
        return new TimeEstimated(time, NO_ESTIMATION);
    }


    @SuppressWarnings("unused")
    public static TimeEstimated estimate(long initialTime, long timeMillis) {
        long deltaMinutes = TimeUnit.MINUTES
                .convert(Math.abs(initialTime - timeMillis), TimeUnit.MILLISECONDS);
        int estimation = getEstimationFromMinutesDelta(deltaMinutes);
        return new TimeEstimated(timeMillis, estimation);
    }

    @ColorInt
    public static int toColor(TimeEstimated timeEstimated, ResourceFinder resourceFinder, boolean inverse) {
        int color = resourceFinder.getColorInt(getColorResourceName(RES_COLOR_ESTIMATION_NO, inverse));
        if (timeEstimated.estimation == Estimation.NOW) {
            color = resourceFinder.getColorInt(getColorResourceName(RES_COLOR_ESTIMATION_NOW, inverse));
        } else if (timeEstimated.estimation == Estimation.ABOUT_TO) {
            color = resourceFinder.getColorInt(getColorResourceName(RES_COLOR_ESTIMATION_ABOUT_TO, inverse));
        }
        return color;
    }

    private static String getColorResourceName(String name, boolean inverse) {
        return (inverse) ? name + INVERSE_SUFFIX : name;
    }

    @Estimation
    public static int getEstimationFromMinutesDelta(long minute) {
        if (minute <= 3) {
            return NOW;
        } else if (minute > 3 && minute < 10) {
            return ABOUT_TO;
        }
        return NO_ESTIMATION;
    }


    @Override
    public String toString() {
        return "TimeEstimated{" +
                "estimation=" + estimation +
                ", timeStr=" + time +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TimeEstimated that = (TimeEstimated) o;
        return estimation == that.estimation && time == that.time;

    }

    @Override
    public int hashCode() {
        int result = estimation;
        result = 31 * result + (int) (time ^ (time >>> 32));
        return result;
    }

    @IntDef
    @Retention(RetentionPolicy.SOURCE)
    public @interface Estimation {
        int NO_ESTIMATION = 0;
        int NOW = 1;
        int ABOUT_TO = 2;
    }
}
