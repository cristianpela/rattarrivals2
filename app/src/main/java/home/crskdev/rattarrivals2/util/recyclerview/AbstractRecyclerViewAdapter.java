package home.crskdev.rattarrivals2.util.recyclerview;

import android.annotation.TargetApi;
import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import android.support.annotation.RestrictTo;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Stream;

/**
 * Created by criskey on 4/10/2017.
 */
public class AbstractRecyclerViewAdapter<T, VH extends RecyclerView.ViewHolder> extends
        RecyclerView.Adapter<VH> implements AdapterOps<T> {

    private AdapterOps<T> adapterOps;

    private ViewHolderBinderManager<VH, T> viewHolderBinderManager;

    public static <T, VH extends RecyclerView.ViewHolder> AbstractRecyclerViewAdapter
    createWithBaseAdapterOpsProvided(ItemLookupStrategy<T> lookupStrategy, ViewHolderBinder<VH, T>... binders){
        return new AbstractRecyclerViewAdapter<>(lookupStrategy, binders);
    }

    public static <T, VH extends RecyclerView.ViewHolder> AbstractRecyclerViewAdapter
    createWithBaseAdapterOpsProvided(ViewHolderBinder<VH, T>... binders){
        return new AbstractRecyclerViewAdapter<>(binders);
    }

    @SafeVarargs
    public AbstractRecyclerViewAdapter(AdapterOps<T> adapterOps, ViewHolderBinder<VH, T>... binders) {
        this.adapterOps = adapterOps;
        this.viewHolderBinderManager = new ViewHolderBinderManager<>(Arrays.asList(binders));
    }

    @SafeVarargs
    @RestrictTo(RestrictTo.Scope.SUBCLASSES)
    protected AbstractRecyclerViewAdapter(ItemLookupStrategy<T> lookupStrategy, ViewHolderBinder<VH, T>... binders) {
        this(new BaseAdapterOps<>(lookupStrategy), binders);
    }

    @SafeVarargs
    @RestrictTo(RestrictTo.Scope.SUBCLASSES)
    protected AbstractRecyclerViewAdapter(ViewHolderBinder<VH, T>... binders) {
        this(new BaseAdapterOps<>(), binders);
    }

    @RestrictTo(RestrictTo.Scope.SUBCLASSES)
    protected AbstractRecyclerViewAdapter() {}


    /**
     * Call this in subclasses constructor if needed
     * @param adapterOps
     * @param binders
     */
    @SafeVarargs
    @SuppressWarnings("JavaDoc")
    protected final void postInit(AdapterOps<T> adapterOps, ViewHolderBinder<VH, T>... binders){
        this.adapterOps = adapterOps;
        this.viewHolderBinderManager = new ViewHolderBinderManager<>(Arrays.asList(binders));
    }

    /**
     * Call this in subclasses constructor if needed
     * @param lookupStrategy
     * @param binders
     */
    @SafeVarargs
    @SuppressWarnings("JavaDoc")
    protected final void postInit(ItemLookupStrategy<T> lookupStrategy, ViewHolderBinder<VH, T>... binders){
        this.adapterOps = new BaseAdapterOps<>(lookupStrategy);
        this.viewHolderBinderManager = new ViewHolderBinderManager<>(Arrays.asList(binders));
    }


    @Override
    public VH onCreateViewHolder(ViewGroup parent, int viewType) {
        return viewHolderBinderManager.onCreateViewHolder(parent, viewType);
    }

    @Override
    public void onBindViewHolder(VH holder, int position) {
        viewHolderBinderManager.onBindViewHolder(holder, adapterOps.getItemAt(position));
    }

    @Override
    public int getItemViewType(int position) {
        return viewHolderBinderManager.getViewType(adapterOps.getItemAt(position));
    }

    @Override
    public T getItemAt(@IntRange(from = 0) int position) {
        return adapterOps.getItemAt(position);
    }

    @Override
    public int getItemCount() {
        return adapterOps.getItemCount();
    }

    @Override
    public void setItems(List<T> items) {
        adapterOps.setItems(items);
        notifyDataSetChanged();
    }

    @SafeVarargs
    @Override
    public final boolean upsert(@NonNull T... arrival) {
        boolean updated =  adapterOps.upsert(arrival);
        if (updated) {
            notifyDataSetChanged();
        }
        return updated;
    }

    @Override
    public int remove(T item) {
        int index = adapterOps.remove(item);
        if(index != NO_INDEX){
            notifyItemRemoved(index);
        }
        return index;
    }

    @Override
    public int changeProperty(Object lookupKey, String propertyName, Object newValue) {
        int index = adapterOps.changeProperty(lookupKey, propertyName, newValue);
        if (index != NO_INDEX) {
            notifyItemChanged(index);
        }
        return index;
    }

    @Override
    public void changePropertyAll(String propertyName, Object newValue) {
        adapterOps.changePropertyAll(propertyName, newValue);
        notifyDataSetChanged();
    }

    @Override
    public ItemLookupStrategy<T> itemLookupStrategy() {
        return adapterOps.itemLookupStrategy();
    }

    @Override
    @TargetApi(24)
    public Stream<T> itemStream() {
        return adapterOps.itemStream();
    }

    @Override
    public Iterator<T> iterator() {
        return adapterOps.iterator();
    }
}
