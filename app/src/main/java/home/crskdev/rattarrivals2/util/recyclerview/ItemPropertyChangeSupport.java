package home.crskdev.rattarrivals2.util.recyclerview;

import android.support.annotation.NonNull;

import java.lang.reflect.Field;

/**
 * Offers reflection support to change object's property. Note: Object's property must be mutable
 * Created by criskey on 6/10/2017.
 */
public class ItemPropertyChangeSupport<T> {

    @SuppressWarnings("TryWithIdenticalCatches")
    public void changeProperty(T item, @NonNull String propertyName, Object newValue){
        try {
            Field field = getField(item, propertyName);
            field.set(item, newValue);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    private Field getField(T item, String propertyName) throws NoSuchFieldException {
        Field field = null;
        Class<?> clazz = item.getClass();
        //propagate up the hierarchy until we we found or not the field;
        NoSuchFieldException ex = null;
        while (field == null) {
            try {
                field = clazz.getDeclaredField(propertyName);
            } catch (NoSuchFieldException e) {
                ex = e;
                clazz = clazz.getSuperclass();
                if (clazz == null) break; // reached Object class
            }
        }
        //noinspection ConstantConditions
        if (field == null) {
            throw ex;
        }
        return field;
    }
}
