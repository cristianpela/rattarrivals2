package home.crskdev.rattarrivals2.util.recyclerview;

import android.support.annotation.IntRange;
import android.support.annotation.LayoutRes;
import android.support.v7.widget.RecyclerView;

/**
 * Mediates the binding between {@link android.support.v7.widget.RecyclerView.Adapter}
 * and a {@link android.support.v7.widget.RecyclerView.ViewHolder}</br>
 * <p>
 * Created by criskey on 4/10/2017.
 */
@SuppressWarnings("WeakerAccess")
public interface ViewHolderBinder<VH extends RecyclerView.ViewHolder, T> {

    int NO_VIEW_TYPE = -1;

    int DEFAULT_VIEW_TYPE = 0;

    /**
     * The actual binding
     *
     * @param viewHolder provided view holder
     * @param item       provided item from adapter's list model
     */
    void onBind(VH viewHolder, T item);

    /**
     * Given a position and item return a unique number of a view type
     * @param item of the adapter's list model
     * @return unique number >=  {@link #DEFAULT_VIEW_TYPE} or {@link #NO_VIEW_TYPE} if the position and item don't satisfy
     * the returning condition
     */
    int getViewTypeByItem(T item);


    /**
     * Item type info
     * @return itemType different than NO_VIEW_TYPE
     */
    @IntRange(from = DEFAULT_VIEW_TYPE)
    int getViewType();

    /**
     * @return layout id
     */
    @LayoutRes
    int layout();


    /**
     * Used by {@link ViewHolderBinderManager}
     * to create the {@link android.support.v7.widget.RecyclerView.ViewHolder}
     *
     * @return the class of the {@link android.support.v7.widget.RecyclerView.ViewHolder}
     */
    Class<VH> getViewHolderClass();

}
