package home.crskdev.rattarrivals2.util.time;

import android.support.annotation.MainThread;
import android.support.annotation.NonNull;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * .
 * Created by criskey on 19/10/2017.
 */
@Singleton
@MainThread
public final class CountValueCacheContainer {

    private Map<String, ResumableCountValue> container;

    @Inject
    public CountValueCacheContainer() {
        container = new HashMap<>();
    }

    /**
     * Adds a {@link ResumableCountValue}. If value is has reach end we avoid
     * adding it and remove key if already present
     *
     * @param key        unique key
     * @param countValue nonNull value
     */
    public void put(String key, @NonNull ResumableCountValue countValue) {
        if (countValue.isFlag()) {
            return; // don't allow flags
        }
        if (countValue.hasReachedEnd()) {
            container.remove(key);
        } else {
            container.put(key, countValue);
        }
    }

    public ResumableCountValue get(String key) {
        return container.get(key);
    }

    public ResumableCountValue getOrElse(String key) {
        return container.get(key);
    }

    public ResumableCountValue remove(String key) {
        return container.remove(key);
    }

}
