package home.crskdev.rattarrivals2.util.time;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.util.Pair;

import com.jakewharton.rxrelay2.BehaviorRelay;
import com.jakewharton.rxrelay2.PublishRelay;
import com.jakewharton.rxrelay2.Relay;

import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.FlowableTransformer;
import io.reactivex.Observable;
import io.reactivex.Scheduler;
import timber.log.Timber;

import static home.crskdev.rattarrivals2.util.time.TimeUtils.safeConvert;

/**
 * A wrapper around {@link IntervalCounterImpl} that feeds the interval with new sets of {@link CountValue}
 * <br> In order to compensate time lost while un/resubscribing, and be fine grain, the internal time Unit is in
 * {@link java.util.concurrent.TimeUnit#MILLISECONDS}
 * Created by criskey on 21/10/2017.
 */
public class FeedingIntervalCounterImpl implements FeedingIntervalCounter {

    private final long min;
    private final long max;
    private final int direction;
    private final TimeUnit periodUnit;
    private final IntervalCounter intervalCounter;
    private final Timeline timeline;
    private final Scheduler scheduler;

    private Relay<CompositeCountValue> feedRelay;

    /**
     * Subject that will cache latest value while the counter is disposed
     */
    private Relay<CompositeCountValue> offlineFeedRelay;

    private AtomicBoolean isOffline;

    public FeedingIntervalCounterImpl(Scheduler scheduler, long min, long max, TimeUnit periodUnit,
                                      int direction) {
        this.min = safeConvert(min, periodUnit, TimeUnit.MILLISECONDS);
        this.max = safeConvert(max, periodUnit, TimeUnit.MILLISECONDS);
        this.direction = direction;
        this.periodUnit = periodUnit;
        this.scheduler = scheduler;

        feedRelay = PublishRelay.create();
        offlineFeedRelay = BehaviorRelay.createDefault(emptyCompositeCountValue());
        isOffline = new AtomicBoolean(true);

        intervalCounter = new IntervalCounterImpl(scheduler, (int) safeConvert(1, periodUnit, TimeUnit.MILLISECONDS));
        timeline = new Timeline(scheduler.now(TimeUnit.MILLISECONDS), periodUnit);
    }

    @Nullable
    @Override
    public CountValue getValue() {
        return intervalCounter.getValue();
    }

    @Override
    public void feedValues(Collection<CountValue> countValues, boolean forced) {
        //add last value from where counter has counter has been stopped/switched
        CompositeCountValue value = (CompositeCountValue) intervalCounter.getValue();

        if (value != null && !forced) {
            /*pre-advance the previous value to keep the timeline advancement in sync
              Time line example (1 minute period)
              t0: [0]
              t1: [1]
              t2: [2, 0] -@t2 new feed val happens - without pre-advance it will look like [1,0],
                           this means prev value will be un-synced with time line
              t3: [3, 1]
              ...
             */
            value = value.advance(safeConvert(1, periodUnit, TimeUnit.MILLISECONDS))
                    .addCountValues(countValues);
        } else {
            if (forced) {
                Timber.d("Previous feed interval value staled! Create new value");
            }
            value = CompositeCountValue.createHomogeneus(min, max, TimeUnit.MILLISECONDS,
                    direction, countValues);
        }

        if (isOffline.get()) {
            //accumulate previous values
            CompositeCountValue prevValue = (CompositeCountValue) ((BehaviorRelay) offlineFeedRelay)
                    .getValue();
            offlineFeedRelay.accept(value.composeWith(prevValue));
        } else {
            feedRelay.accept(value);
            //reset offline subject - feedRelay will take care from here
            offlineFeedRelay.accept(emptyCompositeCountValue());
        }

    }

    @Override
    public void removeFromFeed(@NonNull CountValue countValue) {
        CompositeCountValue value = (CompositeCountValue) intervalCounter.getValue();
        if (value != null) {
            value = value.removeCountValue(countValue);
            if (isOffline.get()) {
                CompositeCountValue prevValue = (CompositeCountValue) ((BehaviorRelay) offlineFeedRelay)
                        .getValue();
                offlineFeedRelay.accept(value.composeWith(prevValue.removeCountValue(value)));
            } else {
                feedRelay.accept(value);
            }
        }
    }

    private FlowableTransformer<CompositeCountValue, CompositeCountValue> handleOnlineSideFX() {
        return upstream -> upstream
                .doOnSubscribe((d) -> {
                    Timber.d("STREAM STARTED - Feeding Counter is ONLINE");
                    goOffline(false);
                })
                //on any terminal events we go offline
                .doOnTerminate(() -> {
                    Timber.d("STREAM TERMINATED - Feeding Counter is OFFLINE");
                    goOffline(true);
                })
                .doOnCancel(() -> {
                    Timber.d("STREAM CANCELED - Feeding Counter is OFFLINE");
                    goOffline(true);
                })
                //is not completing, due to subjects/relays?
                .doOnComplete(() -> {
                    Timber.d("STREAM COMPLETED - Feeding Counter is OFFLINE");
                    goOffline(true);
                });
    }

    @Override
    public Flowable<CountValue> startCount(@NonNull CountValue resumedCountValue) {
        if (!(resumedCountValue instanceof ResumableCountValue)) {
            throw new IllegalArgumentException("resumedCountValue must be instance of ResumableCountValue");
        }

        final ResumableCountValue rcv = (ResumableCountValue) resumedCountValue;

        Flowable<CompositeCountValue> resumeStart = resumedCountValue.isFlag()
                ? Flowable.empty()
                : Flowable.defer(() -> {
            CountValue resumeAt = rcv.resumeAt(scheduler.now(TimeUnit.MILLISECONDS));
            Timber.d("Feeding counter resume count value: %s", resumeAt);
            return Flowable.just(CompositeCountValue.createHomogeneus(min, max,
                    TimeUnit.MILLISECONDS, direction,
                    Collections.singletonList(resumeAt)));
        });

        Flowable<CompositeCountValue> resumeNextDelayed = resumedCountValue.isFlag()
                ? Flowable.empty()
                : Flowable.defer(() -> {
            Timeline.Tick tick = timeline.nextTickFrom(scheduler.now(TimeUnit.MILLISECONDS));
            CountValue resumeAt = rcv.resumeAt(tick.time);
            long delay = tick.until;
            Timber.d("Feeding counter resume next count value, delay: %d [%s] - %s",
                    TimeUnit.SECONDS.convert(delay, TimeUnit.MILLISECONDS),
                    TimeUtils.DATE_FORMAT.format(new Date(tick.time)),
                    resumeAt);
            return Flowable.just(CompositeCountValue.createHomogeneus(min, max,
                    TimeUnit.MILLISECONDS, direction,
                    Collections.singletonList(resumeAt)))
                    .delay(delay, TimeUnit.MILLISECONDS, scheduler);
        });

        Flowable<CompositeCountValue> mergeFeedSubjects = Observable
                .merge(feedRelay.serialize(),
                        offlineFeedRelay.serialize()
                                .filter(ccv -> !ccv.getCountValues().isEmpty()))
                .toFlowable(BackpressureStrategy.LATEST);

        Flowable<CompositeCountValue> mergeAll = resumeNextDelayed
                .publish(rnd -> Flowable.merge(mergeFeedSubjects, rnd.takeUntil(mergeFeedSubjects)));

        Flowable<CountValue> countValueFlowable = mergeAll
                .compose(handleOnlineSideFX())
                .scan(Pair.<Boolean, CountValue>create(null, null), (acc, curr) -> Pair.create(
                        acc.first == null || (acc.first ? false : acc.first),
                        curr
                ))
                .skip(1)//we skip initial value in scan
                .switchMap(pair -> {
                    if (pair.second.hasReachedEnd()) {
                        return Flowable.just(pair.second);
                    }
                    /*
                                               ######Explanation#####
                        Let's say we have an interval in minutes.
                        At 1:20 we feed a new value. This means the current counter will be switched/canceled.
                        The next counter should take in consideration those 20 seconds in between ticks.
                        So the next counter should start at minute 2 (in real time) w/ 40 seconds delay.
                        Also we want the first switch to have a 0 delay. See pair scan above
                     */
                    long delay = 0;
                    if (!pair.first) {
                        Timeline.Tick nextTick = timeline.nextTickFrom(scheduler.now(TimeUnit.MILLISECONDS));
                        delay = nextTick.until;
                    }
                    Timber.d("Switch delay in feed counter %d",
                            TimeUnit.SECONDS.convert(delay, TimeUnit.MILLISECONDS));
                    return intervalCounter
                            .startCount(delay, pair.second)
                            .filter(c -> !c.isFlag())
                            .doOnNext(cv_ -> Timber.d(" Feed counter next: %s", cv_.changeTimeUnit(periodUnit)));
                });

        return Flowable.concat(resumeStart, countValueFlowable)
                .startWith(CountValue.startFlag(min, max));
    }

    @Override
    public Flowable<CountValue> startCount(long ___, @NonNull CountValue __) {
        throw new UnsupportedOperationException();
    }

    private void goOffline(boolean offline) {
      //  timeline.changeOrigin(scheduler.now(TimeUnit.MILLISECONDS));
        boolean old;
        do {
            old = isOffline.get();
        } while (!isOffline.compareAndSet(old, offline));
    }

    private CompositeCountValue emptyCompositeCountValue() {
        return CompositeCountValue.createHomogeneus(min, max,
                TimeUnit.MILLISECONDS, direction, Collections.emptyList());
    }

    boolean isOffline() {
        return isOffline.get();
    }
}
