package home.crskdev.rattarrivals2.util.time;

import android.support.annotation.NonNull;

import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * .
 * Created by criskey on 19/10/2017.
 */
public class ResumableCountValue extends CountValue {

    public final long timestampMillis;

    private CountValue wrapped;

    private ResumableCountValue(CountValue cv, long timestampMillis) {
        super(cv.getValue(), cv.min, cv.max, cv.unit, cv.direction);
        this.timestampMillis = timestampMillis;
        this.wrapped = cv;
    }

    public static ResumableCountValue from(@NonNull CountValue cv, long timestampMillis) {
        return new ResumableCountValue(cv, timestampMillis);
    }

    public static ResumableCountValue from(@NonNull CountValue cv) {
        return new ResumableCountValue(cv, System.currentTimeMillis());
    }

    @Override
    public CountValue changeTimeUnit(TimeUnit toUnit) {
        return ResumableCountValue.from(wrapped.changeTimeUnit(toUnit), timestampMillis);
    }

    @Override
    public CountValue advance(long newValue) {
        return advance(newValue, direction);
    }

    @Override
    public CountValue advance(long newValue, @Dir int direction) {
        CountValue v = wrapped.advance(newValue, direction);
        long n = TimeUtils.safeConvert(v.getValue(), v.unit, TimeUnit.MILLISECONDS);
        return ResumableCountValue.from(v, this.timestampMillis + direction * n);
    }

    public CountValue resumeAt(long timeNowMillis) {
        long delta = timeNowMillis - timestampMillis;
        long valuesPassed = TimeUtils.safeConvert(delta, TimeUnit.MILLISECONDS, unit);
        return wrapped.advance(valuesPassed);
    }

    public CountValue resume() {
        return resumeAt(System.currentTimeMillis());
    }

    @Override
    public boolean isFlag() {
        return wrapped.isFlag();
    }

    @Override
    public String toString() {
        return "ResumableCountValue{" +
                "timestampMillis=" + TimeUtils.DATE_FORMAT.format(new Date(timestampMillis)) +
                " ,countValue= " + super.toString() +
                '}';
    }
}
