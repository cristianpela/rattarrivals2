package home.crskdev.rattarrivals2.util.time;

import java.util.concurrent.TimeUnit;

/**
 *
 * Created by criskey on 2/10/2017.
 */
public final class TimeValue {

    public final long value;

    public final TimeUnit unit;

    public TimeValue(long value, TimeUnit unit) {
        this.value = value;
        this.unit = unit;
    }

    public TimeValue convert(TimeUnit newUnit){
        long newValue = newUnit.convert(value, unit);
        return new TimeValue(newValue, newUnit);
    }


    @Override
    public String toString() {
        return "TimeValue{" +
                "value=" + value +
                ", unit=" + unit +
                '}';
    }
}
