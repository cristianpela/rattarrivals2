package home.crskdev.rattarrivals2.util.time;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

/**
 * .
 * Created by criskey on 15/9/2017.
 */

public final class TimeUtils {

    public static final TimeZone ROMANIAN_TIME_ZONE = TimeZone.getTimeZone("Europe/Bucharest");

    public static final DateFormat HOUR_MINUTES_FORMAT = new SimpleDateFormat("HH:mm", Locale.getDefault());
    public static final DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss", Locale.getDefault());

    public static final TimeValue THROTTLE_TIME_VALUE = new TimeValue(30, TimeUnit.SECONDS);
    public static final TimeValue DEBOUNCE_TIME_VALUE = new TimeValue(300, TimeUnit.MILLISECONDS);
    public static final TimeValue COOLDOWN_TIME_VALUE = new TimeValue(30, TimeUnit.SECONDS);
    public static final TimeValue STALE_TIME_VALUE = new TimeValue(30, TimeUnit.SECONDS);

    static {
        HOUR_MINUTES_FORMAT.setTimeZone(ROMANIAN_TIME_ZONE);
    }

    static {
        DATE_FORMAT.setTimeZone(ROMANIAN_TIME_ZONE);
    }

    private TimeUtils() {
        throw new IllegalStateException("illegal constructor");
    }

    public static long safeConvert(long value, TimeUnit from, TimeUnit to) {
        if (from == to) return value;
        return to.convert(value, from);
    }
}
