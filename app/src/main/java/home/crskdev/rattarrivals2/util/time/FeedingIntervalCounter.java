package home.crskdev.rattarrivals2.util.time;

import java.util.Collection;

/**
 * .
 * Created by criskey on 24/10/2017.
 */
public interface FeedingIntervalCounter extends IntervalCounter {

    /**
     * feed a new set of values that will be composed with passed values (unless forced)
     * @param countValues  new set
     * @param forced  indicates that any passed values will be disregarded, and a new composite value will
     *               be created see {@link CompositeCountValue}
     */
    void feedValues(Collection<CountValue> countValues, boolean forced);

    void removeFromFeed(CountValue countValue);

}
