package home.crskdev.rattarrivals2.util;

import org.reactivestreams.Subscription;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;

/**
 * Created by criskey on 13/9/2017.
 */

public final class RxUtils {

    private RxUtils(){
        throw new UnsupportedOperationException();
    }

    public static Disposable nullDisposable(){
        return Observable.never().subscribe();
    }

}
