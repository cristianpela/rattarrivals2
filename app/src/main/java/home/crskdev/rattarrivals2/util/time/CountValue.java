package home.crskdev.rattarrivals2.util.time;

import android.support.annotation.IntDef;
import android.support.annotation.RestrictTo;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.concurrent.TimeUnit;

import home.crskdev.rattarrivals2.util.MathUtils;
import home.crskdev.rattarrivals2.util.asserts.Asserts;

/**
 * .
 * Created by criskey on 19/10/2017.
 */
public class CountValue {

    public static final CountValue NULL_FLAG = CountValue.startFlag(Long.MIN_VALUE, Long.MAX_VALUE);

    public static final int FORWARD = 1;

    public static final int BACKWARD = -1;

    public final long max;
    public final long min;
    public final TimeUnit unit;
    @Dir
    public final int direction;

    private final long value;

    public CountValue(long value, long min, long max, TimeUnit unit, @Dir int direction) {
        checkInBounds(value, min, max);
        this.value = value;
        this.min = min;
        this.max = max;
        this.unit = unit;
        this.direction = direction;
    }

    public static CountValue endValue(long min, long max,  TimeUnit unit, @Dir int direction){
        if(direction == FORWARD){
            return startAtMax(min, max, unit, direction);
        }else{
            return startAtMin(min, max, unit, direction);
        }
    }

    public static CountValue startFlag(long min, long max) {
        return new FlagCountValue(min, max);
    }

    public static CountValue startAtMax(long min, long max, TimeUnit unit, @Dir int direction) {
        return new CountValue(max, min, max, unit, direction);
    }

    public static CountValue startAtMin(long min, long max, TimeUnit unit, @Dir int direction) {
        return new CountValue(min, min, max, unit, direction);
    }

    public CountValue advance(long newValue, @Dir int direction) {
        long absNewValue = Math.abs(newValue); //absolute to be sure we keep direction applied correct
        long nextValue = nextValue(absNewValue, direction);
        return new CountValue(nextValue, min, max, unit, direction);
    }

    private long nextValue(long newValue, @Dir int direction) {
        return MathUtils.clamp(value + newValue * direction, min, max);
    }

    public CountValue advance(long newValue) {
        return advance(newValue, direction);
    }

    @RestrictTo(RestrictTo.Scope.SUBCLASSES)
    protected void checkInBounds(long value, long min, long max) {
        Asserts.basicAssert(isInBounds(value, min, max), "Value must be within  [" + min + ", " + max + "]");
    }

    private boolean isInBounds(long value, long min, long max) {
        return value >= min && value <= max;
    }

    public CountValue changeTimeUnit(TimeUnit toUnit) {
        long newValue = TimeUtils.safeConvert(value, unit, toUnit);
        long newMin = TimeUtils.safeConvert(min, unit, toUnit);
        long newMax = TimeUtils.safeConvert(max, unit, toUnit);
        return new CountValue(newValue, newMin, newMax, toUnit, direction);
    }

    public CountValue changeDirection(@Dir int newDirection) {
        return (direction != newDirection)
                ? new CountValue(value, min, max, unit, newDirection)
                : this;
    }

    public long getValue() {
        return value;
    }

    public boolean isFlag() {
        return false;
    }

    public boolean hasReachedEnd() {
        return remaining() == 0L;
    }

    public long remaining() {
        return (direction == FORWARD) ? Math.abs(max - value) : Math.abs(min - value);
    }

    public long passed() {
        return (direction == FORWARD) ? Math.abs(min - value) : Math.abs(max - value);
    }

    @SuppressWarnings("SimplifiableIfStatement")
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CountValue that = (CountValue) o;

        if (value != that.value) return false;
        if (max != that.max) return false;
        if (min != that.min) return false;
        if (direction != that.direction) return false;
        return unit == that.unit;

    }

    @Override
    public int hashCode() {
        int result = (int) (value ^ (value >>> 32));
        result = 31 * result + (int) (max ^ (max >>> 32));
        result = 31 * result + (int) (min ^ (min >>> 32));
        result = 31 * result + (unit != null ? unit.hashCode() : 0);
        result = 31 * result + direction;
        return result;
    }

    @Override
    public String toString() {
        return "CountValue{" +
                " value=" + value +
                ",max=" + max +
                ",min=" + min +
                ",unit=" + unit +
                ",dir=" + direction +
                ",isFlag=" + isFlag() +
                '}';
    }

    @IntDef(value = {FORWARD, BACKWARD})
    @Retention(RetentionPolicy.SOURCE)
    public @interface Dir {
    }

    private static final class FlagCountValue extends CountValue {

        private FlagCountValue(long min, long max) {
            super(min, min, max, TimeUnit.MILLISECONDS, FORWARD);
        }

        @Override
        public CountValue advance(long newValue, @Dir int direction) {
            throw new UnsupportedOperationException("Flag count value");
        }

        @Override
        public CountValue advance(long newValue) {
            throw new UnsupportedOperationException("Flag count value");
        }

        @Override
        public CountValue changeTimeUnit(TimeUnit toUnit) {
            throw new UnsupportedOperationException("Flag count value");
        }

        @Override
        public CountValue changeDirection(@Dir int newDirection) {
            throw new UnsupportedOperationException("Flag count value");
        }

        @Override
        public long remaining() {
            throw new UnsupportedOperationException("Flag count value");
        }

        @Override
        public boolean isFlag() {
            return true;
        }

        @Override
        protected void checkInBounds(long value, long min, long max) {
            //don't touch ! - leave it empty as it is
        }

        @Override
        public String toString() {
            return "Flag Count Value";
        }
    }
}
