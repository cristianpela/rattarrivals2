package home.crskdev.rattarrivals2.util.recyclerview;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Stream;

import home.crskdev.rattarrivals2.util.ImmLists;
import home.crskdev.rattarrivals2.util.Pair;

/**
 * Created by criskey on 2/10/2017.
 */

public class BaseAdapterOps<T> implements AdapterOps<T> {

    private List<T> items;

    private ItemLookupStrategy<T> lookupStrategy;

    private ItemPropertyChangeSupport<T> itemPropertyChangeSupport;

    public BaseAdapterOps(ItemLookupStrategy<T> lookupStrategy) {
        items = new ArrayList<>();
        itemPropertyChangeSupport = new ItemPropertyChangeSupport<T>();
        this.lookupStrategy = lookupStrategy;
    }

    public BaseAdapterOps() {
        this(Object::equals);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public T getItemAt(int position) {
        return items.get(position);
    }

    @Override
    public Stream<T> itemStream() {
        return items.stream();
    }

    @Override
    public void setItems(@NonNull List<T> newItems) {
        if (!items.isEmpty())
            items.clear();
        //noinspection Convert2MethodRef
        items.addAll(newItems);
    }

    @SafeVarargs
    @Override
    public final boolean upsert(@NonNull T... newItems) {
        for (T item : newItems) {
            //todo: optimize
            if (items.contains(item)) {
                int index = items.indexOf(item);
                items.set(index, item);
            } else {
                items.add(item);
            }
        }
        //todo maybe return an array of indices if updated or empty array if added
        return true;
    }

    @Override
    public int remove(T item) {
        int index = items.indexOf(item);
        return items.remove(item) ? index : NO_INDEX;
    }

    @Override
    public int changeProperty(Object lookupKey, String propertyName, Object newValue) {
        Pair<Integer, T> result = getIndexAndItemById(lookupKey);
        if (result != null) {
            itemPropertyChangeSupport.changeProperty(result.second, propertyName, newValue);
            return result.first;
        }
        return NO_INDEX;
    }

    @Override
    public void changePropertyAll(String propertyName, Object newValue) {
        for (int i = items.size() - 1; i >= 0; i--) {
            itemPropertyChangeSupport.changeProperty(items.get(i), propertyName, newValue);
        }
    }

    @Override
    public ItemLookupStrategy<T> itemLookupStrategy() {
        return lookupStrategy;
    }

    @Override
    public Iterator<T> iterator() {
        return items.iterator();
    }

    @Nullable
    private Pair<Integer, T> getIndexAndItemById(Object lookupKey) {
        for (int i = 0, s = items.size(); i < s; i++) {
            T item = items.get(i);
            if (lookupStrategy.isMatched(item, lookupKey)) {
                return Pair.of(i, item);
            }
        }
        return null;
    }
}
