package home.crskdev.rattarrivals2.util;

/**
 *  A utility class providing functions useful for common mathematical operations.
 *  that are not present in {@link android.support.v4.math.MathUtils}
 * Created by criskey on 21/10/2017.
 */

public class MathUtils {

    private MathUtils() {}

    /**
     * This method takes a numerical value and ensures it fits in a given numerical range. If the
     * number is smaller than the minimum required by the range, then the minimum of the range will
     * be returned. If the number is higher than the maximum allowed by the range then the maximum
     * of the range will be returned.
     *
     * @param value the value to be clamped.
     * @param min minimum resulting value.
     * @param max maximum resulting value.
     *
     * @return the clamped value.
     */
    public static long clamp(long value, long min, long max) {
        if (value < min) {
            return min;
        } else if (value > max) {
            return max;
        }
        return value;
    }

}
