package home.crskdev.rattarrivals2.util.time;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.concurrent.atomic.AtomicReference;
import java.util.function.UnaryOperator;

import home.crskdev.rattarrivals2.util.asserts.Asserts;
import io.reactivex.Flowable;
import io.reactivex.Scheduler;

/**
 * .
 * Created by criskey on 19/10/2017.
 */
public final class IntervalCounterImpl implements IntervalCounter {

    private final int interval;
    private final Scheduler scheduler;
    private AtomicReference<CountValue> currentRef;

    public IntervalCounterImpl(Scheduler scheduler, int interval) {
        this.scheduler = scheduler;
        this.currentRef = new AtomicReference<>(null);
        this.interval = interval;
    }

    public IntervalCounterImpl(Scheduler scheduler) {
        this(scheduler, 1);
    }

    @Override
    @Nullable
    public CountValue getValue() {
        return currentRef.get();
    }

    @Override
    public Flowable<CountValue> startCount(@NonNull CountValue fromValue) {
        return startCount(0L, fromValue);
    }

    @Override
    public Flowable<CountValue> startCount(long delay, @NonNull CountValue fromValue) {
        Asserts.basicAssert(!fromValue.isFlag(), "Start Flag Count Value is unasignable here");
        CountValue current = updateCurrent(fromValue);
        if (current.hasReachedEnd()) {
            return Flowable.empty(); // we're done
        }
        final long toTake = current.remaining() / interval + 1;
        return Flowable.interval(delay, interval, current.unit, scheduler)
                .scan(CountValue.startFlag(current.min, current.max),
                        (acc, __) -> acc.isFlag() ? current : acc.advance(interval))
                .skip(1) // skip the initial value - the flag
                .take(toTake)
                .onBackpressureLatest()
                .startWith(CountValue.startFlag(current.min, current.max))
                .doOnNext(c -> {
                    if (!c.isFlag()) {
                        updateCurrent(c);
                    }
                });
    }

    /**
     * this is basically {@link AtomicReference#updateAndGet(UnaryOperator)} from 1.8
     *
     * @param countValue to update value
     * @return updatedValue
     */
    @NonNull
    private CountValue updateCurrent(final @NonNull CountValue countValue) {
        CountValue prev, next;
        do {
            prev = currentRef.get();
            next = countValue;
        } while (!currentRef.compareAndSet(prev, next));
        return next;
    }

}
