package home.crskdev.rattarrivals2.util.time;

import java.util.concurrent.TimeUnit;

/**
 * A count value that is identified by a key.
 * Created by criskey on 21/10/2017.
 */
public final class OwnedCountValue<K> extends CountValue {

    public final K key;

    private final CountValue wrapper;

    private OwnedCountValue(CountValue cv, K key) {
        super(cv.getValue(), cv.min, cv.max, cv.unit, cv.direction);
        this.key = key;
        this.wrapper = cv;
    }

    public static <K> OwnedCountValue<K> from(CountValue cv, K key) {
        return new OwnedCountValue<>(cv, key);
    }

    @Override
    public OwnedCountValue advance(long newValue, @Dir int direction) {
        return OwnedCountValue.from(wrapper.advance(newValue, direction), key);
    }

    @Override
    public OwnedCountValue advance(long newValue) {
        return OwnedCountValue.from(wrapper.advance(newValue), key);
    }

    @Override
    public OwnedCountValue changeTimeUnit(TimeUnit toUnit) {
        return OwnedCountValue.from(wrapper.changeTimeUnit(toUnit), key);
    }

    @Override
    public OwnedCountValue changeDirection(@Dir int newDirection) {
        return OwnedCountValue.from(wrapper.changeDirection(newDirection), key);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        OwnedCountValue<?> that = (OwnedCountValue<?>) o;

        return key != null ? key.equals(that.key) : that.key == null;
    }

    @Override
    public int hashCode() {
        return key.hashCode();
    }

    @Override
    public String toString() {
        return "OwnedCountValue{" +
                "key= " + key + ", " +
                super.toString() +
                '}';
    }


}
