package home.crskdev.rattarrivals2.util;

import android.support.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.disposables.DisposableContainer;

import static home.crskdev.rattarrivals2.util.LifecycleCompositeDisposable.DisposePoint.DESTROY;
import static home.crskdev.rattarrivals2.util.LifecycleCompositeDisposable.DisposePoint.PAUSE;

/**
 * .
 * Created by criskey on 30/8/2017.
 */

public class LifecycleCompositeDisposable implements Disposable, DisposableContainer {

    private CompositeDisposable onDestroyDisposable;
    private CompositeDisposable onPauseDisposable;

    public LifecycleCompositeDisposable() {
        onDestroyDisposable = new CompositeDisposable();
        onPauseDisposable = new CompositeDisposable();
    }

    @Override
    public void dispose() {
        onDestroyDisposable.dispose();
        onPauseDisposable.dispose();
        onDestroyDisposable = new CompositeDisposable();
        onPauseDisposable = new CompositeDisposable();
    }

    @Override
    public boolean isDisposed() {
        return onDestroyDisposable.isDisposed() && onPauseDisposable.isDisposed();
    }

    public boolean add(Disposable disposable, @DisposePoint int disposePoint) {
        boolean added = false;
        switch (disposePoint) {
            case DisposePoint.DESTROY:
                added = onDestroyDisposable.add(disposable);
                break;
            case DisposePoint.PAUSE:
                added = onPauseDisposable.add(disposable);
                break;
        }
        return added;
    }

    @Override
    public boolean add(Disposable disposable) {
        return add(disposable, DESTROY);
    }

    @Override
    public boolean remove(Disposable d) {
        return onDestroyDisposable.remove(d) || onPauseDisposable.remove(d);
    }

    @Override
    public boolean delete(Disposable d) {
        return onDestroyDisposable.delete(d) || onPauseDisposable.delete(d);
    }

    public void dispose(@DisposePoint int disposePoint) {
        switch (disposePoint) {
            case DisposePoint.DESTROY:
                onDestroyDisposable.dispose();
                onDestroyDisposable = new CompositeDisposable();
                break;
            case DisposePoint.PAUSE:
                onPauseDisposable.dispose();
                onPauseDisposable = new CompositeDisposable();
                break;
        }
    }

    @IntDef(value = {PAUSE, DESTROY})
    @Retention(RetentionPolicy.SOURCE)
    public @interface DisposePoint {
        int PAUSE = 0;
        int DESTROY = 1;
    }


}
