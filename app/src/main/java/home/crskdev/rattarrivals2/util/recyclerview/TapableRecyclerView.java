package home.crskdev.rattarrivals2.util.recyclerview;


import android.content.Context;
import android.support.annotation.IntDef;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import io.reactivex.Observable;
import io.reactivex.subjects.BehaviorSubject;

import static home.crskdev.rattarrivals2.util.recyclerview.TapableRecyclerView.TapKind.DOUBLE_TAP;
import static home.crskdev.rattarrivals2.util.recyclerview.TapableRecyclerView.TapKind.LONG_TAP;
import static home.crskdev.rattarrivals2.util.recyclerview.TapableRecyclerView.TapKind.MOVE;
import static home.crskdev.rattarrivals2.util.recyclerview.TapableRecyclerView.TapKind.SINGLE_TAP;
import static home.crskdev.rattarrivals2.util.recyclerview.TapableRecyclerView.TapKind.SWIPE_LEFT;
import static home.crskdev.rattarrivals2.util.recyclerview.TapableRecyclerView.TapKind.SWIPE_RIGHT;


/**
 * .
 * Created by criskey on 18/7/2017.
 */
public class TapableRecyclerView extends RecyclerView {

    protected BehaviorSubject<TapEvent> tapSubject;

    public TapableRecyclerView(Context context) {
        super(context);
        init();
    }

    public TapableRecyclerView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public TapableRecyclerView(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        if(this.isInEditMode()){
            //we are in a developer tool ui builder
            return;
        }

        tapSubject = BehaviorSubject.create();
        addOnItemTouchListener(new OnItemTouchListener() {

            GestureDetector gestureDetector = new GestureDetector(getContext(), new GestureDetector
                    .SimpleOnGestureListener() {

                @Override
                public void onLongPress(MotionEvent e) {
                    sendTapEvent(e, LONG_TAP);
                }

                @Override
                public boolean onSingleTapConfirmed(MotionEvent e) {
                    sendTapEvent(e, SINGLE_TAP);
                    return true;
                }

                @Override
                public boolean onDoubleTap(MotionEvent e) {
                    sendTapEvent(e, DOUBLE_TAP);
                    return true;
                }

                private void sendTapEvent(MotionEvent e, int kind) {
                    View v = findChildViewUnder(e.getX(), e.getY());
                    if (v != null) {
                        int position = getChildAdapterPosition(v);
                        int viewType = getAdapter().getItemViewType(position);
                        Object extra = v.getTag();
                        tapSubject.onNext(new TapEvent(kind, position, viewType, extra));
                    }
                }
            });

            @Override
            public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
                return gestureDetector.onTouchEvent(e);
            }

            @Override
            public void onTouchEvent(RecyclerView rv, MotionEvent e) {
            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {
            }
        });
    }

    public Observable<TapEvent> tapObservable() {
        return tapSubject.filter(ev -> ev != null);
    }

    @IntDef(value = {SINGLE_TAP, LONG_TAP, DOUBLE_TAP, SWIPE_LEFT, SWIPE_RIGHT, MOVE})
    @Retention(RetentionPolicy.SOURCE)
    public @interface TapKind {
        int SINGLE_TAP = 0;
        int LONG_TAP = 1;
        int DOUBLE_TAP = 2;
        int SWIPE_LEFT = 3;
        int SWIPE_RIGHT = 4;
        int MOVE = 5;
    }

    public static final class TapEvent {

        @TapKind
        public final int kind;
        public final int position;
        public final int viewType;
        public final Object extra;

        public TapEvent(@TapKind int kind, int position, int viewType) {
            this(kind, position, viewType, null);
        }

        public TapEvent(@TapKind int kind, int position, int viewType, Object extra) {
            this.kind = kind;
            this.position = position;
            this.viewType = viewType;
            this.extra = extra;
        }

        public TapEvent addExtra(Object extra) {
            return new TapEvent(kind, position, viewType, extra);
        }

        @Override
        public String toString() {
            return "TapEvent{" +
                    "kind=" + kind +
                    ", position=" + position +
                    ", viewType=" + viewType +
                    ", extra =" + extra +
                    '}';
        }
    }
}