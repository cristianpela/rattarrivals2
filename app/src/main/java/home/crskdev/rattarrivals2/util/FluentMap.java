package home.crskdev.rattarrivals2.util;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by criskey on 5/1/2017.
 */
public class FluentMap<K, V> {

    private Map<K, V> mMap;

    public FluentMap(Map<K, V> mMap) {
        this.mMap = mMap;
    }

    public FluentMap() {
        this.mMap = new HashMap<>();
    }

    public static <K, V> Map<K, V> single(K key, V value) {
        return new FluentMap<>().put(key, value).get();
    }

    public FluentMap put(K key, V value) {
        mMap.put(key, value);
        return this;
    }

    public Map<K, V> get() {
        return mMap;
    }

}
