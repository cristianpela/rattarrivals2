package home.crskdev.rattarrivals2.util.time;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import static home.crskdev.rattarrivals2.util.time.TimeUtils.ROMANIAN_TIME_ZONE;
import static home.crskdev.rattarrivals2.util.time.TimeEstimated.Estimation.*;

/**
 * Created by criskey on 1/9/2017.
 */
public class TimeConverter {

    private final long initialTime;
    private Calendar calendar;

    public TimeConverter(long seedMillis, TimeZone timeZone) {
        calendar = GregorianCalendar.getInstance(timeZone);
        initialTime = seedMillis;
        resetCalendar();
    }

    public TimeConverter(long seedMillis) {
        calendar = GregorianCalendar.getInstance(ROMANIAN_TIME_ZONE);
        initialTime = seedMillis;
        resetCalendar();
    }

    public TimeConverter() {
        this(System.currentTimeMillis(), ROMANIAN_TIME_ZONE);
    }


    public TimeEstimated format(String time) {
        final int minIndex;
        int estimation = NO_ESTIMATION;
        //unconfirmed, but I think that "min" and ">>" on the raw arrival data
        //have a have better accuracy of arrival prediction.
        //if so we only use estimation to "min" and ">>" format branches
        if ((minIndex = time.toLowerCase().indexOf("min")) != -1) {
            int minute = Integer.parseInt(time.substring(0, minIndex).trim());
            estimation = TimeEstimated.getEstimationFromMinutesDelta(minute);
            calendar.add(Calendar.MINUTE, minute);
        } else if (time.equals(">>")) {
            estimation = NOW;
        } else if (time.equals("**:**")) {
            calendar.setTimeInMillis(0L);
        } else if (time.contains(":")) {
            String[] hhmm = time.split(":");
            int hh = Integer.parseInt(hhmm[0]);
            int mm = Integer.parseInt(hhmm[1]);
            if (calendar.get(Calendar.HOUR_OF_DAY) > hh) {
                //advance one day
                calendar.add(Calendar.DATE, 1);
            }
            calendar.set(Calendar.HOUR_OF_DAY, hh);
            calendar.set(Calendar.MINUTE, mm);
        }
        long newTimeMillis = calendar.getTimeInMillis();

        resetCalendar();
        return new TimeEstimated(newTimeMillis, estimation);
    }


    private void resetCalendar() {
        calendar.setTimeInMillis(initialTime);
    }

}
