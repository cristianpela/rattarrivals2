package home.crskdev.rattarrivals2.util.asserts;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Credits:
 * <a href="https://stackoverflow.com/questions/42426108/adding-dividers-in-gridlayout-recyclerview">
 *     Stockoverflow</a><br/>
 * Created by criskey on 11/9/2017.
 */
public class SpacesItemDecoration extends RecyclerView.ItemDecoration {

    private int space;

    public SpacesItemDecoration(int space) {
        this.space = space;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        outRect.bottom = space;
    }
}