package home.crskdev.rattarrivals2.util.recyclerview;

import android.support.annotation.LayoutRes;
import android.support.v7.widget.RecyclerView;

/**
 * Convenient class for DEFAULT_VIEW_TYPE implementation.
 * Created by criskey on 6/10/2017.
 */
public abstract class DefaultViewHolderBinder<VH extends RecyclerView.ViewHolder, T>
        implements ViewHolderBinder<VH, T> {

    private int resLayout;

    public DefaultViewHolderBinder(@LayoutRes int resLayout) {
        this.resLayout = resLayout;
    }

    @Override
    public int getViewTypeByItem(T item) {
        return DEFAULT_VIEW_TYPE;
    }

    @Override
    public int getViewType() {
        return DEFAULT_VIEW_TYPE;
    }

    @Override
    public int layout() {
        return resLayout;
    }

}
