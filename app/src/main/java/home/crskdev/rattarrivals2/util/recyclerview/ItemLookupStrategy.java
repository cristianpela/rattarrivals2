package home.crskdev.rattarrivals2.util.recyclerview;

/**
 * Strategy used by {@link AdapterOps} to look up an item
 * Created by criskey on 6/10/2017.
 */
public interface ItemLookupStrategy<T> {
    boolean isMatched(T current, Object comparingKey);
}
