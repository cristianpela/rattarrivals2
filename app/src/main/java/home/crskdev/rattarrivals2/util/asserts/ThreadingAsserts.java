package home.crskdev.rattarrivals2.util.asserts;

/**
 * Created by criskey on 7/7/2017.
 */
public interface ThreadingAsserts {

    boolean assertIsOnMainThread();

    boolean assertIsBackgroundThread();

}