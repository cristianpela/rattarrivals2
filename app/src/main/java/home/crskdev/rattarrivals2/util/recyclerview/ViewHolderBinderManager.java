package home.crskdev.rattarrivals2.util.recyclerview;

import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import home.crskdev.rattarrivals2.util.ImmLists;
import home.crskdev.rattarrivals2.util.asserts.Asserts;

import static home.crskdev.rattarrivals2.util.recyclerview.ViewHolderBinder.DEFAULT_VIEW_TYPE;
import static home.crskdev.rattarrivals2.util.recyclerview.ViewHolderBinder.NO_VIEW_TYPE;

/**
 * Dispatches the {@link ViewHolder}
 * to an appropriate {@link ViewHolderBinder}
 * Created by criskey on 4/10/2017.
 */
@SuppressWarnings("WeakerAccess")
public class ViewHolderBinderManager<VH extends ViewHolder, T> {

    private List<ViewHolderBinder<VH, T>> viewHolderBinders;

    public ViewHolderBinderManager(@NonNull List<ViewHolderBinder<VH, T>> viewHolderBinders) {
        Asserts.basicAssert(!viewHolderBinders.isEmpty(), "View Holder Binder list is empty!");
        this.viewHolderBinders = createUniqueBinderList(viewHolderBinders);
    }

    /*
     * Compare ViewHolder's class name by using a wrapper Comparable
     * @param viewHolderBinders
     * @return no duplicate binders
     */
    private List<ViewHolderBinder<VH, T>> createUniqueBinderList(List<ViewHolderBinder<VH, T>> viewHolderBinders) {
        //we need mutually comparable list of objects to satisfy the sorting contract
        List<ComparableBinder<ViewHolderBinder<VH, T>>> copy = ImmLists
                .mutableMap(viewHolderBinders, ComparableBinder::new);
        Collections.sort(copy);
        List<ViewHolderBinder<VH, T>> out = new ArrayList<>(copy.size());
        int s = copy.size();
        for (int i = 0; i < s; i++) {
            int j = i;
            while (j + 1 < s && copy.get(j + 1).compareTo(copy.get(i)) == 0) {
                j++;
            }
            out.add(copy.get(j).binder);
            i = j;
        }
        return out;
    }

    public VH onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewHolderBinder<VH, T> viewHolderBinder = getViewHolderBinderByType(viewType);
        View view = LayoutInflater.from(parent.getContext())
                .inflate(viewHolderBinder.layout(), parent, false);
        return createViewHolder(view, viewHolderBinder.getViewHolderClass());
    }

    public void onBindViewHolder(VH holder, T item) {
        ViewHolderBinder<VH, T> viewHolderBinder = getViewHolderBinderByViewHolder(holder);
        viewHolderBinder.onBind(holder, item);
    }

    public int getViewType(T item) {
        for (ViewHolderBinder<VH, T> vhb : viewHolderBinders) {
            int viewType = vhb.getViewTypeByItem(item);
            // DEFAULT_VIEW_TYPE has low priority so we skip it
            // because this view type doesn't do any checks on item
            // and we don't want to return before we make sure there is no other ViewHolderBinder
            // which has a view type by item
            if (viewType != NO_VIEW_TYPE && viewType != DEFAULT_VIEW_TYPE) {
                return viewType;
            }
        }
        return DEFAULT_VIEW_TYPE;
    }

    private ViewHolderBinder<VH, T> getViewHolderBinderByViewHolder(VH holder) {
        for (ViewHolderBinder<VH, T> vhb : viewHolderBinders) {
            if (holder.getClass().equals(vhb.getViewHolderClass())) {
                return vhb;
            }
        }
        throw new IllegalStateException("Could not find a view holder binder for " +
                holder.getClass().getSimpleName());
    }

    private ViewHolderBinder<VH, T> getViewHolderBinderByType(int viewType) {
        for (ViewHolderBinder<VH, T> vhb : viewHolderBinders) {
            if (vhb.getViewType() == viewType) {
                return vhb;
            }
        }
        throw new IllegalStateException("Could not find a view holder binder by viewType " +
                viewType);
    }

    @SuppressWarnings("TryWithIdenticalCatches")
    private VH createViewHolder(View view, Class<VH> viewHolderClass) {
        Exception ex;
        try {
            Constructor<VH> constructor = viewHolderClass.getConstructor(View.class);
            return constructor.newInstance(view);
        } catch (NoSuchMethodException e) {
            ex = e;
        } catch (IllegalAccessException e) {
            ex = e;
        } catch (InstantiationException e) {
            ex = e;
        } catch (InvocationTargetException e) {
            ex = e;
        }
        throw new RuntimeException(ex);
    }

    /**
     * Simulates a onBindViewHolder call. Use this method in tests
     *
     * @param parent parent
     * @param items  emitted items
     */
    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    void emitOnBind(ViewGroup parent, List<T> items) {
        for (T item : items) {
            int type = getViewType(item);
            VH vh = onCreateViewHolder(parent, type);
            onBindViewHolder(vh, item);
        }
    }

    int getBinderCount() {
        return viewHolderBinders.size();
    }

    private static class ComparableBinder<VHB extends ViewHolderBinder> implements
            Comparable<ComparableBinder> {

        private VHB binder;

        ComparableBinder(VHB binder) {
            this.binder = binder;
        }

        @Override
        public int compareTo(@NonNull ComparableBinder o) {
            final String name1 = binder.getViewHolderClass().getName();
            final String name2 = o.binder.getViewHolderClass().getName();
            return name1.compareTo(name2);
        }
    }

}
