package home.crskdev.rattarrivals2.util.asserts;

import android.os.Looper;

/**
 * Created by criskey on 6/7/2017.
 */
public final class Asserts {

    private static final String PREFIX = "Assertion Failed: ";

    /**
     * on tests change this to a stub implementation of {@link ThreadingAsserts}
     */
    public static ThreadingAsserts threadingAsserts = new AndroidThreadingAsserts();

    private Asserts() {
        throw new UnsupportedOperationException();
    }

    public static void assertIsOnMainThread() {
        basicAssert(threadingAsserts.assertIsOnMainThread(),
                "Method must be called from main thread");
    }

    public static void assertIsBackgroundThread() {
        basicAssert(threadingAsserts.assertIsBackgroundThread(),
                "Method must be called from background thread");
    }

    public static void basicAssert(boolean check, String failedMessage) {
        if (!check) {
            throw new RuntimeException(PREFIX + failedMessage);
        }
    }

    private static class AndroidThreadingAsserts implements ThreadingAsserts {

        @Override
        public boolean assertIsOnMainThread() {
            return Looper.myLooper() == Looper.getMainLooper();
        }

        @Override
        public boolean assertIsBackgroundThread() {
            return Looper.myLooper() != Looper.getMainLooper();
        }
    }

}
