package home.crskdev.rattarrivals2.util;


import android.support.annotation.NonNull;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import home.crskdev.rattarrivals2.arrivals.model.ArrivalExtraViewModel;
import io.reactivex.functions.BiFunction;
import io.reactivex.functions.Function;
import io.reactivex.functions.Predicate;

/**
 * Created by criskey on 15/6/2017.
 */

public final class ImmLists {


    public static <T> List<T> fromIterator(Iterator<T> iterator){
        List<T> out = new ArrayList<T>();
        while(iterator.hasNext()){
            out.add(iterator.next());
        }
        return Collections.unmodifiableList(out);
    }

    public static <T> List<T> add(T element, List<T> src) {
        ArrayList<T> out = new ArrayList<>(src);
        out.add(element);
        return Collections.unmodifiableList(out);
    }

    public static <T> List<T> addAll(Collection<T> elements, Collection<T> src) {
        ArrayList<T> out = new ArrayList<>(src);
        out.addAll(elements);
        return Collections.unmodifiableList(out);
    }

    public static <T> List<T> remove(int index, List<T> src) {
        ArrayList<T> out = new ArrayList<>(src);
        out.remove(index);
        return Collections.unmodifiableList(out);
    }

    public static <T> List<T> remove(T element, List<T> src) {
        ArrayList<T> out = new ArrayList<>(src);
        out.remove(findIndex(element, out));
        return Collections.unmodifiableList(out);
    }

    public static <T> List<T> set(int index, T element, List<T> src) {
        ArrayList<T> out = new ArrayList<>(src);
        out.set(index, element);
        return Collections.unmodifiableList(out);
    }

    public static <T> List<T> set(T element, List<T> src) {
        ArrayList<T> out = new ArrayList<>(src);
        out.set(findIndex(element, out), element);
        return Collections.unmodifiableList(out);
    }

    public static <T> List<T> asMutable(List<T> list) {
        return new ArrayList<T>(list);
    }

    public static <T> List<T> clear() {
        return new ArrayList<>();
    }


    public static <Out, In> List<Out> mutableMap(@NonNull Collection<In> list, Function<In, Out> func) {
        List<Out> mapped = new ArrayList<Out>(list.size());
        for (In f : list) {
            try {
                mapped.add(func.apply(f));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return mapped;
    }

    public static <In, Out> List<Out> mutableMap(@NonNull Collection<In> list, BiFunction<In, Integer, Out> func) {
        List<Out> mapped = new ArrayList<Out>(list.size());
        int index = 0;
        for (In f : list) {
            try {
                mapped.add(func.apply(f, index));
            } catch (Exception e) {
                e.printStackTrace();
            }
            index++;
        }
        return mapped;
    }

    public static <Out extends In, In> List<Out> cast(@NonNull Collection<In> list, Class<Out> clazz) {
        return Collections.unmodifiableList(mutableMap(list, clazz::cast));
    }

    public static <Out, In> List<Out> map(@NonNull Collection<In> list, Function<In, Out> func) {
        return Collections.unmodifiableList(mutableMap(list, func));
    }

    public static <Out, In> List<Out> map(@NonNull Collection<In> list, BiFunction<In, Integer, Out> func) {
        return Collections.unmodifiableList(mutableMap(list, func));
    }

    public static <Out, In> List<Out> mutableMap(@NonNull In[] objects, Function<In, Out> func) {
        List<Out> mapped = new ArrayList<Out>(objects.length);
        for (In f : objects) {
            try {
                mapped.add(func.apply(f));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return mapped;
    }

    public static <Out, In> List<Out> mutableMap(@NonNull In[] objects, BiFunction<In, Integer, Out> func) {
        List<Out> mapped = new ArrayList<Out>(objects.length);
        int index = 0;
        for (In f : objects) {
            try {
                mapped.add(func.apply(f, index));
            } catch (Exception e) {
                e.printStackTrace();
            }
            index++;
        }
        return mapped;
    }

    public static <Out, In> List<Out> map(@NonNull In[] objects, Function<In, Out> func) {
        return Collections.unmodifiableList(mutableMap(objects, func));
    }

    public static <Out, In> List<Out> map(@NonNull In[] objects, BiFunction<In, Integer, Out> func) {
        return Collections.unmodifiableList(mutableMap(objects, func));
    }

    public static <T> List<T> filter(@NonNull Collection<T> list, Predicate<T> predicate) {
        List<T> out = new ArrayList<>();
        try {
            for (T item : list) {
                if (predicate.test(item))
                    out.add(item);
            }
        } catch (Exception e) {
            //
        }
        return Collections.unmodifiableList(out);
    }

    public static <In, Out> Out reduce(@NonNull Collection<In> list,
                                       final @NonNull Out init, TriFunction<Out, In, Integer, Out> func) {
        Out acc = init;
        int index = 0;
        for (In elem : list) {
            //accumulator
            acc = func.apply(acc, elem, index);
            index++;
        }
        return acc;
    }

    private static <T> int findIndex(T element, ArrayList<T> out) {
        int index = -1;
        for (int i = 0; i < out.size(); i++) {
            if (out.get(i).equals(element)) {
                index = i;
                break;
            }
        }
        if (index == -1) {
            throw new RuntimeException("Element " + element + " is not part of the source collection");
        }
        return index;
    }

    public static <T> Chain<T> of(List<T> src) {
        return new Chain<>(src);
    }

    public static <T> T[] toArray(List<T> list, Class<T> clazz) {
        @SuppressWarnings("unchecked")
        T[] array = (T[]) Array.newInstance(clazz, list.size());
        return list.toArray(array);
    }

    public static <T> void print(List<T> list) {
        for (T t : list) {
            System.out.println(t);
        }
    }

    public interface TriFunction<In1, In2, In3, Out> {

        Out apply(In1 in1, In2 in2, In3 in3);

    }

    public static class Chain<T> {

        private List<T> list;

        private Chain(List<T> list) {
            this.list = new ArrayList<>(list);
        }

        public static <T> Chain<T> from(List<T> list) {
            return new Chain<>(list);
        }

        public static <T> Chain<T> from(Collection<T> col) {
            return new Chain<>(new ArrayList<T>(col));
        }


        public Chain<T> add(T element) {
            return Chain.from(ImmLists.add(element, list));
        }

        public Chain<T> addAll(Collection<T> elements) {
            return Chain.from(ImmLists.addAll(elements, list));
        }

        public Chain<T> remove(int index) {
            return Chain.from(ImmLists.remove(index, list));
        }


        public Chain<T> remove(T element) {
            return Chain.from(ImmLists.remove(element, list));
        }

        public Chain<T> set(int index, T element) {
            return Chain.from(ImmLists.set(index, element, list));
        }

        public Chain<T> set(T element) {
            return Chain.from(ImmLists.set(element, list));
        }

        public Chain<T> clear() {
            return Chain.from(ImmLists.clear());
        }

        public Chain<T> filter(Predicate<T> predicate) {
            return Chain.from(ImmLists.filter(list, predicate));
        }

        public Chain<T> ifEmptyThen(List<T> fallBackList) {
            return Chain.from((list.isEmpty()) ? new ArrayList<T>(fallBackList) : list);
        }

        public Chain<T> sort(Comparator<? super T> comparator) {
            List<T> l = new ArrayList<>(list);
            Collections.sort(l, comparator);
            return Chain.from(l);
        }

        public <S> Chain<S> map(Function<T, S> func) {
            return Chain.from(ImmLists.map(list, func));
        }

        public <S extends T> Chain<S> cast(Class<S> clazz) {
            return Chain.from(ImmLists.cast(list, clazz));
        }

        public Chain<T> setForEach(Function<T, T> func) {
            return Chain.from(ImmLists.map(list, func));
        }

        //un-wrappers
        public void print() {
            ImmLists.print(list);
        }

        public <R> R reduce(final @NonNull R init, TriFunction<R, T, Integer, R> func) {
            return ImmLists.reduce(list, init, func);
        }

        public List<T> get() {
            return Collections.unmodifiableList(list);
        }

        public T[] toArray(Class<T> clazz) {
            return ImmLists.toArray(list, clazz);
        }

    }

}