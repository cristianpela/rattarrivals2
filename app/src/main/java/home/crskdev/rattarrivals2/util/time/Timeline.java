package home.crskdev.rattarrivals2.util.time;

import android.support.annotation.NonNull;

import java.sql.Time;
import java.util.Date;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

import home.crskdev.rattarrivals2.util.asserts.Asserts;
import timber.log.Timber;

/**
 * Represents ticks between two defined moments in time.
 * In milliseconds
 * <p>
 * Created by criskey on 23/10/2017.
 */
public final class Timeline {

    private static final int FORWARD = 1;
    private static final int BACKWARD = -1;
    //1 second drift behind allowance
    private static final long DRIFT_BEHIND_TOLERANCE = 1000L;
    private AtomicLong origin = new AtomicLong();
    private long periodMillis;
    private TimeUnit tickUnit;

    public Timeline(long originMillis, TimeUnit tickUnit) {
        this(tickUnit);
        // Asserts.basicAssert(originMillis > 0L, "Origin millis must be larger then 0!");
        initOrigin(originMillis);
    }

    public Timeline(TimeUnit tickUnit) {
        this.tickUnit = tickUnit;
        origin = new AtomicLong();
        periodMillis = TimeUnit.MILLISECONDS.convert(1L, tickUnit);
    }


    public Tick nextTickFrom(long timeMomentMillis) {
        return advanceTick(timeMomentMillis, FORWARD);
    }

    public Tick previousTickFrom(long timeMomentMillis) {
        return advanceTick(timeMomentMillis, BACKWARD);
    }

    public long getOrigin() {
        return origin.get();
    }

    public void changeOrigin(long originMillis) {
        long old;
        do {
            old = origin.get();
        } while (!origin.compareAndSet(old, originMillis));
    }

    public void initOrigin(long originMillis) {
        origin.set(originMillis);
        Timber.d("Timeline set origin: %s", TimeUtils.DATE_FORMAT.format(new Date(origin.get())));
    }

    public Timeline spawn(long origin) {
        return new Timeline(origin, tickUnit);
    }

    @NonNull
    private Tick advanceTick(long timeMomentMillis, int direction) {
        long originTime = origin.get();
        Asserts.basicAssert(timeMomentMillis >= originTime, "No passed time allowed");
        long diff = timeMomentMillis - originTime;
        long remainder = diff % periodMillis;
        long ticks = diff / periodMillis;
        long nextTime;
        if (remainder == 0) {
            nextTime = originTime + (ticks + direction) * periodMillis;
        } else {
            long dirSnap = (direction == BACKWARD) ? 0 : 1;
            nextTime = originTime + (ticks + dirSnap) * periodMillis;
        }
        return new Tick(nextTime,
                Math.abs((direction == FORWARD) ? periodMillis - remainder : remainder));
    }

    public Iterator<Tick> iterateToOrigin(final long timeMomentMillis) {
        return new Iterator<Tick>() {

            Tick tick = new Tick(Long.MAX_VALUE, 0L);

            @Override
            public boolean hasNext() {
                return tick.time > origin.get();
            }

            @Override
            public Tick next() {
                return tick = previousTickFrom((tick.time == Long.MAX_VALUE)
                        ? timeMomentMillis
                        : tick.time);
            }
        };
    }

    public boolean isBehind(long now) {
        return origin.get() + DRIFT_BEHIND_TOLERANCE < now;
    }


    public static final class Tick {

        public final long time;
        public final long until;

        public Tick(long time, long until) {
            this.time = time;
            this.until = until;
        }

        @Override
        public String toString() {
            return "Tick{" +
                    "time=" + time +
                    ", until=" + until +
                    '}';
        }
    }

}
