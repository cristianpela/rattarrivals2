package home.crskdev.rattarrivals2.util;

import android.support.annotation.IntRange;
import android.support.annotation.NonNull;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

import javax.inject.Inject;

import home.crskdev.rattarrivals2.util.asserts.Asserts;
import home.crskdev.rattarrivals2.util.time.IntervalCounterImpl;
import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.schedulers.Schedulers;

import static home.crskdev.rattarrivals2.model.Result.Cooldown;

/**
 * Use the general {@link IntervalCounterImpl} instead.
 * Created by criskey on 4/9/2017.
 */
@Deprecated
public final class CountdownCooldown {

    private static final int TERMINATION_STARTING_VALUE = 0;

    private AtomicReference<Cooldown> currentRef;
    private Scheduler scheduler;

    public CountdownCooldown(Scheduler scheduler, int startValue) {
        this.scheduler = scheduler;
        this.currentRef = new AtomicReference<>(Cooldown.just(startValue));
    }

    public CountdownCooldown(int startValue) {
        this(Schedulers.computation(), startValue);
    }

    @Inject
    public CountdownCooldown() {
        this(Schedulers.computation(), TERMINATION_STARTING_VALUE);
    }

    public Observable<Cooldown> startCooldown() {
        Cooldown current = currentRef.get();
        Asserts.basicAssert(current.value <= current.max,
                "Illegal state: remaining value must be lesser or eq to max");
        if (current.value <= TERMINATION_STARTING_VALUE) {
            return Observable.empty(); // we're done
        }
        return Observable.interval(1, TimeUnit.SECONDS, scheduler)
                .scan(current.value, (acc, __) -> acc - 1)
                .map(remain -> new Cooldown(remain, current.max))
                .take(current.value + 1)
                .startWith(Cooldown.asStartFlag(current.max))
                .doOnNext(c -> {
                    if (!c.isStart()) {
                        updateCurrent(c);
                    }
                });
    }

    /**
     * Use this method as a post inject, in case of using dagger
     *
     * @param startValue - starting value
     */
    public void setStartValue(@IntRange(from = 1) int startValue) {
        updateCurrent(Cooldown.just(startValue));
    }

    public void resume(Cooldown resumeValue) {
        Cooldown rv = (resumeValue.value < TERMINATION_STARTING_VALUE)
                ? new Cooldown(TERMINATION_STARTING_VALUE, resumeValue.max)
                : resumeValue;
        updateCurrent(rv);
    }

    @NonNull
    public Cooldown getRemaining() {
        return currentRef.get();
    }

    private void updateCurrent(Cooldown cooldown) {
        boolean updated = false;
        while (!updated) {
            Cooldown old = currentRef.get();
            updated = currentRef.compareAndSet(old, cooldown);
        }
    }
}
