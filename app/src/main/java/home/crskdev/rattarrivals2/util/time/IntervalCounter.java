package home.crskdev.rattarrivals2.util.time;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import io.reactivex.Flowable;

/**
 * .
 * Created by criskey on 21/10/2017.
 */

public interface IntervalCounter {
    @Nullable
    CountValue getValue();

    Flowable<CountValue> startCount(@NonNull CountValue fromValue);

    Flowable<CountValue> startCount(long delay, @NonNull CountValue fromValue);
}
