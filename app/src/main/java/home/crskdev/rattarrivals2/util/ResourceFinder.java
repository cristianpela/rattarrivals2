package home.crskdev.rattarrivals2.util;

import android.support.annotation.ArrayRes;
import android.support.annotation.ColorInt;
import android.support.annotation.ColorRes;
import android.support.annotation.DrawableRes;
import android.support.annotation.StringRes;

/**
 * Created by criskey on 16/10/2017.
 */
public interface ResourceFinder {
    @ColorRes
    int getColorId(String name);

    @StringRes
    int getStringId(String string);

    @ArrayRes
    int getArrayId(String array);

    @ColorInt
    int getColorInt(String name);

    @DrawableRes
    int getImage(String name);

    String getString(String resName);

    String getString(String resName, Object... formatArgs);

    String[] getStringArray(String resName);
}
