package home.crskdev.rattarrivals2.util;

import android.app.Activity;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.DrawableRes;
import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.BaseTransientBottomBar;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import java.io.Serializable;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import home.crskdev.rattarrivals2.R;

/**
 * Created by criskey on 19/8/2017.
 */

public class ViewUtils {

    public static void toBundle(@NonNull Bundle bundle,@NonNull Map<String, ?> args) {
        if (args.isEmpty())
            return;
        for (Map.Entry<String, ?> entry : args.entrySet()) {
            String key = entry.getKey();
            Object value = entry.getValue();
            Class<?> clazz = value.getClass();
            if (clazz.equals(Integer.class)) {
                bundle.putInt(key, (Integer) value);
            } else if (clazz.equals(Long.class)) {
                bundle.putLong(key, (Long) value);
            } else if (clazz.equals(Double.class)) {
                bundle.putDouble(key, (Double) value);
            } else if (clazz.equals(Float.class)) {
                bundle.putFloat(key, (Float) value);
            } else if (clazz.equals(Short.class)) {
                bundle.putShort(key, (Short) value);
            } else if (clazz.equals(Long.class)) {
                bundle.putString(key, (String) value);
            } else if (Parcelable.class.isAssignableFrom(clazz)) {
                bundle.putParcelable(key, (Parcelable) value);
            } else if (Serializable.class.isAssignableFrom(clazz)) {
                bundle.putSerializable(key, (Serializable) value);
            } else {
                throw new IllegalArgumentException("Unsupported argument type conversion to Bundle: " + clazz);
            }
        }
    }

    public static Bundle toBundle(@NonNull Map<String, ?> args) {
        Bundle bundle = new Bundle();
        toBundle(bundle, args);
        return bundle;
    }

    public static Map<String, ?> toMap(@Nullable Bundle bundle) {
        if (bundle == null || bundle.isEmpty()) {
            return Collections.emptyMap();
        }
        Map<String, Object> args = new HashMap<>();
        for (String key : bundle.keySet()) {
            args.put(key, bundle.get(key));
        }
        return args;
    }

    public static int getStatusBarHeight(Activity activity){
        Rect rectangle = new Rect();
        Window window = activity.getWindow();
        window.getDecorView().getWindowVisibleDisplayFrame(rectangle);
        int statusBarHeight = rectangle.top;
        int contentViewTop =
                window.findViewById(Window.ID_ANDROID_CONTENT).getTop();
        return contentViewTop - statusBarHeight;
    }


    public static Snackbar multiLineSnackBar(ViewGroup layout, CharSequence message,
                                             @StringRes int actionTitle,
                                             View.OnClickListener action,
                                            int duration,
                                             @IntRange(from = 2, to = 10) int lines){
        Snackbar snackbar = Snackbar.make(layout, message, duration)
                .setAction(actionTitle, action);
        View snackbarLayout = snackbar.getView();
        TextView textView = snackbarLayout.findViewById(android.support.design.R.id.snackbar_text);
        textView.setMaxLines(lines);
//        textView.setCompoundDrawablesWithIntrinsicBounds(icon, 0, 0, 0);
//        textView.setCompoundDrawablePadding(layout.getResources().getDimensionPixelOffset(R.dimen.default_margin));
        return snackbar;
    }

}
