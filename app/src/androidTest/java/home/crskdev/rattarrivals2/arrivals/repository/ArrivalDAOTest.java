package home.crskdev.rattarrivals2.arrivals.repository;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import home.crskdev.rattarrivals2.AndroidBaseDBTest;
import home.crskdev.rattarrivals2.arrivals.ArrivalsConverter;
import home.crskdev.rattarrivals2.arrivals.model.Arrival;
import home.crskdev.rattarrivals2.arrivals.model.ArrivalExtra;
import home.crskdev.rattarrivals2.lines.model.Line;
import home.crskdev.rattarrivals2.lines.model.LineType;
import home.crskdev.rattarrivals2.lines.repository.LineDAO;
import home.crskdev.rattarrivals2.lines.LineConverter;
import home.crskdev.rattarrivals2.util.ImmLists;
import home.crskdev.rattarrivals2.util.time.TimeEstimated;
import io.reactivex.subscribers.TestSubscriber;

import static home.crskdev.rattarrivals2.arrivals.repository.ArrivalRepositoryConverter.toDTOExtraFromEntityExtra;
import static home.crskdev.rattarrivals2.arrivals.repository.ArrivalRepositoryConverter.toEntityFromDTOExtra;
import static junit.framework.Assert.assertEquals;

/**
 *
 * Created by criskey on 21/9/2017.
 */
public class ArrivalDAOTest extends AndroidBaseDBTest {

    private ArrivalDAO dao;

    private long line40Id = 1886L;

    private Line line40 = new Line(line40Id, "40", LineType.AUTO, true);
    private Line line1 = new Line(9999, "1", LineType.TRAM, true);
    private Arrival arrStuparilor = new Arrival(line40.id, "Stuparilor", TimeEstimated.nonEstimated(1234L)
            , "Grozavescu", 1L);
    private Arrival arrHoldelor = new Arrival(line40.id, "Holdelor", TimeEstimated.nonEstimated(2222L)
            , "Grozavescu", 2L);
    private ArrivalExtra extraStuparilor = ArrivalsConverter.compose(arrStuparilor, line40);
    private ArrivalExtra extraHoldelor = ArrivalsConverter.compose(arrHoldelor, line40);

    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
        dao = db.getPinnedArrivalRepository();

        LineDAO lineRepository = db.getLineRepository();
        lineRepository.create(Arrays.asList(LineConverter.toEntity(line40, 1L),
                LineConverter.toEntity(line1, 1L)));
    }

    @Test
    public void should_show_when_count_is_positive() throws Exception {
        final TestSubscriber<Integer> test = dao.observeCount().test();

        insert();
        delete();

        assertEquals(Arrays.asList(0, 2, 0), test.values());

    }

    @Test
    public void should_show_all_extra() throws Exception {
        insert();
        assertEquals(Arrays.asList(
                extraStuparilor,
                extraHoldelor
                ),
                ImmLists.map(dao.getAllExtra(),
                        ArrivalRepositoryConverter::toDTOExtraFromEntityExtra));
    }

    @Test
    public void should_show_some() throws Exception {
        insert();
        final List<ArrivalExtraEntity> extraEntities = dao.selectSome(
                Arrays.asList(342342L, 4534543L, line40Id, 4354534L, line40Id),
                Arrays.asList("xxx", "foo", "Stuparilor", "bar", "Holdelor"),
                Arrays.asList("to", "tooo", "Grozavescu", "ttt", "Grozavescu"));
        final List<ArrivalExtra> expected = new ArrayList<>(Arrays.asList(
                extraStuparilor,
                extraHoldelor
        ));
        final Comparator<ArrivalExtra> comparator = (o1, o2) -> o1.stationName.compareTo(o2.stationName);
        final List<ArrivalExtra> actual = ImmLists.Chain
                .from(extraEntities)
                .map(ArrivalRepositoryConverter::toDTOExtraFromEntityExtra)
                .sort(comparator)
                .get();
        Collections.sort(expected, comparator);
        assertEquals(expected, actual);

    }

    @Test
    public void should_select_one() throws Exception {
        insert();
        final ArrivalExtraEntity extraEntity = dao.selectOne(line40Id, "Stuparilor", "Grozavescu");
        assertEquals(extraStuparilor, toDTOExtraFromEntityExtra(extraEntity));
    }

    private void insert() {
        dao.upsert(
                toEntityFromDTOExtra(extraStuparilor),
                toEntityFromDTOExtra(extraHoldelor));
    }

    private void delete() {
        dao.delete(
                toEntityFromDTOExtra(extraStuparilor),
                toEntityFromDTOExtra(extraHoldelor));
    }
}