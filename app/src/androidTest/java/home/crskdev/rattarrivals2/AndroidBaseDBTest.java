package home.crskdev.rattarrivals2;

import android.arch.core.executor.AppToolkitTaskExecutor;
import android.arch.core.executor.TaskExecutor;
import android.arch.persistence.room.Room;
import android.content.Context;
import android.support.annotation.CallSuper;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;

import home.crskdev.rattarrivals2.repository.RATTArrivals2Database;

/**
 *
 * Created by criskey on 21/9/2017.
 */
public class AndroidBaseDBTest extends AndroidBaseTest{

    protected RATTArrivals2Database db;


    @Before
    @CallSuper
    public void setUp() throws Exception {
        super.setUp();
        AppToolkitTaskExecutor.getInstance().setDelegate(new TaskExecutor() {
            @Override
            public void executeOnDiskIO(Runnable runnable) {
                runnable.run();
            }

            @Override
            public void postToMainThread(Runnable runnable) {
                runnable.run();
            }

            @Override
            public boolean isMainThread() {
                return false;
            }
        });
        db = Room.inMemoryDatabaseBuilder(context, RATTArrivals2Database.class)
                .build();
    }

    @After
    @CallSuper
    public void tearDown() throws Exception {
        db.close();
    }
}
