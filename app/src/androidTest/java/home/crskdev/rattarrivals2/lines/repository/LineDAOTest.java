package home.crskdev.rattarrivals2.lines.repository;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import home.crskdev.rattarrivals2.AndroidBaseDBTest;
import home.crskdev.rattarrivals2.lines.model.LineType;
import home.crskdev.rattarrivals2.lines.repository.LineDAO;
import home.crskdev.rattarrivals2.lines.repository.LineEntity;
import io.reactivex.observers.TestObserver;
import io.reactivex.subscribers.TestSubscriber;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by criskey on 13/8/2017.
 */
public class LineDAOTest extends AndroidBaseDBTest {

    static final long START_WATCH_TIME = 3;
    long timeLine = 0;
    LineDAO dao;
    List<LineEntity> lines;

    @Before
    public void setUp() throws Exception {
        super.setUp();
        dao = db.getLineRepository();
        lines = Arrays.asList(
                new LineEntity(1L, "1", LineType.AUTO, false, timeLine++),
                new LineEntity(2L, "2", LineType.TRAM, false, timeLine++),
                new LineEntity(3L, "3", LineType.TROLLEY, false, timeLine++)
        );
        dao.create(lines);
    }

    @Test
    public void create() throws Exception {
        Long[] indices = dao.create(lines);
        assertArrayEquals(new Long[]{1L, 2L, 3L}, indices);
    }

    @Test
    public void getLinesByType() throws Exception {
        TestSubscriber<LineEntity> test = dao.getLinesByType(LineType.AUTO)
                .concatWith(dao.getLinesByType(LineType.TRAM))
                .concatWith(dao.getLinesByType(LineType.TROLLEY).toFlowable())
                //flatten the lists
                .map(l -> l.get(0)).test();
        assertEquals(3, test.valueCount());
        assertEquals(lines, test.values());
    }

    @Test
    public void observeFavoriteChanges() throws Exception {

        TestSubscriber<LineEntity> test = dao.observeChanges(START_WATCH_TIME).test();

        dao.updateLine(lines.get(0).setFavorite(true, timeLine++));
        dao.updateLine(lines.get(1).setFavorite(true, timeLine++));
        dao.updateLine(lines.get(1).setFavorite(false, timeLine++));
        dao.updateLine(lines.get(2).setFavorite(true, timeLine++));
        dao.updateLine(lines.get(0).setFavorite(false, timeLine++));

        assertEquals(5, test.valueCount());

        assertEquals(lines.get(0), test.values().get(0));
        assertTrue(test.values().get(0).favorite);

        assertEquals(lines.get(1), test.values().get(1));
        assertTrue(test.values().get(1).favorite);

        assertEquals(lines.get(1), test.values().get(2));
        assertFalse(test.values().get(2).favorite);

        assertEquals(lines.get(2), test.values().get(3));
        assertTrue(test.values().get(3).favorite);

        assertEquals(lines.get(0), test.values().get(4));
        assertFalse(test.values().get(4).favorite);

    }

    @Test
    public void getLinesById() throws Exception {
        TestObserver<LineEntity> test = dao.getLineById(lines.get(0).id).test();
        assertEquals(lines.get(0), test.values().get(0));
    }


    <T> void print(Collection<T> l) {
        for (T t : l) {
            System.out.println(t);
        }
    }

}