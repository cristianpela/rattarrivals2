package home.crskdev.rattarrivals2.util.recyclerview;

import android.support.annotation.LayoutRes;
import android.support.test.filters.SmallTest;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.widget.FrameLayout;

import org.junit.Test;

import java.util.Arrays;

import home.crskdev.rattarrivals2.AndroidBaseTest;

import static junit.framework.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.internal.verification.VerificationModeFactory.times;

/**
 * Created by criskey on 6/10/2017.
 */
@SuppressWarnings("ALL")
@SmallTest
public class ViewHolderBinderManagerTest extends AndroidBaseTest {

    private static final int FOO_BINDER_VIEW_TYPE = 1;
    private static final int BAR_BINDER_VIEW_TYPE = 2;

    private static boolean inRange(int value, int inclusiveBottom, int inclusiveUpper) {
        return value >= inclusiveBottom && value <= inclusiveUpper;
    }

    @Test
    @SuppressWarnings("unchecked")
    public void onBindViewHolder() throws Exception {

        final FooViewHolder fooViewHolder = new FooViewHolder(new View(context));
        final BarViewHolder barViewHolder = new BarViewHolder(new View(context));
        final DefaultViewHolderImpl defaultViewHolderImpl = new DefaultViewHolderImpl(new View(context));

        ViewHolderBinder<FooViewHolder, Integer> fooBinder = spy(new FooViewBinder());
        ViewHolderBinder<BarViewHolder, Integer> barBinder = spy(new BarViewBinder());
        ViewHolderBinder<DefaultViewHolderImpl, Integer> defaultBinder = spy(
                new SimpleViewHolderBinder(android.R.layout.simple_list_item_2));


        //we put extra same binders to test if manager filter the dubplicates
        ViewHolderBinderManager<ViewHolder, Integer> manager =
                new ViewHolderBinderManager(Arrays.asList(barBinder, defaultBinder,
                        fooBinder, fooBinder, barBinder, new DefaultViewHolderImpl(new View(context))));

        assertEquals(3, manager.getBinderCount());

        manager.emitOnBind(new FrameLayout(context), Arrays.asList(
                1,      //foo
                1,      //foo
                1500,   //bar
                3000,   //default
                1800,   //bar
                35,     //foo
                75,     //foo
                45656   //default
        ));

        verify(fooBinder, times(4)).onBind(any(FooViewHolder.class), anyInt());
        verify(barBinder, times(2)).onBind(any(BarViewHolder.class), anyInt());
        verify(defaultBinder, times(2)).onBind(any(DefaultViewHolderImpl.class), anyInt());

    }

    static class FooViewBinder implements ViewHolderBinder<FooViewHolder, Integer> {

        @Override
        public void onBind(FooViewHolder viewHolder, Integer item) {

        }

        @Override
        public int getViewTypeByItem(Integer item) {
            return inRange(item, 1, 999) ? FOO_BINDER_VIEW_TYPE : NO_VIEW_TYPE;
        }

        @Override
        public int getViewType() {
            return FOO_BINDER_VIEW_TYPE;
        }

        @Override
        public int layout() {
            return android.R.layout.simple_list_item_1;
        }

        @Override
        public Class<FooViewHolder> getViewHolderClass() {
            return FooViewHolder.class;
        }
    }

    static class BarViewBinder implements ViewHolderBinder<BarViewHolder, Integer> {

        @Override
        public void onBind(BarViewHolder viewHolder, Integer item) {

        }

        @Override
        public int getViewTypeByItem(Integer item) {
            return inRange(item, 1000, 2000) ? BAR_BINDER_VIEW_TYPE : NO_VIEW_TYPE;
        }

        @Override
        public int getViewType() {
            return BAR_BINDER_VIEW_TYPE;
        }

        @Override
        public int layout() {
            return android.R.layout.simple_list_item_1;
        }

        @Override
        public Class<BarViewHolder> getViewHolderClass() {
            return BarViewHolder.class;
        }
    }

    static class SimpleViewHolderBinder extends DefaultViewHolderBinder<DefaultViewHolderImpl, Integer> {

        public SimpleViewHolderBinder(@LayoutRes int resLayout) {
            super(resLayout);
        }

        @Override
        public void onBind(DefaultViewHolderImpl viewHolder, Integer item) {

        }

        @Override
        public Class<DefaultViewHolderImpl> getViewHolderClass() {
            return DefaultViewHolderImpl.class;
        }
    }

    public static class DefaultViewHolderImpl extends ViewHolder {

        public DefaultViewHolderImpl(View itemView) {
            super(itemView);
        }
    }

    public static class FooViewHolder extends ViewHolder {

        public FooViewHolder(View itemView) {
            super(itemView);
        }
    }

    public static class BarViewHolder extends ViewHolder {

        public BarViewHolder(View itemView) {
            super(itemView);
        }
    }

}