package home.crskdev.rattarrivals2;

import android.content.Context;
import android.support.annotation.CallSuper;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Before;
import org.junit.runner.RunWith;

import home.crskdev.rattarrivals2.util.asserts.Asserts;
import home.crskdev.rattarrivals2.util.asserts.ThreadingAsserts;
import io.reactivex.Scheduler;
import io.reactivex.plugins.RxJavaPlugins;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.schedulers.TestScheduler;

/**
 * Created by criskey on 6/10/2017.
 */
@RunWith(AndroidJUnit4.class)
public class AndroidBaseTest {

    static {
        Asserts.threadingAsserts = new ThreadingAsserts() {
            @Override
            public boolean assertIsOnMainThread() {
                return true;
            }

            @Override
            public boolean assertIsBackgroundThread() {
                return true;
            }
        };
    }

    public TestScheduler testScheduler = new TestScheduler();
    public Scheduler uiScheduler = Schedulers.trampoline();
    protected Context context;

    @Before
    @CallSuper
    public void setUp() throws Exception {
        RxJavaPlugins.setIoSchedulerHandler(__ -> Schedulers.trampoline());
        RxJavaPlugins.setComputationSchedulerHandler(__ -> testScheduler);
        context = InstrumentationRegistry.getTargetContext();
    }

}
